#!groovy
import groovy.json.JsonOutput
// Add whichever params you think you'd most want to have
def notifySlack(username, icon_emoji, channel, error=0, text) {
    def slackURL = 'https://hooks.slack.com/services/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'
    sh "'${env.WORKSPACE}/scripts/notify.sh' '${slackURL}' '${icon_emoji}' '${channel}' '${username}' '${error}' '${text}'"
}

def isFirstBuild() {
    def first = fileExists('../workspace@tmp/LAST_REVISION')
    return first ? false : true
}

node {
    currentBuild.result = "SUCCESS"

    try {

        // Environment
        env.PROJECT_NAME = "hatch"
        env.WORKSPACE = pwd()
        env.WORKSPACE_TMP = "${env.WORKSPACE}@tmp"
        env.BASE_JOB_NAME = "${env.JOB_NAME}".split('/').last()
        env.NODE_ENV = "PRODUCTION"
        print "WORKSPACE ${env.WORKSPACE}"
        print "WORKSPACE_TMP ${env.WORKSPACE_TMP}"

        // Environment - Repository
        // env.GIT_URL = "git@bitbucket.org:thispage/16070_belfius-video.git"
        // env.GIT_REMOTE_BRANCH = "*/develop"
        // env.GIT_LOCAL_BRANCH = "develop"

        // Environment - Slack
        env.SLACK_CHANNEL = "#hatch"
        env.SLACK_PROJECT_NAME = env.PROJECT_NAME + " ${env.PROJECT_ENV}"

        // Environment - Tools
        env.NPM_CACHE = "/var/lib/jenkins/.npm/"
        env.COMPOSER_BIN = "/usr/local/bin/composer"
        env.DEPLOY_SHELL = (env.DEPLOY_SHELL ? env.DEPLOY_SHELL : "/bin/bash")

        // Environment - Archive
        env.ARCHIVE_PREFIX = "dist-${env.PROJECT_NAME}-${env.PROJECT_ENV}"
        env.ARCHIVE = "${env.ARCHIVE_PREFIX}-${env.BUILD_NUMBER}"
        env.ARCHIVE_EXT = "tar.bz2"
        env.ARCHIVE_FULL = "${env.ARCHIVE}.${env.ARCHIVE_EXT}"
        env.ARCHIVE_EXCLUDE = "--exclude=workspace/node_modules --exclude=workspace/.git --exclude=workspace/dist workspace/"

        // Environment - Deploy
        // env.DIST_VHOST = "www/preview/boilerplate"
        env.DIST_DOCROOT = "${env.DIST_VHOST}/public"
        env.DIST_DIR_OLD = "${env.DIST_VHOST}/old"
        env.DIST_DIR_PREVIOUS = "${env.DIST_VHOST}/previous"
        env.DIST_DIR_CURRENT = "${env.DIST_VHOST}/current"

        def first = isFirstBuild()
        if (first) {
            env.FIRST_TIME = true
        } else {
            env.FIRST_TIME = false
        }
        print "> ENV.FIRST_TIME=${env.FIRST_TIME}"

        if (first) {
            print "> First time build... ${env.FIRST_TIME}"
        } else {
            print "> Regular build, continuing...  ${env.FIRST_TIME}"
        }

        // Mark the code build 'stage'....
        stage 'Checkout'
            if (first) {
                print "> No last revision found  due to first build"
            } else {
                // Save last revision
                sh 'git log --pretty="%h" -n1 HEAD > "$WORKSPACE_TMP/LAST_REVISION"'
                env.LAST_REVISION = readFile('../workspace@tmp/LAST_REVISION')
            }

            // Checkout
            checkout scm

            // Save changes
            if (first) {
                env.CHANGES = "First build"
                sh 'git log --pretty="%h" -n1 HEAD > "$WORKSPACE_TMP/LAST_REVISION"'
            } else {
                sh 'git log --pretty=format:"- %s [%an]\\n" `echo $LAST_REVISION|tr -d "\n[:space:]"`...HEAD > CHANGES'
                env.CHANGES = readFile('CHANGES')
                sh 'rm CHANGES'
            }
            print "Changes are: ${env.CHANGES}"

            // Parameters
            sh 'git log --pretty="- *%h* %N" -n1 HEAD > PROJECT_VERSION'
            sh 'date +"_%d/%m/%y %T_" > BUILD_DATE'
            env.PROJECT_VERSION = readFile('PROJECT_VERSION')
            env.BUILD_DATE = readFile('BUILD_DATE')
            sh 'rm BUILD_DATE'
            sh 'rm PROJECT_VERSION'

        stage 'Notify-Build'
            def text = "Starting a new build <${env.BUILD_URL}|#${env.BUILD_NUMBER}> for <${env.SLACK_PROJECT_URL}|${env.SLACK_PROJECT_NAME}> ${env.PROJECT_VERSION}"
            notifySlack('Jenkins', ':jenkins:', env.SLACK_CHANNEL, 0, text)

        stage 'Build'
            print "Environment will be : ${env.NODE_ENV}"
            print "Workspace is: ${env.WORKSPACE}"

            sh "'${env.WORKSPACE}/scripts/build.sh'"

        stage 'Deploy'
            sshagent([env.DEPLOY_AGENT]) {

                if (env.PROJECT_ENV == 'production') {
                    sh "'${env.WORKSPACE}/scripts/deploy_production.sh'"
                } else {
                    sh "'${env.WORKSPACE}/scripts/deploy.sh'"
                }
            }

        stage 'Notify-Release'
            text = "A new version has been built and released for <${env.SLACK_PROJECT_URL}|${env.SLACK_PROJECT_NAME}> ${env.PROJECT_VERSION} ${env.BUILD_DATE}\\n${env.CHANGES}"
            notifySlack('Jenkins', ':jenkins:', env.SLACK_CHANNEL, 0, text)
    }
    catch (err) {

        currentBuild.result = "FAILURE"

        errorMessage = err.getLocalizedMessage();
        text = "*Oh noes*, something wrong happened in build <${env.BUILD_URL}|#${env.BUILD_NUMBER}> ! <${env.SLACK_PROJECT_URL}|${env.SLACK_PROJECT_NAME}> ${env.PROJECT_VERSION} ${env.BUILD_DATE}\\n${errorMessage}"
        notifySlack('Jenkins', ':jenkins:',env.SLACK_CHANNEL, 1, text)

        throw err
    }
}
