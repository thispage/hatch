#/bin/env bash
set -o errexit # Exit on error
set -x # Deployment debug

# Deploy static assets
# s3cmd --exclude .DS_Store --no-mime-magic -M --no-preserve put --recursive public/build s3://static.brandcentral.io/app/

echo "# Running keyscan of remote host..."
add-to-known-hosts ${DEPLOY_HOST} ${DEPLOY_PORT} rsa /var/lib/jenkins/.ssh/known_hosts

echo "# Sending Archive"
RESULT_RSYNC=`rsync -Pave "ssh -p $DEPLOY_PORT -i /var/lib/jenkins/.ssh/jenkins_frb_hatch_rsa" dist/$ARCHIVE_FULL $DEPLOY_USER@$DEPLOY_HOST:dist/$ARCHIVE_FULL`
# TODO rsync archive
echo "# Sending Commands"
RESULT_EXEC=$(ssh -p $DEPLOY_PORT -i /var/lib/jenkins/.ssh/jenkins_frb_hatch_rsa $DEPLOY_USER@$DEPLOY_HOST $DEPLOY_SHELL << EOF
echo "# Move old release"
mv -f $DIST_DIR_PREVIOUS $DIST_DIR_OLD

# echo "# Maintenance for old website"
# php $DIST_DIR_CURRENT/artisan down

echo "# Backup current release"
mv -f $DIST_DIR_CURRENT $DIST_DIR_PREVIOUS
mkdir -p $DIST_DIR_CURRENT

echo "# Update with new release"
tar -xf dist/$ARCHIVE_FULL -C $DIST_DIR_CURRENT/ --strip-components=1

# echo "# Maintenance for new website"
# php $DIST_DIR_CURRENT/artisan down

echo "# Copy Configuration: "
echo "env/${PROJECT_NAME}_${PROJECT_ENV} $DIST_DIR_CURRENT/.env"
cp -f env/${PROJECT_NAME}_${PROJECT_ENV} $DIST_DIR_CURRENT/.env

# echo "# MySQL Migrate"
# php $DIST_DIR_CURRENT/artisan migrate

# echo "# Create storage and uploads"
# mkdir -p $DIST_DIR_CURRENT/storage/
# mkdir -p $DIST_DIR_CURRENT/uploads/

# move uploaded files
echo "# Copy uploads"
cp -R $DIST_DIR_PREVIOUS/web/app/uploads/* $DIST_DIR_CURRENT/web/app/uploads/
# mv $DIST_DIR_PREVIOUS/web/app/uploads/ $DIST_DIR_PREVIOUS/web/app/uploads-git/

# ln -s $DIST_DIR_CURRENT/web/app/uploads /data/web/bosebrand/wp-uploads

echo "# Apply permissions"
# chmod -R 2770 $DIST_DIR_CURRENT/storage/
chmod -R 2777 $DIST_DIR_CURRENT/web/app/uploads/

ln -s /srv/app/hatch-production/htdocs/current/web/wp/wp-includes/ $DIST_DIR_CURRENT/web/wp-includes
# ln -s /srv/app/hatch-production/htdocs/current/web/wp/wp-admin/ $DIST_DIR_CURRENT/web/wp-admin
ln -s /srv/app/hatch-production/htdocs/current/web/wp/wp-login.php $DIST_DIR_CURRENT/web/wp-login.php

# echo "# Run optimize"
# php $DIST_DIR_CURRENT/artisan optimize


# echo "# Maintenance finished for old website"
# php $DIST_DIR_CURRENT/artisan up


echo "# Destroy archive"
rm -Rf dist/$ARCHIVE_FULL

echo "# Clean oldest release"
rm -Rf $DIST_DIR_OLD
exit 0
EOF
)

echo "SSH result $RESULT_EXEC"
