#/bin/env bash
set -o errexit # Exit on error

source ~/.nvm/nvm.sh

nvm use 5.10
# npm config set cache $NPM_CACHE


# Install Composer vendors
echo "> Composer install"
$COMPOSER_BIN install -o

# Frontend install
echo "> Installing dependencies"
cd web/app/themes/hatch_fo/
npm install
bower install

echo "> Compiling assets"
# npm run build:js
gulp build

cd ../../../../

# Archiving
# Make dist dir just in case its first time
cd ../

# Clean and Compress

rm -f workspace
ln -s "$BASE_JOB_NAME"/ workspace
mkdir -p workspace/dist
rm -f workspace/dist/$ARCHIVE_PREFIX-*.$ARCHIVE_EXT

echo "Will run command: tar -hIlbzip2 -cf $ARCHIVE_FULL $ARCHIVE_EXCLUDE"
tar -hIlbzip2 -cf $ARCHIVE_FULL $ARCHIVE_EXCLUDE
mv $ARCHIVE_FULL workspace/dist/$ARCHIVE_FULL
echo "=> Created archive $ARCHIVE"
