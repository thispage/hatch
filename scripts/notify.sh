#!/usr/bin/env bash

# Usage: slackpost "<webhook_url>" "<icon_emoji>" "<channel>" "<username>" "<error>" "<message>"

# ------------
# Parameters
color="good"

# ------------
webhook_url=$1
if [[ $webhook_url == "" ]]
then
        echo "No hook specified"
        exit 1
fi

# ------------
shift
icon_emoji=$1
if [[ $icon_emoji == "" ]]
then
        icon_emoji=":slack:"
fi


# ------------
shift
channel=$1
if [[ $channel == "" ]]
then
        echo "No channel specified"
        exit 1
fi

# ------------
shift
username=$1
if [[ $username == "" ]]
then
        echo "No username specified"
        exit 1
fi

# ------------
shift
error=$1

if [[ $error == 1 ]]
then
        color="danger"
fi

# ------------
shift
text=$*

if [[ $text == "" ]]
then
        echo "No text specified"
        exit 1
fi

escapedText=$(echo $text | sed 's/"/\"/g' | sed "s/'/\'/g" | sed ":a;N;$!ba;s/\\\\n/\\n/g" )


json="{\"channel\": \"$channel\", \"username\":\"$username\", \"icon_emoji\":\"$icon_emoji\", \"attachments\":[{\"color\":\"$color\" , \"text\": \"${escapedText:0:4000}\", \"mrkdwn_in\":[\"text\"]}]}"

echo "Payload debug: $json"
echo ""
curl -s -d "payload=$json" "$webhook_url"
