<?php

// Default field values
$custom_fields = array();

// Agencies (504)
$args = array(
    'include' => 504,
    'post_type' => 'page',
    'post_status' => 'publish'
);

$posts_array = get_pages($args);

if (count($posts_array) === 1) {
    $post = $posts_array[0];
    $custom_fields = get_fields($post->ID);
}

?>

<section class="full-page-header">
    <div class="full-page-header-content">
        <h1><?php echo $custom_fields['header_title']; ?></h1>
        <h3><span class="full-page-header-date"><?php echo $custom_fields['header_subtitle']; ?></span></h3>
    </div>

    <div class="full-page-header-bg video-background"
        <?php
            if (isset($custom_fields['header_background_image']) && $custom_fields['header_background_image'] != '') {
                echo 'style="background-image: url('.$custom_fields['header_background_image'].')"';
            }
        ?>
    >
    <?php
    if(!IS_TABLET) {
        echo '<div class="video-foreground " id="myVideo">
            <iframe id="myVideo" width="100%" height="100%"
            src="https://www.youtube.com/embed/YaAwSaMYDyU?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=0&amp;volume=0"
            frameborder="0"  allowfullscreen">
            </iframe>
        </div>';
    }
    ?>



    </div>
</section>

<section class="section brands-section brands-section__top_section">
    <div class="news__section__pattern news__section__pattern--bottom news-page-top-pattern"></div>

    <div class="section__holder">
        <div class="section__header">
            <h1 class="section__header__title"><?php echo $custom_fields['intro_header']; ?></h1>
        </div>
        <?php if ($custom_fields['first_text_intro'] !== '') { ?>
            <h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['first_text_intro']; ?></span></h2>
        <?php } ?>
        <p>
            <?php echo $custom_fields['intro_text_col_2']; ?>
        </p>

        <div class="section__header">
            <h1 class="section__header__title"></h1>
            <h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['intro_sub_header']; ?></span></h2>
        </div>

        <p>
            <?php echo $custom_fields['intro_text_2']; ?>
        </p>

        <?php
            if (isset($custom_fields['intro_image']) && $custom_fields['intro_image'] != '') {
                echo '<img src="'.$custom_fields['intro_image'].'" class="brands-top__big-image" />';
            }
        ?>

        <?php if(isset($custom_fields['logos_intro']) && $custom_fields['logos_intro'] !== '') { ?>
            <div class="section__header">
    			<div class="section__header__title"></div>
    			<h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['logos_intro']; ?></span></h2>
    		</div>
        <?php } ?>


        <div class="section__content integration__companies">
            <?php
            if(isset($custom_fields['intro_logo'])) {

                foreach ($custom_fields['intro_logo'] as $key => $item) {

                    echo '<div class="integration__companies__logo"><img class="grayscale grayscale-fade" src="'.$item['logo'].'" alt=""></div>';
                }
            }
            ?>
        </div>


        <div class="our_solution__hightlight_blocks">
        <?php
            if(isset($custom_fields['intro_blocks'])) {

                foreach ($custom_fields['intro_blocks'] as $key => $block) {

                    echo '<div class="our_solution__hightlight_block">
                        <div class="our_solution__hightlight_block__title">
                            '.$block['title'].'
                        </div>
                        <div class="our_solution__hightlight_block__desc">
                            '.$block['text'].'
                        </div>
                    </div>';
                }
            }

        ?>
        </div>

        <div class="partners__content__quote">
            <div class="quote_image_holder">
                <?php
                    if (isset($custom_fields['why_quote_date_image']) && $custom_fields['why_quote_date_image'] != '') {
                        echo '<img src="'.$custom_fields['why_quote_date_image'].'" />';
                    }
                ?>
            </div>
            <div class="quote_content_holder">
                <blockquote>
                    <?php echo $custom_fields['why_quote_text']; ?>
                </blockquote>

                <p>
                    <span class="quote-name"><?php echo $custom_fields['why_quote_name']; ?></span>
                    <span class="quote-date">- <?php echo $custom_fields['why_quote_date']; ?></span>
                </p>
            </div>
        </div>


    </div>
</section>

<div class="section__separators">
    <div class="section__arrow section__arrow--intro" data-id="brand-step1">
        <div class="section__arrow__bg white-bg"></div>
        <span class="white-bg">Why use Hatch?</span>
    </div>

    <div class="section__pattern section__pattern--2">
        <div class="section__pattern__part section__pattern__part--top"></div>
        <div class="section__pattern__part section__pattern__part--bottom"></div>
    </div>
</div>

<section class="section brands-section brands-section-green brands-section__step_section" id="brand-step1">
    <div class="section__pattern section__pattern--4">
        <div class="section__pattern__part section__pattern__part--top"></div>
        <div class="section__pattern__part section__pattern__part--bottom"></div>
    </div>

    <div class="section__holder">
        <div class="section__header">
            <h1 class="section__header__title"><?php echo $custom_fields['why_header']; ?></h1>
            <h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['why_sub_header_1']; ?></span></h2>
        </div>

        <!--- ---- -->
        <div class="agencies-slideshow-mask">
            <div class="agencies-slideshow">
                <?php
                if(isset($custom_fields['slideshow'])) {

                    foreach ($custom_fields['slideshow'] as $key => $item) {
                        echo '<div class="agencies-slidehow-slide">
                            '.$item['title'].'
                            '.$item['description'].' ';

                            if (isset($item['image']) && $item['image'] != '') {
                                echo '<img src="'.$item['image'].'" alt="" class="slide"/>';
                            }

                        echo '</div>';
                    }
                }
                ?>
            </div>
        </div>


        <?php
        if(isset($custom_fields['slideshow'])) {
            echo '<ul class="navigation-slideshow">';
            foreach ($custom_fields['slideshow'] as $key => $item) {
                $num = $key + 1;
                echo '<li class="dots" data-dot= "' . $num . '"><div ></div></li>';

            }
            echo '</ul>';
        }
        ?>

        <!--- ---- -->
        <div class="section__header">
            <h1 class="section__header__title"></h1>
            <h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['why_sub_header2']; ?></span></h2>
        </div>

        <p class="step_3_intro">
            <?php echo $custom_fields['step_3_block_intro']; ?>
        </p>

        <div class="brand-step3__block">
            <div class="brand-step3__block_title">
                <?php echo $custom_fields['why_sub_header_3']; ?>
            </div>
            <div class="brand-step3__block_content">
                <ul>
                <?php
                if(isset($custom_fields['why_blocks'])) {

                    foreach ($custom_fields['why_blocks'] as $key => $item) {
                        echo '<li>'.$item['text'].'</li>';
                    }
                }
                ?>
                </ul>
            </div>
            <div class="brand-step3_logos">
                <?php
                if(isset($custom_fields['block_logo'])) {

                    foreach ($custom_fields['block_logo'] as $key => $item) {
                        echo '<div class="brand-step3_logo_block">';


                            echo '<img src="'.$item['block_logo_image'].'" />';


                            echo '<span>'.$item['block_logo_title'].'</span>
                        </div>';
                    }
                }
                ?>
            </div>
        </div>

        <!--- ---- -->
        <div class="section__header">
            <h1 class="section__header__title"></h1>
            <h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['case_study_title']; ?></span></h2>
        </div>
        <div class="section__content integration__cards">
            <?php
            if(isset($custom_fields['case_study_items'])) {
                foreach ($custom_fields['case_study_items'] as $key => $item) {

                    echo '<div class="integration__cards__card">
                            <div class="integration__cards__card__top" data-img="'.$item['image'].'" style="background-image: url('.$item['image'].');">
                                <img src="'.$item['image'].'" alt="" class="no-display">
                            </div>
                            <div class="integration__cards__card__bottom">
                                <p class="integration__cards__card__text">'.$item['desc'].'</p>
                            </div>
                            <button class="section__smallbtn" data-title="'.$item['title'].'"><span>Request PDF</span><svg height="40" version="1.1" width="180" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: absolute; left: 0px; top: 0px;" viewBox="0 0 180 40" preserveAspectRatio="xMinYMin"><rect x="0" y="-40" width="180" height="0" rx="10" ry="10" fill="rgb(77,77,77)" stroke="none" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" stroke-width="1"></rect></svg></button>
                        </div>';

                }
            }

            ?>
        </div>

        <!--- ---- -->

    </div>
    <div class="videoId"><?php echo $custom_fields['video_background']; ?></div>
</section>
