<section class="section disclaimer">
<?php

$fields = array(
	'get-in-touch' => array(
		'navigation' => '',
		'title' => '',
		'subtitle' => '',
		'shortcode' => '',
		'our_offices_subtitle' => '',
		'offices' => array()
	)
);

$c_fields = array(
	'id' => (int) get_the_ID(),
	'title' => '',
	'sub_title' => '',
	'content' => array()
);  

$c_fields['title'] = get_the_title();

$args = array(
	'include' => $c_fields['id'],
	'post_type' => 'page',
	'post_status' => 'publish'
);

$posts_array = get_pages($args);

if (count($posts_array) === 1) {
	$post = $posts_array[0];

	$custom_fields = get_fields($post->ID);

	//$c_fields['sub_title'] 	= $custom_fields['sub_title'];
	//$c_fields['content']	= $custom_fields['content'];
}
?>
<div class="section__holder">

	<div class="section__content disclaimer__content">
		<p><?php do_action( 'hatch_widget', array('mpn' => 'MR.JH911.001', 'ean' => '4713147096886', 'region' => 'GB', 'currency_position' => 'before'));  ?></p>
	</div>

</div>

</section>

?>
