<section class="section pricing">
	<div class="pricing__section__pattern--top pricing__section__pattern pricing__section__pattern"></div>

<?php

$fields = array(
	'get-in-touch' => array(
		'navigation' => '',
		'title' => '',
		'subtitle' => '',
		'shortcode' => '',
		'our_offices_subtitle' => '',
		'offices' => array()
	)
);

$c_fields = array(
	'id' => (int) get_the_ID(),
	'title' => '',
	'sub_title' => '',
	'plans' => array(),
	'disclaimer' => '',
	'shortcode' => ''
);

$c_fields['title'] = get_the_title();

define(CFIELD_ID, $c_fields['id']);

$args = array(
	'include' => $c_fields['id'],
	'post_type' => 'page',
	'post_status' => 'publish'
);

$posts_array = get_pages($args); 

if (count($posts_array) === 1) {
	$post = $posts_array[0];
	
	$custom_fields = get_fields($post->ID);
	
	$c_fields['sub_title'] 	= $custom_fields['sub_title'];
	$c_fields['disclaimer']	= $custom_fields['disclaimer'];
	$c_fields['shortcode']	= $custom_fields['shortcode'];
}

echo '<div class="section__holder">
		<header class="section__header section__header--pricing">';
		echo '<h1 class="section__header__title">'.$c_fields['title'].'</h1>';

		echo '<h2 class="section__header__subtitle shown"><span>'.$c_fields['sub_title'].'</span></h2>';
echo '</header>';

echo '<div class="pricing__content">';
echo do_shortcode('[rpt name="pricing-list"]');
echo '</div>';

echo '
	<div class="disclaimer">
		'.$c_fields['disclaimer'].'
	</div>';
echo '</div>' // end 'section__holder'

?>
	<div class="pricing__section__pattern pricing__section__pattern--bottom"></div>
</section>

<?php
// Get In Touch (192)
$args = array(
	'include' => 192,
	'post_type' => 'page',
	'post_status' => 'publish'
);
$posts_array = get_pages($args); 

if (count($posts_array) === 1) {
	$post = $posts_array[0];
	
	$custom_fields = get_fields($post->ID);
	
	$fields['get-in-touch']['title']		= $post->post_title;
	$fields['get-in-touch']['subtitle'] 	= $custom_fields['subtitle'];
	$fields['get-in-touch']['navigation'] 	= $custom_fields['navigation'];
	$fields['get-in-touch']['shortcode'] 	= $custom_fields['shortcode'];
}
?>
<section class="section section--contact contact" id="contact">
<div class="section__holder">
	<header class="section__header section__header--contact">
		<h1 class="section__header__title"><span><?=strtoupper($fields['get-in-touch']['title'])?></span></h1>
		<h2 class="section__header__subtitle"><span><?=$fields['get-in-touch']['subtitle']?></span></h2>
	</header>
	
	<div class="section__content contact__form">
		<?=do_shortcode($fields['get-in-touch']['shortcode'])?>
	</div>
</div>
<div class="section__pattern section__pattern--6 news__section__pattern--contact">
	<div class="section__pattern__part section__pattern__part--top"></div>
	<div class="section__pattern__part section__pattern__part--bottom"></div>
</div>

