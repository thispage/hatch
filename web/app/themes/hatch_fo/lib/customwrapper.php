<?php

namespace Roots\Sage\Custom_Wrapper;

//use Roots\Sage\Wrapper;
use Roots\Sage\Mobile_Detect;


function template_path() {
  return CustomWrapping::$main_template;
}

function get_page_name() {
  return CustomWrapping::$base ? CustomWrapping::$base : 'index';
}

class CustomWrapping {
  // Stores the full path to the main template file
  public static $main_template;

  // Basename of template file
  public $slug;

  // Array of templates
  public $templates;

  // Stores the base name of the template file; e.g. 'page' for 'page.php' etc.
  public static $base;

  public function __construct($template = 'base.php') {
    $this->slug = basename($template, '.php');
    $this->templates = [$template];

    if (self::$base) {
      $str = substr($template, 0, -4);
      array_unshift($this->templates, sprintf($str . '-%s.php', self::$base));
    }
  }

  public function __toString() {
    $this->templates = apply_filters('sage/wrap_' . $this->slug, $this->templates);
    return locate_template($this->templates);
  }

  private static function _checkMobile() {
    $MobileDetect = new Mobile_Detect();

    define('IS_MOBILE', ($MobileDetect->isMobile() && !$MobileDetect->isTablet() ? true : false));
    define('IS_TABLET', $MobileDetect->isTablet());

    if(IS_MOBILE && !IS_TABLET) {
      $basename = basename(self::$main_template);
      $filename = 'mobile/' . $basename;
      self::$main_template = str_replace($basename, $filename, self::$main_template);
    }
  }

  public static function wrap($main) {
    // Check for other filters returning null
    if (!is_string($main)) {
      return $main;
    }

    self::$main_template = $main;
    self::_checkMobile();
    self::$base = basename(self::$main_template, '.php');

    if (self::$base === 'index') {
      self::$base = false;
    }

    return new CustomWrapping();
  }
}
add_filter('template_include', [__NAMESPACE__ . '\\CustomWrapping', 'wrap'], 99);
