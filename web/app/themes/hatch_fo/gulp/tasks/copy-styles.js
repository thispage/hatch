/*
  Copy styles to build.
*/

var gulp            = require('gulp');
var stylus 	        = require('gulp-stylus');
var autoprefixer    = require('gulp-autoprefixer');
var handleErrors    = require('../util/handleErrors');
var browserSync     = require('browser-sync');

gulp.task('copy-styles', function() {
  var dest = './dist/styles';

  return gulp.src('./client/styles/*.styl')
    .pipe(stylus({
    	'include css': true
    }))
    .on('error', handleErrors)
    .pipe(autoprefixer())
    .on('error', handleErrors)
    .pipe(gulp.dest(dest))
    .pipe(browserSync.stream());
});
