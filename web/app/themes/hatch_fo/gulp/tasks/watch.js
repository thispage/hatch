/*
  Watch for changes in the stylus, images, and php directories and run respective task.
  The browserify task already watches for changes in the client code.
  NOTE: The browserify gulp task already handles js recompiling with watchify
  so we don't need to watch it here.
*/
var gulp = require('gulp');

gulp.task('watch', ['setWatch', 'browserSync'], function() {
    gulp.watch('client/assets/**', ['copy-assets']);
    gulp.watch('client/manifest.json', ['copy-manifest']);
    gulp.watch('client/**/*.php', ['copy-php']);
    gulp.watch('client/styles/**/*.styl', ['copy-styles']);
});
