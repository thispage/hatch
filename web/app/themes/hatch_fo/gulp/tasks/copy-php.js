/*
  Move PHP files out of client and into build.
*/

var gulp = require('gulp');
var handleErrors = require('../util/handleErrors');

gulp.task('copy-php', function() {
  var dest = './';

  return gulp.src('client/**/*.php')
    .on('error', handleErrors)
    .pipe(gulp.dest(dest));
});

