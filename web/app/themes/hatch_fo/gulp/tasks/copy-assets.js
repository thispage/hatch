/*
  Copy assets to build.
*/

var changed    = require('gulp-changed');
var gulp       = require('gulp');
var handleErrors = require('../util/handleErrors');

gulp.task('copy-assets', function() {
  var dest = './dist/assets';

  return gulp.src('./client/assets/**')
    .pipe(changed(dest)) // Ignore unchanged files
    .on('error', handleErrors)
    .pipe(gulp.dest(dest));
});
