////////////////
// Live task  //
// Uglify JS  //
// GZIP       //
// Minify CSS //
////////////////

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var stripDebug = require('gulp-strip-debug');

 

var handleErrors = require('../util/handleErrors');

// JS Compress 
gulp.task('compress-js', function() {
  return gulp.src('./dist/scripts/*.js')
    .pipe(stripDebug())
    .on('error', handleErrors)
    .pipe(uglify())
    .on('error', handleErrors)

    .pipe(gulp.dest('./dist/scripts/'));
});


// CSS Minify 
gulp.task('minify-css', function() {
  return gulp.src('./dist/styles/*.css')
    .pipe(minifyCss({compatibility: 'ie9'}))
    .pipe(gulp.dest('./dist/styles/'));
});

gulp.task('live', [
    //'striplog',
    'compress-js', 
    'minify-css'
]);
