/*
  Copy manifest to build.
*/

var changed    = require('gulp-changed');
var gulp       = require('gulp');
var jsonlint = require("gulp-jsonlint");
var handleErrors = require('../util/handleErrors');

gulp.task('copy-manifest', function() {
  var dest = './dist';

  return gulp.src('./client/manifest.json')
    .pipe(changed(dest)) // Ignore unchanged files
    .pipe(jsonlint())
    .on('error', handleErrors)
    .pipe(jsonlint.reporter())
    .pipe(gulp.dest(dest));
});
