/*
  Refresh browser when build directory is updated.
*/
var config = require('../../package.json')
var browserSync = require('browser-sync').create();
var gulp        = require('gulp');
//var modRewrite  = require('connect-modrewrite');

gulp.task('browserSync', ['build'], function() {
    browserSync.init(['**'], {
        proxy: config.devUrl
    });
});

