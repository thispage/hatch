/* browserify task
   ---------------
   Bundle javascripty things with browserify!

   If the watch task is running, this uses watchify instead
   of browserify for faster bundling using caching.
*/

var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');
var watchify = require('watchify');
var babel = require('babelify');

var bundleLogger = require('../util/bundleLogger');
var handleErrors = require('../util/handleErrors');

gulp.task('browserify', function() {
    var dest = './dist/scripts';

    // App Bundler
    var appBundler = browserify({
        // Required watchify args
        cache: {}, packageCache: {}, fullPaths: true,
        // Specify the entry point of your app
        entries: ['./client/javascript/app.js'],
        // Add file extentions to make optional in your requires
        extensions: ['.js'],
        // Enable source maps.
        debug: false
    }).transform(babel.configure({
        ignore: ["client/vendor", "client/javascript/lib/Draggable.js", "client/javascript/lib/DrawSVGPlugin.js", "client/javascript/lib/SplitText.js"]
    }));

    // App Bundler
    var appBundlerMobile = browserify({
        // Required watchify args
        cache: {}, packageCache: {}, fullPaths: true,
        // Specify the entry point of your app
        entries: ['./client/javascript/app_mobile.js'],
        // Add file extentions to make optional in your requires
        extensions: ['.js'],
        // Enable source maps.
        debug: false
    }).transform(babel.configure({
        ignore: ["client/vendor", "client/javascript/lib/Draggable.js", "client/javascript/lib/DrawSVGPlugin.js", "client/javascript/lib/SplitText.js"]
    }));

    

    // Bundles App
    var bundleApp = function() {
        // Log when bundling starts
        bundleLogger.start();

        // Bundles app
        appBundler
            .bundle()
            // Report compile errors
            .on('error', handleErrors)
            // Use vinyl-source-stream to make the
            // stream gulp compatible. Specifiy the
            // desired output filename here.
            .pipe(source('app.js'))
            .pipe(buffer())
            // Specify the output destination
            .pipe(gulp.dest(dest))
            // Log when bundling completes!
            .on('end', bundleLogger.end);
    };
    // Bundles App Mobile
    
    var bundleAppMobile = function() {
        // Log when bundling starts
        bundleLogger.start();

        // Bundles app
        appBundlerMobile
            .bundle()
            // Report compile errors
            .on('error', handleErrors)
            // Use vinyl-source-stream to make the
            // stream gulp compatible. Specifiy the
            // desired output filename here.
            .pipe(source('app_mobile.js'))
            .pipe(buffer())
            // Specify the output destination
            .pipe(gulp.dest(dest))
            // Log when bundling completes!
            .on('end', bundleLogger.end);
    };

   

    // Bundles everything
    var bundle = function() {
        bundleApp();
        bundleAppMobile();
    };


    if(global.isWatching) {
        appBundler = watchify(appBundler);
        appBundler.on('update', bundleApp);

        appBundlerMobile = watchify(appBundlerMobile);
        appBundlerMobile.on('update', bundleAppMobile);
    }

    return bundle();
});

