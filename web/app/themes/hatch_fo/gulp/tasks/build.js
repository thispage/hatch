/**
  Run all build tasks.
*/

var gulp = require('gulp');

gulp.task('build', [
	'browserify',
    'copy-styles',
    'copy-manifest',
	'copy-assets',
	'copy-php'
]);


