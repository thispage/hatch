<?php

// Default field values
$fields = array(
	'faqs' => array(
		'navigation' => '',
		'title' => '',
		'first_description_column' => '',
		'second_description_column' => '',
		'track_records_sub_title' => '',
		'track_records' => array(),
		'our_team_sub_title' => '',
		'team_members' => array()
	),
);

// FAQs (133)
$args = array(
	'include' => 501,
	'post_type' => 'page',
	'post_status' => 'publish'
);
$posts_array = get_pages($args);

if (count($posts_array) === 1) {
	$post = $posts_array[0];
	$custom_fields = get_fields($post->ID);

//	echo '<pre>';
//	exit(print_r($custom_fields));

	$fields['faqs']['title']	= $custom_fields['title'];
	$fields['faqs']['faqs']		= $custom_fields['faqs'];
}

?>

<section class="section section--faq faq" id="section_faq">

	<div class="section__pattern section__pattern--4">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>

	<div class="section__holder">
		<header class="section__header section__header--about no-animate">
			<h1 class="section__header__title"><?php echo $fields['faqs']['title']; ?></h1>
		</header>

		<?php

		foreach($fields['faqs']['faqs'] as $index => $faqs) {

			echo '<div class="faq-blocks">
				<h2 class="section__header__subtitle shown animate-border">
					<span>'.$faqs['category_title'].'</span>
				</h2>
				';

			foreach($faqs['faq'] as $faq) {
				echo '
					<div class="faq-item">
						<div class="faq-question">'.$faq['question'].'</div>

						<div class="faq-answer">
							'.$faq['answer'].'
						</div>
					</div>
				';
			}

			echo '</div>';
		}
		?>


	</div>

	</div>
</section>
