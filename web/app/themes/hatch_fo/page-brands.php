<?php

// Default field values
$custom_fields = array();

// About Us (133)
$args = array(
    'include' => 510,
    'post_type' => 'page',
    'post_status' => 'publish',
);

$posts_array = get_pages($args);

if (count($posts_array) === 1) {
    $post = $posts_array[0];
    $custom_fields = get_fields($post->ID);
}

?>

<section class="full-page-header">
	<div class="full-page-header-content">
		<h1><?php echo $custom_fields['header_title']; ?></h1>
		<h3><span class="full-page-header-date"><?php echo $custom_fields['header_subtitle']; ?></span></h3>
	</div>

    <div class="full-page-header-bg video-background"
        <?php
            if (isset($custom_fields['header_background_image']) && $custom_fields['header_background_image'] != '') {
                echo 'style="background-image: url('.$custom_fields['header_background_image'].')"';
            }
        ?>
    >
    <?php
    if (!IS_TABLET) {
        echo '<div class="video-foreground " id="myVideo">
	        <iframe id="myVideo" width="100%" height="100%"
	        src="https://www.youtube.com/embed/YaAwSaMYDyU?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=0&amp;volume=0"
	        frameborder="0"  allowfullscreen">
	        </iframe>
	    </div>';
    }
    ?>

	</div>
</section>

<section class="section brands-section brands-section__top_section">
	<div class="news__section__pattern news__section__pattern--bottom news-page-top-pattern"></div>

	<div class="section__holder">
		<div class="section__header">
			<h1 class="section__header__title"><?php echo $custom_fields['intro_header']; ?></h1>
		</div>
		<?php if ($custom_fields['intro_sub_header_1'] !== '') {
        ?>
			<h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['intro_sub_header_1']; ?></span></h2>
		<?php
    } ?>
		<p>
			<?php echo $custom_fields['intro_text_1']; ?>
		</p>

		<div class="section__header">
			<div class="section__header__title"></div>
			<h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['intro_sub_header_2']; ?></span></h2>
		</div>
		<p>
			<?php echo $custom_fields['intro_text_2']; ?>
		</p>

		<?php
            if (isset($custom_fields['intro_image']) && $custom_fields['intro_image'] != '') {
                echo '<img src="'.$custom_fields['intro_image'].'" class="brands-top__big-image" />';
            }
        ?>

        <?php if (isset($custom_fields['logos_intro']) && $custom_fields['logos_intro'] !== '') {
            ?>
            <div class="section__header">
    			<div class="section__header__title"></div>
    			<h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['logos_intro']; ?></span></h2>
    		</div>
        <?php
        } ?>


        <div class="brands-intro-logos">
			<?php
            if (isset($custom_fields['intro_logos'])) {
                foreach ($custom_fields['intro_logos'] as $key => $item) {
                    echo '<div class="brands-intro-logo-block">';

                    if (isset($item['link']) && $item['link'] != '') {
                        echo '<a href="'.$item['link'].'">';
                    }

                    if (isset($item['image']) && $item['image'] != '') {
                        echo '<img src="'.$item['image'].'" />';
                    }

                    if (isset($item['link']) && $item['link'] != '') {
                        echo '</a>';
                    }

                    echo '</div>';
                }
            }
            ?>
		</div>

		<div class="our_solution__hightlight_blocks">
		<?php
            if (isset($custom_fields['intro_blocks'])) {
                foreach ($custom_fields['intro_blocks'] as $key => $block) {
                    echo '<div class="our_solution__hightlight_block">
						<div class="our_solution__hightlight_block__title">
							'.$block['title'].'
						</div>
						<div class="our_solution__hightlight_block__desc">
							'.$block['text'].'
						</div>
					</div>';
                }
            }

        ?>
		</div>

		<div class="brands-list">
			<div class="brands-list-title">
				<?php echo $custom_fields['intro_list_title']; ?>
			</div>
			<div class="brands-list-items">
				<ul>
				<?php
                if (isset($custom_fields['intro_list_items'])) {
                    foreach ($custom_fields['intro_list_items'] as $key => $item) {
                        echo '<li>'.$item['text'].'</li>';
                    }
                }
                ?>
				</ul>
			</div>
		</div>

		<div class="partners__content__quote">
			<div class="quote_image_holder">
				<?php
                    if (isset($custom_fields['intro_quote_image']) && $custom_fields['intro_quote_image'] != '') {
                        echo '<img src="'.$custom_fields['intro_quote_image'].'" />';
                    }
                ?>
			</div>
			<div class="quote_content_holder">
				<blockquote>
					<?php echo $custom_fields['intro_quote_text']; ?>
				</blockquote>

				<p>
					<span class="quote-name"><?php echo $custom_fields['intro_quote_name']; ?></span>
					<span class="quote-date">- <?php echo $custom_fields['intro_quote_date']; ?></span>
				</p>
			</div>
		</div>

	</div>
</section>

<div class="section__separators">
	<div class="section__arrow section__arrow--intro" data-id="brand-step1">
		<div class="section__arrow__bg white"></div>
		<span>Why use Hatch?</span>
	</div>

	<div class="section__pattern section__pattern--2">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>
</div>

<section class="section brands-section brands-section-green brands-section__step_section" id="brand-step1">
	<div class="section__pattern section__pattern--4">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>

	<div class="section__holder">
		<div class="section__header">
			<h1 class="section__header__title"><?php echo $custom_fields['step_1_header']; ?></h1>
			<h2 class="section__header__subtitle shown"><span> <?php echo $custom_fields['step_1_sub_header']; ?></span></h2>
		</div>

		<div class="section__content integration__steps">
            <svg version="1.1" class="integration__steps_icons" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" style="enable-background:new 0 0 160 608;" xml:space="preserve" width="752" height="160">
                <circle class="edge_circ_left" cx="80" cy="18" r="6" fill="none" stroke="#FFFFFF" stroke-width="4"></circle>
                <circle class="edge_circ_right" cx="80" cy="8" r="6" fill="none" stroke="#FFFFFF" stroke-width="4"></circle>
                <path class="center_line" d="" fill="red" stroke="#FFFFFF" stroke-width="2"/>
                <circle class="idea_circ_1" cx="80" cy="128" r="0" fill="none" stroke="#FFFFFF" stroke-width="4"></circle>
                <circle class="idea_circ_2" cx="80" cy="259" r="0" fill="none" stroke="#FFFFFF" stroke-width="4"></circle>
                <circle class="idea_circ_3" cx="80" cy="439" r="0" fill="none" stroke="#FFFFFF" stroke-width="4"></circle>
                <g class="svgicon light_bulb">
                    <path
                        fill="white"
                        class="fill"
                        d="M54.8,27.7c0-15-12.2-27.2-27.2-27.2c-1,0-1.8,0.8-1.8,1.8s0.8,1.8,1.8,1.8c13,0,23.6,10.6,23.6,23.6
                                    c0,7.2-3.3,14.1-9,18.5h-1.6V22.8c0-0.7-0.4-1.3-1-1.6c-0.6-0.3-1.4-0.2-1.9,0.2l-10,7.8l-9.9-7.8c-0.5-0.4-1.3-0.5-1.9-0.2
                                    c-0.6,0.3-1,0.9-1,1.6v17.1c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V26.4l8.2,6.4c0.6,0.5,1.6,0.5,2.2,0l8.2-6.4v19.7h-24
                                    c-5.6-4.5-9-11.3-9-18.5c0-4.5,1.3-8.9,3.7-12.7c2.4-3.7,5.7-6.6,9.7-8.5C18.4,6,18.7,4.9,18.3,4c-0.4-0.9-1.5-1.3-2.4-0.8
                                    C11.4,5.4,7.5,8.8,4.8,13C2,17.4,0.5,22.5,0.5,27.7c0,8.1,3.6,15.8,9.8,20.9v9v9.6c0,1,0.8,1.8,1.8,1.8h31.3c1,0,1.8-0.8,1.8-1.8
                                    v-9.6v-9.2C51.3,43.3,54.8,35.7,54.8,27.7z M41.6,65.4H13.9v-6h16.9c1,0,1.8-0.8,1.8-1.8s-0.8-1.8-1.8-1.8H13.9v-6h27.7v7.8V65.4
                                    L41.6,65.4z"/>
                    <path
                        stroke="white"
                        fill="none"
                        stroke-width="2"
                        class="border"
                        d="M54.8,27.7c0-15-12.2-27.2-27.2-27.2c-1,0-1.8,0.8-1.8,1.8s0.8,1.8,1.8,1.8c13,0,23.6,10.6,23.6,23.6
                                    c0,7.2-3.3,14.1-9,18.5h-1.6V22.8c0-0.7-0.4-1.3-1-1.6c-0.6-0.3-1.4-0.2-1.9,0.2l-10,7.8l-9.9-7.8c-0.5-0.4-1.3-0.5-1.9-0.2
                                    c-0.6,0.3-1,0.9-1,1.6v17.1c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V26.4l8.2,6.4c0.6,0.5,1.6,0.5,2.2,0l8.2-6.4v19.7h-24
                                    c-5.6-4.5-9-11.3-9-18.5c0-4.5,1.3-8.9,3.7-12.7c2.4-3.7,5.7-6.6,9.7-8.5C18.4,6,18.7,4.9,18.3,4c-0.4-0.9-1.5-1.3-2.4-0.8
                                    C11.4,5.4,7.5,8.8,4.8,13C2,17.4,0.5,22.5,0.5,27.7c0,8.1,3.6,15.8,9.8,20.9v9v9.6c0,1,0.8,1.8,1.8,1.8h31.3c1,0,1.8-0.8,1.8-1.8
                                    v-9.6v-9.2C51.3,43.3,54.8,35.7,54.8,27.7z M41.6,65.4H13.9v-6h16.9c1,0,1.8-0.8,1.8-1.8s-0.8-1.8-1.8-1.8H13.9v-6h27.7v7.8V65.4
                                    L41.6,65.4z"/>
                </g>
                <g class="svgicon falling_numbers">
                    <g class="fill" fill="white" stroke="none">
                        <path d="M5.6,40.1c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V25.9c0-0.7-0.4-1.4-1.1-1.7c-0.7-0.3-1.5-0.1-2,0.4l-4.6,4.6
                                        c-0.7,0.7-0.7,1.9,0,2.6s1.9,0.7,2.6,0l1.5-1.5V40.1z"/>
                        <path d="M16.3,8.7l1.5-1.5V17c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V2.8c0-0.7-0.4-1.4-1.1-1.7c-0.7-0.3-1.5-0.1-2,0.4
                                        l-4.6,4.6C13,6.8,13,8,13.7,8.7S15.6,9.4,16.3,8.7z"/>
                        <path d="M36.9,24.2c-0.7-0.3-1.5-0.1-2,0.4l-4.6,4.6c-0.7,0.7-0.7,1.9,0,2.6s1.9,0.7,2.6,0l1.5-1.5v9.8
                                        c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V25.9C38.1,25.2,37.6,24.5,36.9,24.2z"/>
                        <path d="M29.1,46.5c-0.7-0.3-1.5-0.1-2,0.4l-4.6,4.6c-0.7,0.7-0.7,1.9,0,2.6s1.9,0.7,2.6,0l1.5-1.5V58
                                        c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8v-9.7C30.2,47.5,29.7,46.8,29.1,46.5z"/>
                        <path
                            d="M26,35.6v-5.3c0-3.4-2.8-6.2-6.2-6.2h-0.1c-3.4,0-6.2,2.8-6.2,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1
                                        C23.2,41.9,26,39.1,26,35.6z M22.4,35.6c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6v-5.3c0-1.4,1.2-2.6,2.6-2.6h0.1
                                        c1.4,0,2.6,1.2,2.6,2.6V35.6z"/>
                        <path
                            d="M48.8,24.1L48.8,24.1c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1c3.4,0,6.2-2.8,6.2-6.2v-5.3
                                        C55,26.9,52.2,24.1,48.8,24.1z M51.4,35.6c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6v-5.3c0-1.4,1.2-2.6,2.6-2.6h0.1
                                        c1.4,0,2.6,1.2,2.6,2.6V35.6z"/>
                        <path
                            d="M32,1L32,1c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2H32c1,0,1.8-0.8,1.8-1.8S33,15.1,32,15.1h-0.1
                                        c-1.4,0-2.6-1.2-2.6-2.6V7.2c0-1.4,1.2-2.6,2.6-2.6H32c1.4,0,2.6,1.2,2.6,2.6c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8
                                        C38.2,3.8,35.4,1,32,1z"/>
                        <path
                            d="M48.8,1L48.8,1c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1c3.4,0,6.2-2.8,6.2-6.2V7.2
                                        C55,3.8,52.2,1,48.8,1z M51.4,12.6c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6V7.3c0-1.4,1.2-2.6,2.6-2.6h0.1
                                        c1.4,0,2.6,1.2,2.6,2.6V12.6z"/>
                        <path
                            d="M11.3,46.4L11.3,46.4c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1c3.4,0,6.2-2.8,6.2-6.2v-5.3
                                        C17.5,49.2,14.7,46.4,11.3,46.4z M13.9,57.9c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6v-5.3c0-1.4,1.2-2.6,2.6-2.6h0.1
                                        c1.4,0,2.6,1.2,2.6,2.6V57.9z"/>
                    </g>
                    <g class="border" stroke="red" stroke-width="2" fill="none">
                        <path d="M5.6,40.1c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V25.9c0-0.7-0.4-1.4-1.1-1.7c-0.7-0.3-1.5-0.1-2,0.4l-4.6,4.6
                                        c-0.7,0.7-0.7,1.9,0,2.6s1.9,0.7,2.6,0l1.5-1.5V40.1z"/>
                        <path d="M16.3,8.7l1.5-1.5V17c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V2.8c0-0.7-0.4-1.4-1.1-1.7c-0.7-0.3-1.5-0.1-2,0.4
                                        l-4.6,4.6C13,6.8,13,8,13.7,8.7S15.6,9.4,16.3,8.7z"/>
                        <path d="M36.9,24.2c-0.7-0.3-1.5-0.1-2,0.4l-4.6,4.6c-0.7,0.7-0.7,1.9,0,2.6s1.9,0.7,2.6,0l1.5-1.5v9.8
                                        c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V25.9C38.1,25.2,37.6,24.5,36.9,24.2z"/>
                        <path d="M29.1,46.5c-0.7-0.3-1.5-0.1-2,0.4l-4.6,4.6c-0.7,0.7-0.7,1.9,0,2.6s1.9,0.7,2.6,0l1.5-1.5V58
                                        c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8v-9.7C30.2,47.5,29.7,46.8,29.1,46.5z"/>
                        <path
                            d="M26,35.6v-5.3c0-3.4-2.8-6.2-6.2-6.2h-0.1c-3.4,0-6.2,2.8-6.2,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1
                                        C23.2,41.9,26,39.1,26,35.6z M22.4,35.6c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6v-5.3c0-1.4,1.2-2.6,2.6-2.6h0.1
                                        c1.4,0,2.6,1.2,2.6,2.6V35.6z"/>
                        <path
                            d="M48.8,24.1L48.8,24.1c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1c3.4,0,6.2-2.8,6.2-6.2v-5.3
                                        C55,26.9,52.2,24.1,48.8,24.1z M51.4,35.6c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6v-5.3c0-1.4,1.2-2.6,2.6-2.6h0.1
                                        c1.4,0,2.6,1.2,2.6,2.6V35.6z"/>
                        <path
                            d="M32,1L32,1c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2H32c1,0,1.8-0.8,1.8-1.8S33,15.1,32,15.1h-0.1
                                        c-1.4,0-2.6-1.2-2.6-2.6V7.2c0-1.4,1.2-2.6,2.6-2.6H32c1.4,0,2.6,1.2,2.6,2.6c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8
                                        C38.2,3.8,35.4,1,32,1z"/>
                        <path
                            d="M48.8,1L48.8,1c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1c3.4,0,6.2-2.8,6.2-6.2V7.2
                                        C55,3.8,52.2,1,48.8,1z M51.4,12.6c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6V7.3c0-1.4,1.2-2.6,2.6-2.6h0.1
                                        c1.4,0,2.6,1.2,2.6,2.6V12.6z"/>
                        <path
                            d="M11.3,46.4L11.3,46.4c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1c3.4,0,6.2-2.8,6.2-6.2v-5.3
                                        C17.5,49.2,14.7,46.4,11.3,46.4z M13.9,57.9c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6v-5.3c0-1.4,1.2-2.6,2.6-2.6h0.1
                                        c1.4,0,2.6,1.2,2.6,2.6V57.9z"/>
                    </g>
                </g>
                <g class="svgicon shopping_cart">
                    <g class="fill" fill="white" stroke="none">
                        <path
                            d="M70.7,21.3c-0.3-0.5-0.9-0.8-1.5-0.8h-8c-1,0-1.8,0.8-1.8,1.8c0,1,0.8,1.8,1.8,1.8h5.2L56.1,47.3H26.5
                                        L14,21.5c-0.3-0.6-0.9-1-1.6-1H2.8c-1,0-1.8,0.8-1.8,1.8c0,1,0.8,1.8,1.8,1.8h8.4l12.5,25.8c0.3,0.6,0.9,1,1.6,1h32
                                        c0.7,0,1.4-0.4,1.6-1.1L70.8,23C71.1,22.5,71,21.8,70.7,21.3z"/>
                        <path d="M27.9,53.3c-3.4,0-6.2,2.8-6.2,6.2s2.8,6.2,6.2,6.2s6.2-2.8,6.2-6.2C34.1,56.1,31.4,53.3,27.9,53.3z
                                        M27.9,62.2c-1.4,0-2.6-1.2-2.6-2.6c0-1.4,1.2-2.6,2.6-2.6s2.6,1.2,2.6,2.6C30.5,61,29.4,62.2,27.9,62.2z"/>
                        <path d="M54.1,53.3c-3.4,0-6.2,2.8-6.2,6.2s2.8,6.2,6.2,6.2c3.4,0,6.2-2.8,6.2-6.2C60.4,56.1,57.6,53.3,54.1,53.3z
                                        M54.1,62.2c-1.4,0-2.6-1.2-2.6-2.6c0-1.4,1.2-2.6,2.6-2.6c1.4,0,2.6,1.2,2.6,2.6C56.8,61,55.6,62.2,54.1,62.2z"/>
                        <path d="M37.2,38.2c10.3,0,18.6-8.3,18.6-18.6S47.5,1,37.2,1S18.6,9.3,18.6,19.6S27,38.2,37.2,38.2z M37.2,4.6
                                        c8.3,0,15,6.7,15,15s-6.7,15-15,15s-15-6.7-15-15S28.9,4.6,37.2,4.6z"/>
                        <path
                            d="M29.5,21.2c0,0.9,0.7,1.5,1.5,1.6c0.9,3.6,3.4,5.5,7.5,5.5c2.6,0,4.9-1,6.2-2.7c0.2-0.3,0.3-0.7,0.3-1
                                        c0-1-0.8-1.8-1.8-1.8c-0.5,0-0.9,0.2-1.4,0.7c-1,1-1.8,1.3-3.3,1.3c-1.8,0-2.8-0.6-3.3-1.9h2c0.9,0,1.6-0.7,1.6-1.6
                                        c0-0.7-0.4-1.3-1-1.5c0.6-0.2,1-0.8,1-1.5c0-0.9-0.7-1.6-1.6-1.6h-2c0.6-1.5,1.6-2.1,3.4-2.1c1.4,0,2.2,0.4,3.1,1.4
                                        c0.4,0.4,0.9,0.7,1.4,0.7c1,0,1.8-0.8,1.8-1.8c0-0.4-0.1-0.7-0.4-1.1c-0.7-1-2.4-2.8-6-2.8c-4.1,0-6.7,2-7.5,5.7
                                        c-0.8,0.1-1.5,0.7-1.5,1.6c0,0.7,0.4,1.3,1,1.5C29.9,19.9,29.5,20.5,29.5,21.2z"/>
                    </g>
                    <g class="border" stroke="red" stroke-width="2" fill="none">
                        <path
                            d="M70.7,21.3c-0.3-0.5-0.9-0.8-1.5-0.8h-8c-1,0-1.8,0.8-1.8,1.8c0,1,0.8,1.8,1.8,1.8h5.2L56.1,47.3H26.5
                                        L14,21.5c-0.3-0.6-0.9-1-1.6-1H2.8c-1,0-1.8,0.8-1.8,1.8c0,1,0.8,1.8,1.8,1.8h8.4l12.5,25.8c0.3,0.6,0.9,1,1.6,1h32
                                        c0.7,0,1.4-0.4,1.6-1.1L70.8,23C71.1,22.5,71,21.8,70.7,21.3z"/>
                        <path s d="M27.9,53.3c-3.4,0-6.2,2.8-6.2,6.2s2.8,6.2,6.2,6.2s6.2-2.8,6.2-6.2C34.1,56.1,31.4,53.3,27.9,53.3z
                                        M27.9,62.2c-1.4,0-2.6-1.2-2.6-2.6c0-1.4,1.2-2.6,2.6-2.6s2.6,1.2,2.6,2.6C30.5,61,29.4,62.2,27.9,62.2z"/>
                        <path d="M54.1,53.3c-3.4,0-6.2,2.8-6.2,6.2s2.8,6.2,6.2,6.2c3.4,0,6.2-2.8,6.2-6.2C60.4,56.1,57.6,53.3,54.1,53.3z
                                        M54.1,62.2c-1.4,0-2.6-1.2-2.6-2.6c0-1.4,1.2-2.6,2.6-2.6c1.4,0,2.6,1.2,2.6,2.6C56.8,61,55.6,62.2,54.1,62.2z"/>
                        <path d="M37.2,38.2c10.3,0,18.6-8.3,18.6-18.6S47.5,1,37.2,1S18.6,9.3,18.6,19.6S27,38.2,37.2,38.2z M37.2,4.6
                                        c8.3,0,15,6.7,15,15s-6.7,15-15,15s-15-6.7-15-15S28.9,4.6,37.2,4.6z"/>
                        <path
                            d="M29.5,21.2c0,0.9,0.7,1.5,1.5,1.6c0.9,3.6,3.4,5.5,7.5,5.5c2.6,0,4.9-1,6.2-2.7c0.2-0.3,0.3-0.7,0.3-1
                                        c0-1-0.8-1.8-1.8-1.8c-0.5,0-0.9,0.2-1.4,0.7c-1,1-1.8,1.3-3.3,1.3c-1.8,0-2.8-0.6-3.3-1.9h2c0.9,0,1.6-0.7,1.6-1.6
                                        c0-0.7-0.4-1.3-1-1.5c0.6-0.2,1-0.8,1-1.5c0-0.9-0.7-1.6-1.6-1.6h-2c0.6-1.5,1.6-2.1,3.4-2.1c1.4,0,2.2,0.4,3.1,1.4
                                        c0.4,0.4,0.9,0.7,1.4,0.7c1,0,1.8-0.8,1.8-1.8c0-0.4-0.1-0.7-0.4-1.1c-0.7-1-2.4-2.8-6-2.8c-4.1,0-6.7,2-7.5,5.7
                                        c-0.8,0.1-1.5,0.7-1.5,1.6c0,0.7,0.4,1.3,1,1.5C29.9,19.9,29.5,20.5,29.5,21.2z"/>
                    </g>
                </g>
            </g>
            </svg>

                <div class="integration__steps__item integration__steps__item--start"><i style="opacity: 1;">Start</i></div>
			<br>

			<p class="integration__steps__item integration__steps__item--bullet bullet_1" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">1</p>
			<div class="integration__steps__item integration__steps__item--ico">
				<img src="/wp-content/themes/hatch_fo/dist/assets/images/integration/integration_ico_discover.svg" width="166" height="200" alt="Discover">
			</div>
			<p class="integration__steps__item integration__steps__item--txt bullet_text_1" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"><i><span>1. </span><?php echo $custom_fields['step_1_graphic_item_1']; ?> </i></p>
			<br>

			<p class="integration__steps__item integration__steps__item--bullet bullet_2" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">2</p>
			<div class="integration__steps__item integration__steps__item--ico">
				<img src="/wp-content/themes/hatch_fo/dist/assets/images/integration/integration_ico_configure.svg" width="166" height="200" alt="Configure">
			</div>
			<p class="integration__steps__item integration__steps__item--txt bullet_text_2" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"><i><span>2 </span><?php echo $custom_fields['step_1_graphic_item_2']; ?></i></p>
			<br>

			<p class="integration__steps__item integration__steps__item--bullet bullet_3" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">3</p>
			<div class="integration__steps__item integration__steps__item--ico">
				<img src="/wp-content/themes/hatch_fo/dist/assets/images/integration/integration_ico_implement.svg" width="166" height="200" alt="Implement">
			</div>
			<p class="integration__steps__item integration__steps__item--txt bullet_text_3" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"><i><span>3. </span><?php echo $custom_fields['step_1_graphic_item_3']; ?></i>
			</p>
			<br>
			<div class="integration__steps__item integration__steps__item--end"><i style="opacity: 1;">Launch</i></div>
		</div>

	</div>
</section>


<div class="section__separators">
	<div class="section__pattern section__pattern--2">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>
</div>

<section class="section brands-section brands-section__step_section" id="brand-step2">
	<div class="section__pattern section__pattern--5 green">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>

	<div class="section__holder">
		<div class="section__header">
            <h1 class="section__header__title"></h1>
			<h2 class="section__header__subtitle shown"><span> <?php echo $custom_fields['step_2_sub_header']; ?></span></h2>
		</div>

		<div class="brands-step2__content">
			<p>
				<?php echo $custom_fields['step_2_text']; ?>
			</p>

			<?php
                if (isset($custom_fields['step_2_image']) && $custom_fields['step_2_image'] != '') {
                    echo '<img src="'.$custom_fields['step_2_image'].'" />';
                }
            ?>
		</div>

	</div>
</section>

<div class="section__separators">
	<div class="section__pattern section__pattern--2">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>
</div>

<section class="section brands-section brands-section-green brands-section__step_section" id="brand-step3">
	<div class="section__pattern section__pattern--4">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>

	<div class="section__holder">
		<div class="section__header">
			<h1 class="section__header__title"></h1>
			<h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['step_3_sub_header']; ?></span></h2>
		</div>

		<p>
			<?php echo $custom_fields['step_3_text']; ?>
		</p>

		<?php
            if (isset($custom_fields['step_3_graphic']) && $custom_fields['step_3_graphic'] != '') {
                echo '<img src="'.$custom_fields['step_3_graphic'].'" />';
            }
        ?>

		<div class="brand-step3__block">
			<div class="brand-step3__block_title">
				<?php echo $custom_fields['step_3_block_title']; ?>
			</div>
			<div class="brand-step3__block_content">
				<ul>
				<?php
                if (isset($custom_fields['step3_block_list_items'])) {
                    foreach ($custom_fields['step3_block_list_items'] as $key => $item) {
                        echo '<li>'.$item['text'].'</li>';
                    }
                }
                ?>
				</ul>
			</div>
			<div class="brand-step3_logos">
				<?php
                if (isset($custom_fields['step_3_block_logos'])) {
                    foreach ($custom_fields['step_3_block_logos'] as $key => $item) {
                        echo '<div class="brand-step3_logo_block">';

                        if (isset($item['image']) && $item['image'] != '') {
                            echo '<img src="'.$item['image'].'" />';
                        }

                        echo '<span>'.$item['text'].'</span>
						</div>';
                    }
                }
                ?>
			</div>
		</div>

	</div>
</section>

<section class="section brands-section brands-section__step_section" id="brand-step4">
	<div class="section__pattern section__pattern--5 green">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>

	<div class="section__holder">
		<div class="section__header">
			<h1 class="section__header__title"></h1>
			<h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['step_4_sub_header_1']; ?></span></h2>
		</div>

		<p>
			<?php echo $custom_fields['step_4_text']; ?>
		</p>

		<div class="brand-step4_highlights">
			<?php
            if (isset($custom_fields['step_4_blocks'])) {
                foreach ($custom_fields['step_4_blocks'] as $key => $item) {
                    echo '<div class="brand-step4_highlight_block">

							<div class="brand-step4_highlight_block__title">
								'.$item['title'].'
							</div>
							<div class="brand-step4_highlight_block__desc">
								'.$item['text'].'
							</div>

						</div>';
                }
            }
            ?>
		</div>
		<div class="section__header">
			<h1 class="section__header__title"></h1>
			<h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['step_4_sub_header_2']; ?></span></h2>
		</div>


		<div class="section__content integration__cards">
			<?php
            if (isset($custom_fields['step_4_case_studies'])) {
                foreach ($custom_fields['step_4_case_studies'] as $key => $item) {
                    echo '<div class="integration__cards__card">
							<div class="integration__cards__card__top" data-img="'.$item['image'].'" style="background-image: url('.$item['image'].');">
								<img src="'.$item['image'].'" alt="" class="no-display">
							</div>
							<div class="integration__cards__card__bottom">
								<p class="integration__cards__card__text">'.$item['text'].'</p>
							</div>
							<button class="section__smallbtn" data-title="'.$item['title'].'"><span>Request PDF</span><svg height="40" version="1.1" width="180" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: absolute; left: 0px; top: 0px;" viewBox="0 0 180 40" preserveAspectRatio="xMinYMin"><rect x="0" y="-40" width="180" height="0" rx="10" ry="10" fill="rgb(77,77,77)" stroke="none" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" stroke-width="1"></rect></svg></button>
						</div>';
                }
            }

            ?>
		</div>
		<?php if (isset($custom_fields['step_4_sub_header_3']) && $custom_fields['step_4_sub_header_3'] !== '') {
                ?>
		<div class="section__header">
			<h1 class="section__header__title"></h1>
			<h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['step_4_sub_header_3']; ?></span></h2>
		</div>
        <?php

            }
        if (isset($custom_fields['step_4_logos']) && $custom_fields['step_4_logos'] !== '0' && count($custom_fields['step_4_logos']) > 0) {
            echo '<div class="section__content integration__companies">';

            foreach ($custom_fields['step_4_logos'] as $key => $item) {
                echo '<div class="integration__companies__logo"><img class="grayscale grayscale-fade" src="'.$item['logo'].'" alt=""></div>';
            }

            echo '</div>';
        }
        ?>

	</div>
	<div class="videoId"><?php echo $custom_fields['video_background']; ?></div>
</section>
