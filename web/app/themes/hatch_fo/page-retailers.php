<?php

// Default field values
$custom_fields = array();

// About Us (133)
$args = array(
	'include' => 506,
	'post_type' => 'page',
	'post_status' => 'publish'
);

$posts_array = get_pages($args);

if (count($posts_array) === 1) {
	$post = $posts_array[0];
	$custom_fields = get_fields($post->ID);
}

?>

<section class="full-page-header">
	<div class="full-page-header-content">
		<h1><?php echo $custom_fields['header_title']; ?></h1>
		<h3><span class="full-page-header-date"><?php echo $custom_fields['header_subtitle']; ?></span></h3>
	</div>

    <div class="full-page-header-bg video-background"
        <?php
            if (isset($custom_fields['header_background_image']) && $custom_fields['header_background_image'] != '') {
                echo 'style="background-image: url('.$custom_fields['header_background_image'].')"';
            }
        ?>
    >
    <?php
	if(!IS_TABLET) {
	    echo '<div class="video-foreground " id="myVideo">
	        <iframe id="myVideo" width="100%" height="100%"
	        src="https://www.youtube.com/embed/YaAwSaMYDyU?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=0&amp;volume=0"
	        frameborder="0"  allowfullscreen">
	        </iframe>
	    </div>';
	}
	?>

		</div>
</section>

<section class="section retailers-section retailers-section__top_section">
	<div class="news__section__pattern news__section__pattern--bottom news-page-top-pattern"></div>

	<div class="section__holder">
        <div class="section__header">
    		<h1 class="section__header__title"><?php echo $custom_fields['intro_header']; ?></h1>
        </div>
        <?php if ($custom_fields['intro_text_col_1'] !== '' && $custom_fields['intro_text_col_2' !== '']) { ?>
    		<div class="retailers-intro-cols">
    			<p>
    				<?php echo $custom_fields['intro_text_col_1']; ?>
    			</p>

    			<p>
    				<?php echo $custom_fields['intro_text_col_2']; ?>
    			</p>
    		</div>
        <?php } ?>

		<div class="section__header">
			<h1 class="section__header__title"></h1>
			<h2 class="section__header__subtitle shown">
				<span><?php echo $custom_fields['intro_sub_header']; ?></span>
			</h2>
		</div>

		<p>
			<?php echo $custom_fields['intro_text_2']; ?>
		</p>

		<?php
			if (isset($custom_fields['intro_image']) && $custom_fields['intro_image'] != '') {
				echo '<img src="'.$custom_fields['intro_image'].'" class="retailers-top__big-image" />';
			}
		?>

		<?php if(isset($custom_fields['logos_intro']) && $custom_fields['logos_intro'] !== '') { ?>
            <div class="section__header">
    			<div class="section__header__title"></div>
    			<h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['logos_intro']; ?></span></h2>
    		</div>
        <?php } ?>

		<div class="retailers-intro-logos">
			<?php
			if(isset($custom_fields['intro_logos'])) {

				foreach ($custom_fields['intro_logos'] as $key => $item) {
					echo '<div class="retailers-intro-logo-block">';

					if (isset($item['link']) && $item['link'] != '') {
						echo '<a href="'.$item['link'].'">';
					}

					if (isset($item['image']) && $item['image'] != '') {
						echo '<img src="'.$item['image'].'" />';
					}

					if (isset($item['link']) && $item['link'] != '') {
						echo '</a>';
					}

					echo '</div>';
				}
			}
			?>
		</div>


		<div class="retailers__hightlight_blocks">
		<?php
			if(isset($custom_fields['intro_blocks'])) {

				foreach ($custom_fields['intro_blocks'] as $key => $block) {

					echo '<div class="retailers__hightlight_block">
						<div class="retailers__hightlight_block__title">
							'.$block['title'].'
						</div>
						<div class="retailers__hightlight_block__desc">
							'.$block['text'].'
						</div>
					</div>';
				}
			}

		?>
		</div>

		<div class="brands-list">
			<div class="brands-list-title">
				<?php echo $custom_fields['intro_list_title']; ?>
			</div>
			<div class="brands-list-items">
				<ul>
				<?php
				if(isset($custom_fields['intro_list_item'])) {

					foreach ($custom_fields['intro_list_item'] as $key => $item) {
						echo '<li>'.$item['intro_list_item'].'</li>';
					}
				}
				?>
				</ul>
			</div>
		</div>

		<div class="partners__content__quote">
			<div class="quote_image_holder">
				<?php
					if (isset($custom_fields['why_quote_date_image']) && $custom_fields['why_quote_date_image'] != '') {
						echo '<img src="'.$custom_fields['why_quote_date_image'].'" />';
					}
				?>
			</div>
			<div class="quote_content_holder">
				<blockquote>
					<?php echo $custom_fields['why_quote_text']; ?>
				</blockquote>

				<p>
					<span class="quote-name"><?php echo $custom_fields['why_quote_name']; ?></span>
					<span class="quote-date">- <?php echo $custom_fields['why_quote_date']; ?></span>
				</p>
			</div>
		</div>

		<?php
			if(isset($custom_fields['intro_last_block'])) {

				foreach ($custom_fields['intro_last_block'] as $key => $block) {

					echo '<div class="brands-list">
						<div class="brands-list-title">
							'.$block['title'].'
						</div>
						<div class="brands-list-items">
							<ul>';
							foreach ($block['list_items'] as $li) {
								echo '<li>'.$li['text'].'</li>';
							}
						echo '</ul>
						</div>
					</div>';
				}
			}

		?>


	</div>
</section>

<div class="section__separators">
	<div class="section__arrow section__arrow--intro" data-id="retailers-why">
		<div class="section__arrow__bg white-bg"></div>
		<span class="white-bg">Why use Hatch?</span>
	</div>
</div>

<section class="section retailers-section retailers-section-green retailers-section__step_section" id="retailers-why">
	<div class="section__pattern section__pattern--4">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>

	<div class="section__holder">
		<div class="section__header">
			<h1 class="section__header__title"><?php echo $custom_fields['why_header']; ?></h1>
			<h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['why_sub_header_1']; ?></span></h2>
		</div>

		<div class="retailers__why_blocks">
		<?php
			if(isset($custom_fields['why_blocks'])) {

				foreach ($custom_fields['why_blocks'] as $key => $block) {

					echo '<div class="retailers__why_block">
						<div class="retailers__why_block__desc">
							'.$block['text'].'
						</div>
					</div>';
				}
			}

		?>
		</div>
	</div>
</section>
<?php if ($custom_fields['why_sub_header2'] !== '' || $custom_fields['why_text_1'] !== '' || isset($custom_fields['why_logos_1'][0]->image)) { ?>
<section class="section retailers-section retailers-section__bottom_section">
	<div class="section__pattern section__pattern--5">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>

	<div class="retailers-section retailers-join-us-container">
		<div class="retailers-join-us-container__first-wrapper">
			<div class="section__header">
				<h1 class="section__header__title"></h1>
				<h2 class="section__header__subtitle shown"><span><?php echo $custom_fields['why_sub_header2']; ?></span></h2>
			</div>
			<div class="retailers-join-us-container__text-container">
				<p><?php echo $custom_fields['why_text_1']; ?></p>
			</div>

			<div class="retailers-why-logos">
				<?php
				if(isset($custom_fields['why_logos_1'])) {

					foreach ($custom_fields['why_logos_1'] as $key => $item) {
						echo '<div class="retailers-why-logo-block">';

						if (isset($item['link']) && $item['link'] != '') {
							echo '<a href="'.$item['link'].'">';
						}

						if (isset($item['image']) && $item['image'] != '') {
							echo '<img src="'.$item['image'].'" />';
						}

						if (isset($item['link']) && $item['link'] != '') {
							echo '</a>';
						}

						echo '</div>';
					}
				}
				?>
			</div>
		</div>
	</div>
</section>
<?php } ?>
<div class="videoId"><?php echo $custom_fields['video_background']; ?></div>
