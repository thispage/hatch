HATCH
==

Frontend installation
--
> npm install -g bower 
> npm install -g babel
> npm install

> bower install

Backend setup
--
vhost: hatch.dev pointing to src/public_html/
database: thispage_hatch
wordpress setup: in wp-config.php, fill in the right db info

Init
--
> gulp watch
