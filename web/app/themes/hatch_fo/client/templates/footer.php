<?php
// Get In Touch (192)
$args = array(
	'include' => 192,
	'post_type' => 'page',
	'post_status' => 'publish'
);
$posts_array = get_pages($args);

if (count($posts_array) === 1) {
	$post = $posts_array[0];

	$custom_fields = get_fields($post->ID);

	// print_r($custom_fields);
	// die();

	$fields = [];
	$fields['get-in-touch'] = [];

	$fields['get-in-touch']['title']		= $custom_fields['title'];
	$fields['get-in-touch']['subtitle'] 	= $custom_fields['subtitle'];
	// $fields['get-in-touch']['navigation'] 	= $custom_fields['navigation'];
	$fields['get-in-touch']['shortcode'] 	= $custom_fields['shortcode'];
	$fields['get-in-touch']['our_offices_subtitle'] 	= $custom_fields['our_offices_subtitle'];
	$fields['get-in-touch']['offices'] 		= $custom_fields['offices'];
}
?>

<?php
if (FILENAME != 'page-contact-us') {
?>
<div class="section__separators">
		<div data-id="section_contact" class="section__arrow section__arrow--getintouch section__arrow--intro footer_mobile">
			<div class="section__arrow__bg section__arrow__bg--footer white"></div>
			<span>Get in touch</span>
		</div>
		<div class="section__pattern section__pattern--5 footer__pattern--top top_pattern green ">
			<div class="section__pattern__part section__pattern__part--top"></div>
			<div class="section__pattern__part section__pattern__part--bottom"></div>
		</div>
	</div>
<?php } ?>

<?php
if (FILENAME == 'page-brands') {
?>
<div class="section__separators">
		<div data-id="section_contact" class="section__arrow section__arrow--intro footer_mobile">
			<div class="section__arrow__bg section__arrow__bg--footer white"></div>
			<span>Get in touch</span>
		</div>
		<div class="section__pattern section__pattern--4 footer__pattern--top top_pattern ">
			<div class="section__pattern__part section__pattern__part--top"></div>
			<div class="section__pattern__part section__pattern__part--bottom"></div>
		</div>
	</div>
<?php } ?>

<?php
if (FILENAME == 'page-retailers') {
?>
<div class="section__separators">
		<div data-id="section_contact" class="section__arrow section__arrow--intro footer_mobile">
			<div class="section__arrow__bg section__arrow__bg--footer white"></div>
			<span>Get in touch</span>
		</div>
		<div class="section__pattern section__pattern--4 footer__pattern--top top_pattern ">
			<div class="section__pattern__part section__pattern__part--top"></div>
			<div class="section__pattern__part section__pattern__part--bottom"></div>
		</div>
	</div>
<?php } ?>


<section class="section section--contact contact" id="section_contact">

	<?php
	if (FILENAME == 'page-contact-us') {
	?>
	<div class="section__pattern section__pattern--6 top_pattern contact_pattern">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>
	<?php } ?>

	<div class="section__holder">
		<header class="section__header section__header--contact">
			<h1 class="section__header__title"><span><?=strtoupper($fields['get-in-touch']['title'])?></span></h1>
			<h2 class="section__header__subtitle"><span><?=$fields['get-in-touch']['subtitle']?></span></h2>
		</header>

		<div class="section__content contact__form">
			<?=do_shortcode($fields['get-in-touch']['shortcode'])?>
		</div>
	</div>

	<?php
	if (FILENAME == 'page-contact-us') {
	?>
	<div class="section__holder">
        <div class="section__header">
            <h1 class="section__header__title"></h1>
            <h2 class="section__header__subtitle shown"><span><?=$fields['get-in-touch']['our_offices_subtitle']?></span></h2>
        </div>
	</div>

			<div class="contact__map-wrap">
				<div class="contact__map" id="contactMap"></div>
			</div>
	<div class="section__holder">
			<div class="contact__offices">
			<?php
				if (isset($fields['get-in-touch']['offices']) && !empty($fields['get-in-touch']['offices']) && count($fields['get-in-touch']['offices']) > 0) {

					foreach ($fields['get-in-touch']['offices'] as $index => $office) {

						echo '<div class="contact__offices__address"'.(isset($office['location']) && isset($office['location']['lat']) && $office['location']['lat'] != '' ? ' data-lat="'.$office['location']['lat'].'"' : '').''.(isset($office['location']) && isset($office['location']['lng']) && $office['location']['lng'] != '' ? ' data-lng="'.$office['location']['lng'].'"' : '').'>
							<h3>'.$office['title'].'</h3>
							<p>'.$office['address'].'</p>
							<p>Email: <a href="mailto:'.$office['email'].'">'.$office['email'].'</a></p>
						</div>';

					}
				}
			?>
			</div>
	    <?php } ?>
	</div>


	<div class="section__pattern section__pattern--6 section__pattern--footer">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>

	<footer class="section--footer footer" id="section_footer">
		<div class="footer__logo"></div>
		<div class="footer__text">
			<span>&copy; <?php echo date('Y'); ?> HATCH B.V., all rights reserved</span>
<!--			<span><a href="https://iceleads.workable.com/">Careers</a></span>-->
			<span><a href="/terms">terms of use</a></span>
			<span><a href="/privacy">privacy policy</a></span>
			<span><a href="/about-us">about</a></span>
			<?php if (!IS_MOBILE) { echo '<span><a href="/where-to-buy-solutions/faq">FAQ</a></span>'; } ?>
			<span><a href="/contact-us">contact</a></span>
		</div>
		<div class="footer__social">
			<div class="footer__social__icon footer__social__icon--twitter"><a href="http://www.twitter.com/gethatch" target="_blank"></a></div>
			<div class="footer__social__icon footer__social__icon--linkedin"><a href="https://www.linkedin.com/company/gethatch-com" target="_blank"></a></div>
		</div>
	</footer>
</section>
