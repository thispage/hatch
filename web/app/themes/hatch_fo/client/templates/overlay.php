
<div class="overlay" id="overlay-container">
	<?php if(FILENAME === 'index'): ?>
		<span class="overlay__gallery no-display">
			<svg class="overlay__background" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 520 500" preserveAspectRatio="xMinYMin meet"  xml:space="preserve" width="520" height="500">
				<polygon class="polygon-cache" points="0,0 0,0 0,0 0,0"/>
			</svg>
			<button class="overlay__close"></button>
			<span class="overlay__gallery__content overlay__content">
				<button class="overlay__next"></button>
				<button class="overlay__prev"></button>

				<svg version="1.1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px" viewBox="0 0 1024 560" style="enable-background:new 0 0 1024 560;" xml:space="preserve" width="1024" height="560">
					 <defs>
				    </defs>
				    <g>
				    </g>

				</svg>
			</span>
		</span>

		<?php
		// Our Where To Buy Solution
		$args = array(
			'include' => 59,
			'post_type' => 'page',
			'post_status' => 'publish'
		);
		$posts_array = get_pages($args);

		if (count($posts_array) === 1) {
			$post = $posts_array[0];

			$custom_fields = get_fields($post->ID);

			$vimeo_link = $custom_fields['vimeo_link'];
		}
		?>

		<div class="overlay__video no-display">
			<svg class="overlay__background" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 520 500" preserveAspectRatio="xMinYMin meet"  xml:space="preserve" width="520" height="500">
				<polygon class="polygon-cache" points="0,0 0,0 0,0 0,0"/>
			</svg>
			<button class="overlay__close"></button>
			<span class="overlay__video__content overlay__content">
				<span class="loader">
					<svg width='30px' height='30px' viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-pacman"><rect x="0" y="0" width="100" height="100" fill="rgba(0,0,0,0)" class="bk"></rect><path d="M0 50A50 50 0 1 0 100 50" fill="#ffffff" transform="rotate(30 50 50)"><animateTransform attributeName="transform" type="rotate" dur="0.5s" repeatCount="indefinite" from="30 50 50" to="30 50 50" values="30 50 50;0 50 50;30 50 50"></animateTransform></path><path d="M0 50A50 50 0 1 1 100 50" fill="#ffffff" transform="rotate(-30 50 50)"><animateTransform attributeName="transform" type="rotate" dur="0.5s" repeatCount="indefinite" from="-30 50 50" to="-30 50 50" values="-30 50 50;0 50 50;-30 50 50"></animateTransform></path></svg>
				</span>
				<iframe src="" data-link="<?php echo $vimeo_link . "?autoplay=1"; ?>" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</span>
		</div>

		<div class="overlay__textbox no-display">
			<svg class="overlay__background" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 520 500"  xml:space="preserve" width="520" height="500">
				<polygon class="polygon-cache" points="0,0 0,0 0,0 0,0"/>
			</svg>
			<span class="overlay__textbox__content overlay__content">
				<h2>COPY PAGE TEMPLATE</h2>
				<span>Constituendi autem sunt qui sint in amicitia fines et quasi termini diligendi. De quibus tres video sententias ferri, quarum nullam probo, unam, ut eodem modo erga amicum adfecti simus, quo erga nosmet ipsos, alteram, ut nostra in amicos benevolentia illorum erga nos benevolentiae pariter aequaliterque respondeat, tertiam, ut, quanti quisque se ipse facit, tanti fiat ab amicis.</span>
				<h3>Saepissime igitur mihi de amicitia cogitanti</h3>
				<span>Constituendi autem sunt qui sint in amicitia fines et quasi termini diligendi. De quibus tres video sententias ferri, quarum nullam probo, unam, ut eodem modo erga amicum adfecti simus, quo erga nosmet ipsos, alteram, ut nostra in amicos benevolentia illorum erga nos benevolentiae pariter aequaliterque respondeat, tertiam, ut, quanti quisque se ipse facit, tanti fiat ab amicis.</span>
				<h3>Saepissime igitur mihi de amicitia cogitanti</h3>
				<span>Constituendi autem sunt qui sint in amicitia fines et quasi termini diligendi. De quibus tres video sententias ferri, quarum nullam probo, unam, ut eodem modo erga amicum adfecti simus, quo erga nosmet ipsos, alteram, ut nostra in amicos benevolentia illorum erga nos benevolentiae pariter aequaliterque respondeat, tertiam, ut, quanti quisque se ipse facit, tanti fiat ab amicis.</span>
				<h3>Saepissime igitur mihi de amicitia cogitanti</h3>
				<span>Constituendi autem sunt qui sint in amicitia fines et quasi termini diligendi. De quibus tres video sententias ferri, quarum nullam probo, unam, ut eodem modo erga amicum adfecti simus, quo erga nosmet ipsos, alteram, ut nostra in amicos benevolentia illorum erga nos benevolentiae pariter aequaliterque respondeat, tertiam, ut, quanti quisque se ipse facit, tanti fiat ab amicis.</span>
				<h3>Saepissime igitur mihi de amicitia cogitanti</h3>
			</span>
		</div>

	<?php else: ?>

		<?php
			$page_id = $wp_query->get_queried_object_id();

			$args = array(
				'include' => $page_id,
				'post_type' => 'page',
				'post_status' => 'publish'
			);
			$posts_array = get_pages($args);

			if (count($posts_array) === 1) {
				$post = $posts_array[0];
				$custom_fields = get_fields($post->ID);
			}
		?>

		<div class="overlay__download no-display">
			<?php echo get_the_ID() === 510; ?>
			<svg class="overlay__background" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 520 500" preserveAspectRatio="xMinYMin meet"  xml:space="preserve" width="520" height="500">
				<polygon class="polygon-cache" points="0,0 0,0 0,0 0,0"/>
			</svg>
			<button class="overlay__close"></button>
			<span class="overlay__download__content overlay__content">
				<h3>Request PDF</h3>
				<?=do_shortcode($custom_fields['download_pdf_shortcode'])?>
			</span>
		</div>
	<?php
		$c_fields = array(
			'id' => 216,
			'shortcode' => ''
		);


		$args = array(
			'include' => $c_fields['id'],
			'post_type' => 'page',
			'post_status' => 'publish'
		);

		$posts_array = get_pages($args);

		if (count($posts_array) === 1) {
			$post = $posts_array[0];


			$custom_fields = get_fields($post->ID);

			$c_fields['shortcode']	= $custom_fields['shortcode'];
		}
	?>

		<div class="overlay__getstarted no-display">
			<svg class="overlay__background" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="0 0 520 500"  xml:space="preserve" width="520" height="500" preserveAspectRatio="xMaxYMax meet">
				<polygon class="polygon-cache" points="0,0 0,0 0,0 0,0"/>
			</svg>
			<button class="overlay__close"></button>
			<span class="overlay__getstarted__content overlay__content">

					<h3>Get started</h3>

					<div class="get-started-form">
					<?php
					if (isset($c_fields['shortcode']) && $c_fields['shortcode'] != '') {
						echo do_shortcode($c_fields['shortcode']);
					} ?>
					</div>
			</span>
		</div>


		<div class="overlay__readmore no-display">
			<svg class="overlay__background" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 520 500"  xml:space="preserve" width="520" height="500">
				<polygon class="polygon-cache" points="0,0 0,0 0,0 0,0"/>
			</svg>
			<button class="overlay__close"></button>
			<span class="overlay__readmore__content overlay__content">

				<h2 class="title"> TITLE</h2>

				<span class="date">date</span>

				<div class="text">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde qui quas molestiae accusantium soluta vero est dolor, commodi eligendi ratione reprehenderit numquam dolore repellendus dolorem quidem dicta animi doloribus omnis!
				</div>

			</span>
		</div>
	<?php endif; ?>

</div>
