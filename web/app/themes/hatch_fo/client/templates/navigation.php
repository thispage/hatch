<?php
	// Default nav itemst copy
	$nav = array(
		'home' => 'Home',
		'about_us' => 'About Us',
		'solutions' => 'Solutions',
		'news' => 'News',
		'blog' => 'Blog',

		'partners' => 'Partners',

		'contact' => 'Contact',
		'brands' => 'Brands',
		'retailers' => 'Retailers',
		'agencies' => 'Agencies',
	);
	// Get WordPress content for the navigation items

	// About us
	$args = array(
		'include' => 133,
		'post_type' => 'page',
		'post_status' => 'publish'
	);
	$posts_array = get_pages($args);

	if (count($posts_array) === 1) {
		$post = $posts_array[0];
		$custom_fields 		= get_fields($post->ID);
		//$nav['about_us'] 	= $custom_fields['navigation'];
	}

	// Solutions
	$args = array(
		'include' => 67,
		'post_type' => 'page',
		'post_status' => 'publish'
	);
	$posts_array = get_pages($args);

	if (count($posts_array) === 1) {
		$post = $posts_array[0];
		$custom_fields = get_fields($post->ID);
		//$nav['solutions'] = $custom_fields['navigation'];
	}

	// Get In Touch
	$args = array(
		'include' => 192,
		'post_type' => 'page',
		'post_status' => 'publish'
	);
	$posts_array = get_pages($args);

	if (count($posts_array) === 1) {
		$post = $posts_array[0];
		$custom_fields = get_fields($post->ID);
		//$nav['get_in_touch'] = $custom_fields['navigation'];
	}

	echo '<nav id="navigation" class="navigation">
		<div class="navigation__bar"></div>
		<ul class="navigation__menu navigation__menu--left">

			<li class="navigation__menu__item navigation__menu__item--logo"><a class="navigation__menu__item__link" href="/"></a></li>

			<li class="navigation__menu__item "><a class="navigation__menu__item__link' . (FILENAME == 'page-about-us' ? ' active-menu-item' : '') . '" href="/about-us">'.$nav["about_us"] . '</a></li>
            '.(!IS_MOBILE ? '<li class="navigation__menu__item"><a class="navigation__menu__item__link'.(FILENAME == 'page-brands' ? ' active-menu-item' : '').'" href="/where-to-buy-solutions/brands">'.$nav["brands"].'</a></li>' : '' ).'
            '.(!IS_MOBILE ? '<li class="navigation__menu__item"><a class="navigation__menu__item__link'.(FILENAME == 'page-retailers' ? ' active-menu-item' : '').'" href="/where-to-buy-solutions/retailers">'.$nav["retailers"].'</a></li>' : '' ).'
            '.(!IS_MOBILE ? '<li class="navigation__menu__item"><a class="navigation__menu__item__link'.(FILENAME == 'page-agencies' ? ' active-menu-item' : '').'" href="/where-to-buy-solutions/agencies">'.$nav["agencies"].'</a></li>' : '' ).'
			<li class="navigation__menu__item"><a class="navigation__menu__item__link'.(FILENAME == 'page-blog' || FILENAME == 'single-post' ? ' active-menu-item' : '').'" href="/blog">'.$nav["blog"].'</a></li>

			<li class="navigation__menu__item"><a class="navigation__menu__item__link'.(FILENAME == 'page-contact-us' ? ' active-menu-item' : '').'" href="/contact-us">'.$nav["contact"].'</a></li>

            '.(IS_MOBILE ? '<li class="navigation__menu__item navigation__menu__item--call-us">Call Us</li>' : '' ).'
            '.(IS_MOBILE ? '<li class="navigation__menu__item navigation__menu__item--phone"><a class="navigation__menu__item__link" href="tel:+31207870791">+31 20 787 0790</a></li>' : '' ).'
		</ul>

		<ul  class="navigation__menu navigation__menu--right">';
			if(!IS_MOBILE) {
			 echo '
			 	<li class="navigation__menu__item navigation__menu__item--tel">
					+31 20 787 0790
				</li>
				 	<li class="navigation__menu__item navigation__menu__item--requestDemoBtn"><a class="navigation__menu__item__link navigation__menu__item__link--requestDemoBtn" href="/where-to-buy-solutions/request-demo"><span>Request Demo</span></a></li>
			 	<li class="navigation__menu__item navigation__menu__item--loginBtn"><a class="navigation__menu__item__link navigation__menu__item__link--loginBtn" href="https://my.gethatch.com/" target="_blank"><span>Login</span></a></li>
				';
			}
		echo '</ul>';
		echo '<div class="menu-btn">';
			echo '<span></span><span></span><span></span><span></span>';
		echo '</div>';
	echo '</nav>';

?>
