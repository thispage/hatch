<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
  <meta name="description" content="Hatch connects brands with retailers, allowing consumers to purchase products wherever they are, from within whatever media they are engaging, and from whatever device or channel they are using.">
  <?php wp_head(); ?>
</head>
