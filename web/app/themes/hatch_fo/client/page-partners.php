<?php
	// Get content
	$fields = array(
		'partners' => array(
			'title' => 'Our partners',
			'subtitle' => 'With offices in the US, Europe and Asia Hatch is already working with some of the world\'s biggest brands, retailers and agencies. ',
			'brands' => array(
				'title' => 'Brands',
				'description' => 'Hatch is already working with some of the world\'s biggest brands.'
			),
			'retailers' => array(
				'title' => 'Retailers',
				'description' => 'We have already linked many of the world\'s best known retailers to our platform.'
			),
			'agencies' => array(
				'title' => 'Agencies',
				'description' => 'We\'ve worked with some of the world\'s largest agencies too.'
			),
			'quote' => array(
				'text' => "
					Hatch is a simple  solution for retailers – easy to participate in, free  and with 8% conversion rates, a proven success.
					<br /><br />
					We'll help you build trust with your brand partners, leverage their power and develop your sales funnel.
					<br /><br />
					So you can increase revenues with qualified leads from quality sources.
				",
				'name' => 'Joris Kroese, CEO Of Hatch',
				'date' => 'April, 2016.',
				'image' => '',
			)
		)
	);

	$c_fields = array(
		'id' => (int) get_the_ID(),
		'title' => '',
		'sub_title' => '',
		'posts' => array()
	);

	$c_fields['title'] = get_the_title();

	$args = array(
		'include' => $c_fields['id'],
		'post_type' => 'page',
		'post_status' => 'publish'
	);

	$posts_array = get_pages($args);

	if (count($posts_array) === 1) {
		$post = $posts_array[0];

		$current_url = get_permalink( $post->ID );

		$custom_fields = get_fields((int) $post->ID);

		$fields = array(
			'partners' => array(
				'title' => $custom_fields['title'],
				'subtitle' => $custom_fields['description'],
				'brands' => array(
					'title' => $custom_fields['brands_title'],
					'description' => $custom_fields['brands_description'],
					'images' => $custom_fields['brands_images']
				),
				'retailers' => array(
					'title' => $custom_fields['retailers_title'],
					'description' => $custom_fields['retailers_description'],
					'images' => $custom_fields['retailers_images']
				),
				'agencies' => array(
					'title' => $custom_fields['agencies_title'],
					'description' => $custom_fields['agencies_description'],
					'images' => $custom_fields['agencies_images']
				),
				'quote' => array(
					'text' => $custom_fields['quote_text'],
					'name' => $custom_fields['quote_name'],
					'date' => $custom_fields['quote_date'],
					'image' => $custom_fields['quote_image'],
				)
			)
		);
	}

?>


<section class="section partners">
	<div class="news__section__pattern--top news__section__pattern news__section__pattern"></div>
	<div class="section__header">
		<h1 class="section__header__title"><?php echo $fields['partners']['title']; ?></h1>
	</div>
    <h2 class="section__header__subtitle"><?php echo $fields['partners']['subtitle']; ?></h2>


	<div class="partners__content__card first-content-card">
		<div class="partners__content__card__top">
			<?php echo $fields['partners']['brands']['title']; ?>
		</div>

		<div class="partners__content__card__bottom">
			<p class="partners__content__text">
				<?php echo $fields['partners']['brands']['description']; ?>
			</p>

			<div class="partners__content__card__logos">
				<?php

				if (isset($fields['partners']['brands']['images']) && count($fields['partners']['brands']['images']) > 0) {
					foreach($fields['partners']['brands']['images'] as $index => $image) {
						echo '<a href="'.$image['link'].'"><img src="'.$image['image'].'" alt="" /></a>';
					}
				}

				?>
			</div>
		</div>

		<a class="section__smallbtn btn-center" href="/where-to-buy-solutions/agencies">
			<span>Learn more</span>
		</a>
	</div>

	<div class="partners__content__card">
		<div class="partners__content__card__top">
			<?php echo $fields['partners']['retailers']['title']; ?>
		</div>

		<div class="partners__content__card__bottom">
			<p class="partners__content__text">
				<?php echo $fields['partners']['retailers']['description']; ?>
			</p>

			<div class="partners__content__card__logos">
			<?php

				if (isset($fields['partners']['retailers']['images']) && count($fields['partners']['retailers']['images']) > 0) {
					foreach($fields['partners']['retailers']['images'] as $index => $image) {
						echo '<a href="'.$image['link'].'"><img src="'.$image['image'].'" alt="" /></a>';
					}
				}

			?>
			</div>
		</div>

		<a class="section__smallbtn btn-center" href="/where-to-buy-solutions/retailers">
			<span>Learn more</span>
		</a>
	</div>

	<div class="partners__content__card">
		<div class="partners__content__card__top">
			<?php echo $fields['partners']['agencies']['title']; ?>
		</div>

		<div class="partners__content__card__bottom">
			<p class="partners__content__text">
				<?php echo $fields['partners']['agencies']['description']; ?>
			</p>

			<div class="partners__content__card__logos">
			<?php

				if (isset($fields['partners']['agencies']['images']) && count($fields['partners']['agencies']['images']) > 0) {
					foreach($fields['partners']['agencies']['images'] as $index => $image) {
						echo '<a href="'.$image['link'].'"><img src="'.$image['image'].'" alt="" /></a>';
					}
				}

			?>
			</div>
		</div>

		<a class="section__smallbtn btn-center" href="/where-to-buy-solutions/agencies">
			<span>Learn more</span>
		</a>
	</div>

	<div class="partners__content__quote">
		<div class="quote_image_holder">
			<?php
			if (isset($fields['partners']['quote']['image']) && $fields['partners']['quote']['image'] != '') {
				echo '<img src="'.$fields['partners']['quote']['image'].'" alt="">';
			}
			?>
		</div>
		<div class="quote_content_holder">
			<blockquote>
				<?php echo $fields['partners']['quote']['text']; ?>
			</blockquote>

			<p>
				<span class="quote-name"><?php echo $fields['partners']['quote']['name']; ?></span>
				<span class="quote-date">- <?php echo $fields['partners']['quote']['date']; ?></span>
			</p>
		</div>

	</div>

</section>
