import {SpriteManager} from './spriteManager';

export class ArrowSpriteManager extends SpriteManager{
	constructor(params) {
		super(params);
		this.arrowNumber = params.arrowNumber;
		this.resetspriteInfoObj();
		this.bindFunctions();
		this.setEmitterEvents();
		this.animateSprite(this.onSpriteFinished);
	}

	resetspriteInfoObj() {
		this.spriteInfoObj = {
			frameDuration: 46,
			frameCount: 0,
			coords: [
				[0, 	0],
				[52, 	0],
				[104, 	0],
				[156, 	0],
				[208, 	0],
				[260, 	0],
				[312, 	0],
				[364, 	0],
				[416, 	0],

				[0, 	90],
				[52, 	90],
				[104, 	90],
				[156, 	90],
				[208, 	90],
				[260, 	90],
				[312, 	90],
				[364, 	90],
				[416, 	90],

				[0, 	180],
				[52, 	180],
				[104, 	180],
				[156, 	180],
				[208, 	180],
				[260, 	180],
				[312, 	180],
				[364, 	180],
				[416, 	180],

				[0, 	270],
				[52, 	270],
				[104, 	270],
				[156, 	270],
				[208, 	270],
				[260, 	270],
				[312, 	270],
				[364, 	270],
				[416, 	270],

				[0, 	360],
				[52, 	360],
				[104, 	360],
				[156, 	360],
				[208, 	360],
				[260, 	360],
				[312, 	360],
				[364, 	360],
				[416, 	360],

				[0, 	450],
				[52, 	450],
				[104, 	450],
				[156, 	450],
				[208, 	450],
				[260, 	450],
				[312, 	450],
				[364, 	450],
				[416, 	450],
			]
		};
	}

	init() {

	}

	bindFunctions() {
		this.checkAnimate = this.checkAnimate.bind(this);
		this.onSpriteFinished = this.onSpriteFinished.bind(this);
	}

	setEmitterEvents() {
		//window.App.emitter.on('initMenu', this.onIntroFinished);
		//window.App.emitter.on('arrowhover', this.checkAnimate);
	}

	checkAnimate(arrowNb) {
		console.log(arrowNb, this.arrowNumber);
		if(arrowNb === this.arrowNumber && !this.animating ) {
			this.animateSprite(this.onSpriteFinished);
		}
	}

	onSpriteFinished() {
		this.resetspriteInfoObj();
		this.tween.kill();
		TweenMax.set(this.spriteContainer, {clearProps: "all"});
		TweenMax.delayedCall(1, () => {
			this.animateSprite(this.onSpriteFinished);
		});
	}

	onIntroFinished() {

	}


}
