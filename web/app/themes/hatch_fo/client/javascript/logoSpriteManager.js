import {SpriteManager} from './spriteManager';

export class LogoSpriteManager extends SpriteManager{
	constructor(params) {
		super(params);
		this.spriteInfoObj = {
			frameDuration: 61,
			frameCount: 0,
			coords: [
				[0, 	0],
				[671, 	0],
				[1342, 	0],
				[2013, 	0],
				[2684, 	0],
				[3355, 	0],
				[0, 	280],
				[671, 	280],
				[1342, 	280],
				[2013, 	280],
				[2684, 	280],
				[3355, 	280],
				[0,	 	560],
				[671, 	560],
				[1342, 	560],
				[2013, 	560],
				[2684, 	560],
				[3355, 	560],
				[0, 	840],
				[671, 	840],
				[1342, 	840],
				[2013, 	840],
				[2684, 	840],
				[3355, 	840],
				[0, 	1120],
				[671, 	1120],
				[1342, 	1120],
				[2013, 	1120],
				[2684, 	1120],
				[3355, 	1120],
				[0, 	1400],
				[671, 	1400],
				[1342, 	1400],
				[2013, 	1400],
				[2684, 	1400],
				[3355, 	1400],
				[0, 	1680],
				[671, 	1680],
				[1342, 	1680],
				[2013, 	1680],
				[2684, 	1680],
				[3355, 	1680],
				[0, 	1960],
				[671, 	1960],
				[1342, 	1960],
				[2013, 	1960],
				[2684, 	1960],
				[3355, 	1960],
				[0, 	2240],
				[671, 	2240],
				[1342, 	2240],
				[2013, 	2240],
				[2684, 	2240],
				[3355, 	2240],
				[0, 	2520],
				[671, 	2520],
				[1342, 	2520],
				[2013, 	2520],
				[2684, 	2520],
				[3355, 	2520],
				[0, 	2800],
				[671, 	2800],
				[1342, 	2800],
				[2013, 	2800],
				[2684, 	2800],
				[3355, 	2800]
				
			]
		};
		this.bindFunctions();
		this.setEmitterEvents();
	}

	init() {

	}

	bindFunctions() {
		this.onIntroFinished = this.onIntroFinished.bind(this);
	}

	setEmitterEvents() {
		window.App.emitter.on('initMenu', this.onIntroFinished);
	}

	onIntroFinished() {
		this.spriteContainer.classList.add('still');
		TweenMax.set(this.spriteContainer, {clearProps: "all"});
	}


}