/* app/views/view */
import TweenMax from '../vendor/gsap/src/uncompressed/TweenMax';

import bindAll from './lib/bindAll';
import toArray from './lib/toArray';


export class Slider {

    constructor(options) {

        this.$el = options.el || false;
        if(!this.$el) {

            return false;
        }
        this.auto = options.auto || true;
        this.autoDelay = options.autoDelay || 4;
        this._currentSlide = 1;
        this.dots = options.dots || false
        this.arrow = options.arrow || false

        this.tween = false;
        this.isAnimating = false;
        this.onResizing = false;

        this.init();
    }

    bindFunctions() {
        bindAll(this, ['loop', 'goToSlide', 'next', 'previous', 'setDots','goToDots', 'onResize', 'rightArrowHandler'])
    }

    cacheElements() {
        this.$mask = this.$el
        this.$container = this.$el.querySelector('.agencies-slideshow');

        this.$slides = toArray(document.querySelectorAll(' .agencies-slideshow .agencies-slidehow-slide'));

        //ARROWS
        this.$leftArrow = document.querySelector('.svg-arrow-prev');
        this.$rightArrow = document.querySelector('.svg-arrow-next');

        //DOTS
        this.$dots = toArray(document.querySelectorAll('.navigation-slideshow .dots'))
    }

    compute(){
        this.slideCount = ~~this.$slides.length;

        
        this.maskWidth = ~~this.$mask.offsetWidth;
        this.containerWidth = this.slideCount * this.maskWidth
    }

    eventListeners() {
        if (this.arrow) {
            this.$rightArrow.addEventListener('click', this.rightArrowHandler);
            this.$leftArrow.addEventListener('click', this.previous);
        }


        this.$dots.map((el) => {
            el.addEventListener("click", this.goToDots);
        });

        window.addEventListener('resize', () => {
            if (!this.onResizing) {
                this.onResizing = true;

                setTimeout(() => {
                    console.log('timeout')
                    this.onResize();
                }, 400);
            }
        })

    }

    onResize() {
        console.log(this.maskWidth)
        this.compute();
        this.setContainerWidth(this.containerWidth);
        this.goToSlide(this._currentSlide);
        this.onResizing = false;
    }

    setContainerWidth(width) {
        TweenLite.set(this.$container,{ width : width} );
        TweenLite.set(this.$slides, { width : width/this.slideCount})
    }

    init() {
        this.bindFunctions();
        this.cacheElements();
        this.eventListeners();
        this.compute();
        this.setContainerWidth(this.containerWidth);
        this.bindFunctions();

        this.goToSlide(this._currentSlide);

        if (this.dots) {
            this.setDots();
        }


        if (this.auto) {
            this.loop();

        }
    }

    goToSlide( slide ) {
        console.log('slide BOUM', )
        this._currentSlide = slide;
        TweenMax.to(this.$container, 1 ,{
            x: (slide - 1) * - this.maskWidth,
            ease: Power2.easeInOut,

        });


        if (this.dots){
            let $nextDot = document.querySelector(`[data-dot= "${this._currentSlide}"]`);
            let $nextSlide = document.querySelector(`[data-slide= "${this._currentSlide}"]`);
            for (var i = this.$dots.length - 1; i >= 0; i--) {
                this.$dots[i].classList.remove('selected')
            }

            $nextDot.classList.add('selected')
        }

    }

    rightArrowHandler(){
        TweenLite.killTweensOf(this.loop)
        this.loop();
    }

    leftArrowHandler(){
        TweenLite.killTweensOf(this.loop)

        this.loop(false);
    }


    next() {
        console.log(this.slideCount)
        // this.isAnimating = true;

        if( this._currentSlide == this.slideCount){
            this._currentSlide = 1
            this.goToSlide(this._currentSlide);
        }else{
            this.goToSlide( this._currentSlide + 1 );
        }
    }

    previous() {
        this.isAnimating = true;

        if( this._currentSlide == 0){
            this._currentSlide = this.slideCount
            this.goToSlide(this._currentSlide);
        }else{
            this.goToSlide( this._currentSlide - 1 );

        }
    }

    loop(next=true) {
        if (next) {
            this.next();
        } else {
            this.previous();
       }

       TweenLite.delayedCall(5, this.loop);
    }



    setDots(){
        //
        for (var i = this.$slides.length - 1; i >= 0; i--) {
            this.$dots[i].classList.add("visible")
        }
    }

    goToDots(ev) {
        ev.preventDefault();
        let $dot = ev.currentTarget.getAttribute('data-dot');
        this.goToSlide($dot * 1);
        console.log($dot);
        // this._currentSlide = $dot;
    }




}
