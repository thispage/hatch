import {OverlayManager} from './overlayManager';

export class VideoManager {
	constructor(params) {
		this.videoBtn = params.videoBtn;
		this.overlay = document.getElementsByClassName('overlay__video')[0];
		this.overlay = new OverlayManager({
			overlay: this.overlay
		})
		this.bindFunctions();
	}

	bindFunctions() {
		this.onClick = this.onClick.bind(this);
	}

	init() {
		this.setListeners();
	}

	kill() {
		this.removeListeners();	
	}

	resume() {
		this.setListeners();
	}

	setListeners() {
		this.videoBtn.addEventListener('click', this.onClick);
	}

	removeListeners() {
		this.videoBtn.removeEventListener('click', this.onClick);	
	}

	onClick() {
		this.overlay.show();
	}
}