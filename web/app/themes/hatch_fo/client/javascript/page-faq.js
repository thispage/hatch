import {PageManager} from './pageManager';

export class Faq extends PageManager {
	constructor() {
		super();
	}

	/**
	 * Initialize
	 */
	init() {
		super.init();
		
		var questions = document.getElementsByClassName('faq-question');
		this.getElements();
		this.colorChange();
		for (var i=0; i< questions.length; i++) {
			
			questions[i].addEventListener('click', function(ev) {
				this.classList.toggle('faq-question-expanded');
			});
		}
	}

	getElements() {
		this.$arrow = document.querySelector(".section__arrow__bg--footer ")
		this.$text = document.querySelector(".section__arrow--getintouch span");
		console.log(this.$arrow);

	}

	colorChange() {
		this.$arrow.classList.remove('white');
		this.$text.style.color = "black";
	}
}