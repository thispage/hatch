export class FormManager {
	constructor(params) {
		this.setElements(params);
		this.bindFunctions();
		this.setListeners();
	}

	setElements(params) {
		this.action = params.action,
		this.submitBtn = params.submitBtn,
		this.contactFormData = {
			id: params.contactFormData.id,
			version: params.contactFormData.version,
			lang: params.contactFormData.lang,
			tag: params.contactFormData.tag,
			once: params.contactFormData.once
		};


		this.inputs = params.inputs;

	}

	bindFunctions() {
		this.onFormSubmit = this.onFormSubmit.bind(this);
		this.onreadyStateChange = this.onreadyStateChange.bind(this);
		this.onOverlayClose = this.onOverlayClose.bind(this);
		this.onInputFocusOut = this.onInputFocusOut.bind(this);
		this.onInputFocusIn = this.onInputFocusIn.bind(this);
	}
	
	setListeners() {
		this.submitBtn.addEventListener('click', this.onFormSubmit);
		let key;
		for(key in this.inputs) {
			if(this.inputs.hasOwnProperty(key)) {
				this.inputs[key].addEventListener('focus', this.onInputFocusIn);
				if(window.isMobile && !window.isTablet) {
					this.inputs[key].addEventListener('focusout', this.onInputFocusOut);
				}
			}
		}
		window.App.emitter.on('overlayClose', this.onOverlayClose);
	}

	onFormSubmit(e) {
		e.preventDefault();
		this.checkInputs();
	}

	onInputFocusOut(e) {
		window.App.emitter.emit('focusout');	
	}

	onInputFocusIn(e) {
		if(e.target.getAttribute('id') === 'plan') {
			return;
		}
		e.target.value = e.target.value === 'Email address seems invalid' || e.target.value === 'Please fill in the required field.' ? '' : e.target.value;
		e.target.classList.remove('error');
	}

	checkInputs() {
		this.errors = {};

		let key;
		for(key in this.inputs) {
			if(this.inputs.hasOwnProperty(key)) {
				
				if(this.inputs[key].name === 'pdfCaseName' || this.inputs[key].name === 'planName') {
					continue;
				}

				let val = this.inputs[key].value;
				let type = this.inputs[key].getAttribute('type');
				console.log('value: ' + val, 'type: ' + type);
				if(type === 'email') {
					let re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
					let valid = re.test(val);
					
					if(valid) {
						this.inputs[key].classList.remove('error');
						delete this.errors[key];
					}
					else {
						this.inputs[key].value = 'Email address seems invalid';
						this.inputs[key].classList.add('error');
						this.errors[key] = true;
					}
				}
				else {
					if(val === '' || val === 'Name' || val === 'Please fill in the required field.' || typeof val === 'undefined') {
						this.inputs[key].value = 'Please fill in the required field.';
						this.inputs[key].classList.add('error');
						this.errors[key] = true;
					}
					else {
						this.inputs[key].classList.remove('error');
						delete this.errors[key];
					}
				}
				
			}
		}

		console.log(this.errors);

		if(Object.getOwnPropertyNames(this.errors).length === 0) {
			this.sendForm();
		}

	}

	sendForm() {
		
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
	      	this.httpRequest = new XMLHttpRequest();
	    } else if (window.ActiveXObject) { // IE
	      	try {
	        	this.httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
		    } 
		    catch (e) {
		    	try {
		          this.httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
		        } 
		        catch (e) {}
		    }
		}


		this.httpRequest.onreadystatechange = this.onreadyStateChange;
		this.httpRequest.open('POST', this.action);
    	this.httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    	let string = '_wpcf7=' + encodeURIComponent(this.contactFormData.id) 
    		+ '&_wpcf7_version='+ encodeURIComponent(this.contactFormData.version) 
    		+ '&_wpcf7_locale='+ encodeURIComponent(this.contactFormData.lang) 
    		+ '&_wpcf7_unit_tag='+ encodeURIComponent(this.contactFormData.tag)
    		+ '&_wpnonce='+ encodeURIComponent(this.contactFormData.once); 

    	let key;
    	for(key in this.inputs) {
			if(this.inputs.hasOwnProperty(key)) {
				string += '&' + key + '=' + encodeURIComponent(this.inputs[key].getAttribute('value'));
			}
		};

		string += '&_wpcf7_is_ajax_call=' + encodeURIComponent(1);

		
		this.httpRequest.send(string);

	}

	onreadyStateChange(e) {
		try {
		    if (this.httpRequest.readyState === 4) {
				if (this.httpRequest.status === 200) {
					this.showOkMessage();
					// ga('send', {
					// 	hitType: 'event',
					// 	eventCategory: 'Form',
					// 	eventAction: 'Submit',
					// 	eventLabel: 'Form ' + this.contactFormData.id + ' submit success'
					// });
				} else {
		    		
		    	}
		    }
		}
		catch(e) {
			
		}
	}

	showOkMessage() {
		TweenMax.delayedCall(.5, () => {
			this.submitBtn.value =  "Thank you !";
			TweenMax.delayedCall(.5, () => {
				window.App.emitter.emit('hideOverlay');
			})
		})
	}

	clearInputs() {
		let key;
		for(key in this.inputs) {
			if(this.inputs.hasOwnProperty(key)) {
				this.inputs[key].value = '';
				this.inputs[key].classList.remove('error');
			}
		}
		this.submitBtn.blur();
		this.submitBtn.value = "SEND";
	}

	onOverlayClose() {
		this.clearInputs();
	}


}