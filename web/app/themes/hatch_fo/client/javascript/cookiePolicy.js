import bindAll from "./lib/bindAll";

export class CookiePolicy {
    constructor() {
        this.$cookiePolicy = document.querySelector('.cookie-policy');
        this.$cookiePolicyBtn = document.querySelector('.cookie-policy__button');
    }

    init() {
        this.bindFunctions();
        this.checkCookies();
    }

    bindFunctions() {
        bindAll(this, ['checkCookies', 'acceptCookies']);
    }

    checkCookies() {
        if (!localStorage.getItem('CAcc')) {
            TweenMax.to(this.$cookiePolicy, 1, {
                autoAlpha: 1,
                delay: 1,
                onComplete: () => {
                    this.$cookiePolicyBtn.addEventListener('click', this.acceptCookies);
                }
            });
        }
    }

    acceptCookies() {
        localStorage.setItem('CAcc', true);
        TweenMax.to(this.$cookiePolicy, .5, {
            autoAlpha: 0,
            y: window.isMobile ? 120 : 90,
            onComplete: () => {
                this.$cookiePolicyBtn.removeEventListener('click', this.acceptCookies);
            }
        });
    }
}
