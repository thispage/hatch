import {SectionAnim} from './sectionAnim';
import {LogoSpriteManager} from './logoSpriteManager'

export class LogoSectionAnim extends SectionAnim {
	constructor(params) {
		super(params);
		this.logoSpriteManager = new LogoSpriteManager({
			spriteContainer: this.section.getElementsByClassName('loader__logo')[0]
		});
	}
	getElements() {
		//this.steps = this.section.getElementsByClassName('intro__steps');
	}

	/**
	 * Triggers the section animations
	 */
	trigger(params) {
		this.timeline = params.timeline;
		this.callback = params.callback;
		window.appStates.intro.started = true;
		this.logoSpriteManager.animateSprite(this.callback);
		//window.appStates.intro.finished = true;
		//this.timeline.staggerTo(this.steps, .2, {autoAlpha: 1}, this.suffix);
	}
}