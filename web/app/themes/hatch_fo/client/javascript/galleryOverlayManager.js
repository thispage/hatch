import {OverlayManager} from './overlayManager';

export class GalleryOverlayManager extends OverlayManager{
	constructor(params) {
		super(params);
		this.images = params.images;
		this.imageElts = [];
		this.imageRects = [];
		this.galleryPolygonsClips = [];
		this.galleryPolyPoints = [];
		this.prevArrow =  document.getElementsByClassName('overlay__prev')[0];
		this.nextArrow =  document.getElementsByClassName('overlay__next')[0];
	}

	bindFunctions() {
		//super();
		super.bindFunctions();
		this.goPrev = this.goPrev.bind(this);
		this.goNext = this.goNext.bind(this);
	}

	populateDomElements() {
		if(!this.domReady) {
			this.content = this.overlay.getElementsByClassName('overlay__gallery__content')[0];
			this.svgWrap = this.content.querySelector('svg');
			this.group = this.content.querySelector('g');
			this.defs = this.content.querySelector('defs');
		}
			
		this.namespace = "http://www.w3.org/2000/svg";
		this.defs.innerHTML = '';
		this.group.innerHTML = '';
		this.imageElts = [];
		this.imageRects = [];
		this.galleryPolygonsClips = [];
		this.galleryPolyPoints = [];
		
		for(let i = 0; i < this.images.length; i++) {

			// Clip Paths
			let path = document.createElementNS(this.namespace, 'clipPath');
			path.setAttribute('id', 'image-cache-' + i);
			
			let polygon = document.createElementNS(this.namespace, 'polygon');
			polygon.setAttribute('points', '0,0 0,0 0,0 0,0');

			path.appendChild(polygon);
			this.galleryPolygonsClips.push(polygon);

			// Points coords
			this.galleryPolyPoints.push({
				x1: 0, 		y1: 0,
				x2: 0, 		y2: 0,
				x3: 0, 		y3: 0,
				x4: 0, 		y4: 0,
				angle: 30
			});

			// Images
			let image = document.createElementNS(this.namespace, 'image');
			//image.setAttribute('class', 'no-display');
			image.setAttributeNS('http://www.w3.org/1999/xlink','href', this.images[i]);
			image.setAttribute('x', 0);
			image.setAttribute('y', 0);
			image.setAttribute('width', 1024);
			image.setAttribute('height', 560);
			image.setAttribute('fill', "#FFFFFF");
			// Assign the clip-path to the image
			image.setAttribute('clip-path', 'url(#image-cache-' + i + ')');
			this.imageElts.push(image);

			// Rect to have image bg
			let rect = document.createElementNS(this.namespace, 'rect');
			rect.setAttribute('x', 0);
			rect.setAttribute('y', 0);
			rect.setAttribute('width', 1024);
			rect.setAttribute('height', 560);
			rect.setAttribute('fill', "#FFFFFF");
			// Assign the clip-path to the image
			rect.setAttribute('clip-path', 'url(#image-cache-' + i + ')');
			this.imageRects.push(image);
			
			this.defs.appendChild(path);
			this.group.appendChild(rect);
			this.group.appendChild(image);
		}

		this.domReady = true;

	}

	setArrowListeners() {
		this.prevArrow.addEventListener('click', this.goPrev);
		this.nextArrow.addEventListener('click', this.goNext);
	}

	removeArrowListeners() {
		this.prevArrow.removeEventListener('click', this.goPrev);
		this.nextArrow.removeEventListener('click', this.goNext);
	}

	show(index) {
		console.log('show');
		if(!this.domReady) {
			this.setArrowListeners();
		}
		this.populateDomElements();

		this.setCurrentSlide(index);
		this.checkArrowsState();
		super.show(() => {
			this.showCurrentSlide(index, null, 1, true)
		});
	}

	hide() {
		super.hide();
		this.hideSlides();
	}

	/**
	 * Frees up some memory
	 */
	remove() {
		this.removeArrowListeners();
		for(let i = 0; i < this.imageElts.length; i++) {
			this.svgWrap.removeChild(this.imageElts[i]);
		}
		this.imageElts = [];
		this.domReady = false;
	}

	setCurrentSlide(slideNbr) {
		this.currentSlide = slideNbr;
	}

	hideSlides() {
		for(let i = 0; i< this.galleryPolyPoints.length; i++) {
			this.galleryPolyPoints[i] = {
				x1: 0, 		y1: 0,
				x2: 0, 		y2: 0,
				x3: 0, 		y3: 0,
				x4: 0, 		y4: 0,
				angle: 30
			}

			this.setGalleryPolyPoints(i);
		}
	}

	showCurrentSlide(slide, prevSlide, dir, init) {
		console.log(slide, prevSlide, dir, init);
		dir = dir || 1;
		let ease = init ? Power2.easeInOut : Power4.easeOut;
		let timing = init ? .7 : .9;
		// Animate the svg polygon to reveql the current image
		let x2 = -1 *  (Math.tan(this.galleryPolyPoints[slide].angle * Math.PI/180) * 1024);
		let y3 = -1 *  (560 / Math.tan(this.galleryPolyPoints[slide].angle * Math.PI/180));
		
		if(dir > 0) {
			console.log(this.galleryPolyPoints[slide]);
			TweenMax.fromTo(this.galleryPolyPoints[slide], .9, {
				x1: 1024, 	y1: 560,
				x2: 1024, 	y2: 560,
				x3: 1024, 	y3: 560,
				x4: 1024, 	y4: 560
			},{
				ease: ease,
				x1: 1024, 	y1: 560,
				x2: x2, 	y2: 560,
				x3: 1024, 	y3: y3,
				x4: 1024, 	y4: 560,

				onUpdate: () => {
					this.setGalleryPolyPoints(slide);
				},

				onComplete: () => {
					if(prevSlide !== null)  {
						this.hidePrevSlide(prevSlide, dir);
					}
				}
			});
		}
		else {
			// First show the slide to be shown
			TweenMax.set(this.galleryPolyPoints[slide], {
				x1: 1024, 	y1: 560,
				x2: x2, 	y2: 560,
				x3: 1024, 	y3: y3,
				x4: 1024, 	y4: 560
			});
			this.setGalleryPolyPoints(slide);
			
			// Then hide the prev one ot reveal the new one
			TweenMax.fromTo(this.galleryPolyPoints[prevSlide], .9, {
				x1: 1024, 	y1: 560,
				x2: x2, 	y2: 560,
				x3: 1024, 	y3: y3,
				x4: 1024, 	y4: 560
			},{
				ease: Power4.easeOut,
				x1: 1024, 	y1: 560,
				x2: 1024, 	y2: 560,
				x3: 1024, 	y3: 560,
				x4: 1024, 	y4: 560,

				onUpdate: () => {
					this.setGalleryPolyPoints(prevSlide);
				}
			});
			// TweenMax.to(this.galleryPolyPoints[slide], .7, {
			// 	x1: 0, 		y1: 0,
			// 	x2: x2, 	y2: 0,
			// 	x3: 0, 	    y3: y3,
			// 	x4: 0, 		y4: 0,

			// 	onUpdate: () => {
			// 		this.setgalleryPolyPoints(slide);
			// 	},

			// 	onComplete: () => {
			// 		this.hidePrevSlide(prevSlide, dir);
			// 	}
			// });
		}
	}

	hidePrevSlide(slide, dir) {
		TweenMax.set(this.galleryPolyPoints[slide], {
			x1: 0, 		y1: 0,
			x2: 0, 		y2: 0,
			x3: 0, 	    y3: 0,
			x4: 0, 		y4: 0
		});

		this.setGalleryPolyPoints(slide);
	}


	// hideCurrentSlide(slide, dir) {
	// 	
	// 	TweenMax.to(this.galleryPolyPoints[slide], .7, {
	// 		x1: 0, 		y1: 0,
	// 		x2: 0, 		y2: 0,
	// 		x3: 0, 	    y3: 0,
	// 		x4: 0, 		y4: 0,

	// 		onUpdate: () => {
	// 			this.setgalleryPolyPoints(slide);
				
	// 		}
	// 	});
	// }
	
	setGalleryPolyPoints(slide) {
		console.log(slide);
		this.galleryPolygonsClips[slide].setAttribute("points", this.galleryPolyPoints[slide].x1 + "," + this.galleryPolyPoints[slide].y1 + " " + this.galleryPolyPoints[slide].x2 + "," + this.galleryPolyPoints[slide].y2 + " " + this.galleryPolyPoints[slide].x3 + "," + this.galleryPolyPoints[slide].y3 + " " + this.galleryPolyPoints[slide].x4 + "," + this.galleryPolyPoints[slide].y4);
	}

	goPrev() {
		if(this.currentSlide > 0) {
			let slide = this.currentSlide;
			this.showCurrentSlide(slide - 1, slide, -1);
			this.currentSlide--

		}

		this.checkArrowsState();
	}

	goNext(){
		if(this.currentSlide < this.imageElts.length - 1) {
			let slide = this.currentSlide;
			this.showCurrentSlide(slide + 1, slide, 1);
			this.currentSlide++;
		}

		this.checkArrowsState();
	}

	checkArrowsState() {
		console.log('current slide', this.currentSlide);
		if (this.currentSlide === 0) {
			this.prevArrow.classList.add('inactive');
			this.nextArrow.classList.remove('inactive');
		} else if (this.currentSlide > 0 && this.currentSlide < this.imageElts.length - 1) {
			this.prevArrow.classList.remove('inactive');
			this.nextArrow.classList.remove('inactive');
		} else if (this.currentSlide === this.imageElts.length - 1) {
			this.prevArrow.classList.remove('inactive');
			this.nextArrow.classList.add('inactive');
		}
	}


}

