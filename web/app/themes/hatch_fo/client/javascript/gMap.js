export class GMap {

	constructor(params) {
		this.container = params.mapContainer;
		this.coordinates = params.coordinates;
		this.init();
	}

	/**
	 * Initialize
	 *
	 */
	init() {
		this.map = new google.maps.Map(this.container, {
			center: this.coordinates[0] || {lat: 0, lng: 0},
			zoom: 3, maxZoom: 20, minZoom: 3
		});

		this.map.setOptions({ scrollwheel: false });

		this.addMarkers();
		//this.centerMap();
	}

	/**
	 * Add Markers
	 *
	 */
	addMarkers() {
		this.markers = [];

		for (let x = 0; x < this.coordinates.length; x++) {
			let marker = new google.maps.Marker({
				position: this.coordinates[x],
				map: this.map
			});

			this.markers.push(marker);
		}
	}

	/**
	 * Center Map
	 *
	 */
	centerMap() {
		let bounds = new google.maps.LatLngBounds();

		for (let i = 0; i < this.markers.length; i++) {
			bounds.extend(this.markers[i].position);
		}

		this.map.fitBounds(bounds);
	}

	resize() {
		if (this.map) {
			google.maps.event.trigger(this, 'resize');
			this.centerMap();
		}
	}
}
