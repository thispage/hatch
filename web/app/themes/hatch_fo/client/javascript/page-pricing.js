import {PageManager} from './pageManager';
import {OverlayManager} from './overlayManager';
import {FormManager} from './formManager';

export class Pricing extends PageManager {
	constructor() {
		super();
	}

	/**
	 * Initialize
	 */
	init() {
		super.init();
		
		this.bindFunctions();
		this.getElements();
		this.setListeners();

		this.setEmitterEvents();

		this.subtitles = this.wrapper.getElementsByClassName("section__header__subtitle");
		for(let i = 0; i < this.subtitles.length; i++) {
			this.subtitles[i].classList.add('animate-border');
		}

		this.formManager = new FormManager({
			action: this.formOverlay.querySelector('form').getAttribute('action'),
			submitBtn: document.getElementById('planSubmitBtn'),
			contactFormData: {
				id: this.formOverlay.querySelector('input[name="_wpcf7"]').getAttribute('value'),
				version: this.formOverlay.querySelector('input[name="_wpcf7_version"]').getAttribute('value'),
				lang: this.formOverlay.querySelector('input[name="_wpcf7_locale"]').getAttribute('value'),
				tag: this.formOverlay.querySelector('input[name="_wpcf7_unit_tag"]').getAttribute('value'),
				once: this.formOverlay.querySelector('input[name="_wpnonce"]').getAttribute('value')
			},
			inputs: {
				planName: document.getElementById('planName'),
				planContactName: document.getElementById('planContactName'),
				planContactEmail: document.getElementById('planContactEmail')
			}
		});
	}

	/**
	 * Bind functions
	 */
	bindFunctions() {
		this.onBtnClick = this.onBtnClick.bind(this);
		this.enableScroll = this.enableScroll.bind(this);
		this.disableScroll = this.disableScroll.bind(this);
		
		this.offsetOverlay = this.offsetOverlay.bind(this);
	}

	/**
	 * Get Elements
	 */
	getElements() {
		this.wrapper = document.getElementsByClassName('page-wrapper')[0];
		this.overlay = document.getElementById('overlay-container');
		this.formOverlay = document.getElementsByClassName('overlay__getstarted')[0];
		this.formOverlayTriggers = document.getElementsByClassName('rpt_foot');
		this.formOverlayClose = this.formOverlay.getElementsByClassName('overlay__close')[0];
	}

	/**
	 * Set Listeneres
	 */
	setListeners() {
		for(let i = 0; i < this.formOverlayTriggers.length; i++) {
			this.formOverlayTriggers[i].addEventListener(this.touchEvent, this.onBtnClick);
		}

	}

	setEmitterEvents() {
		window.App.emitter.emit('initMenu');
		window.App.emitter.on('disableScroll', this.disableScroll);
		window.App.emitter.on('enableScroll', this.enableScroll);
	}

	/**
	 * Handle clicks on formOverlayTriggers
	 */
	onBtnClick(e) {
		e.preventDefault();
		this.setPlanName(e.target);
		this.showOverlay();
	}

	/**
	 * Find  and Set Plan Name in the input
	 */
	setPlanName(target) {
		let title = target.parentNode.getElementsByClassName('rpt_title')[0].textContent;
		let planInput = document.getElementById('planName');

		if(title) { planInput.value = title; }

		planInput.placeholder = "Plan";
	}

	/**
	 * Show Overlays
	 */
	showOverlay() {
		this.overlayManager = this.overlayManager || new OverlayManager({
			overlay: this.formOverlay
		});

		this.overlayManager.show();
	}

		/**
	 * Disable Scroll
	 */
	disableScroll() {
		document.addEventListener('touchmove', this.preventEventDefault);
		this.totalHeight = document.body.scrollHeight;
	}

	/**
	 * Enable Scroll
	 */
	enableScroll() {
		document.removeEventListener('touchmove', this.preventEventDefault);
		// VirtualScroll.off(this.onScroll);
	}

	offsetOverlay() {
		let overlayYOffset = Math.min(window.pageYOffset, this.totalHeight - this.overlay.offsetHeight);
		TweenLite.set(this.overlay,  {y: overlayYOffset});
	}

	/**
	 * Prevent Event's dafault
	 */
	preventEventDefault(e) {
		e.preventDefault();
	}
}

























