import {PageManager} from './pageManager';
import PdfForm from './pdfForm';

export class Brands extends PageManager {
    constructor() {
        super();
    }

    /**
     * Initialize
     */
    init() {
        super.init();

        this.pdfForm = new PdfForm();
    }
}
