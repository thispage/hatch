import ScrollToPlugin from '../vendor/gsap/src/uncompressed/plugins/ScrollToPlugin';

export class ArrowBtnsManager {
	constructor(params) {
		this.arrows = params.arrows;
		// this.getElements();
		this.bindFunctions();
		this.setListeners();
	}

	bindFunctions() {
		// this.firstArrowClickHome = this.firstArrowClickHome.bind(this);
		this.onArrowClick = this.onArrowClick.bind(this);
	}

	setListeners() {
		for(let i = 0; i< this.arrows.length; i++) {
			this.arrows[i].addEventListener('click', this.onArrowClick);
		}

		// this.$firstArrowHome.addEventListener('click', this.firstArrowClickHome);
	}

	// getElements() {
		// this.$firstArrowHome = document.querySelector('#first-arrow-home');
		// this.$solutionsElement = document.querySelector('#solutions');
	// }

	// firstArrowClickHome(e) {
		// let sectionId = e.target.getAttribute('id');
		// console.log(`click `, sectionId);
		// console.log(window.scrollTo);

		// TweenMax.to(window, 1, {
		// 	scrollTo: {
		// 		y: '#solutions',
		// 		offsetY: 90
		// 	}
		// });

		// TweenMax.to(window, 1, {
		// 	scrollTo: {
		// 		y: this.$solutionsElement.offsetTop,
		// 		offsetY: 90
		// 	}
		// });
	// }

	onArrowClick(e) {
		console.log(`scroll to `, sectionId);
		let sectionId = e.target.getAttribute('data-id') ? e.target.getAttribute('data-id') : e.target.parentElement.getAttribute('data-id');
		const element = document.querySelector('#'+sectionId);

		if(sectionId) {
			TweenMax.to(window, .5, {
					scrollTo: {
						y: element.offsetTop,
						offsetY: 90,
						autoKill : true
					},
					ease:Power3.easeOut
				}
			);
		}
	}
}
