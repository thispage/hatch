export class VideoButton {
	constructor(pTarget) {
		//--
		this.target = pTarget;
		
		if (typeof this.target === 'undefined') {
			return false;
		}
		
		this.hoverIn = this.hoverIn.bind(this);
		this.hoverOut = this.hoverOut.bind(this);
		this.mouseUp = this.mouseUp.bind(this);
		this.mouseDown = this.mouseDown.bind(this);
		this.animateInComplete = this.animateInComplete.bind(this);
		
		//--
		this.text = this.target.getElementsByTagName("p")[0];
		this.circle = this.target.getElementsByClassName("circle")[0];
		this.triangle = this.target.getElementsByClassName("triangle")[0];
		if (!window.isMobile && !window.isTablet) {
			this.triangle.addEventListener("mouseenter", this.hoverIn);
		} else {
			this.triangle.addEventListener("touchstart", this.mouseDown);
		}
		//--
		this.setupIn();
	}

	hoverIn() {
		this.isIn = true;

		this.triangle.removeEventListener("mouseenter", this.hoverIn);
		this.triangle.addEventListener("mouseleave", this.hoverOut);
		this.triangle.addEventListener("mousedown", this.mouseDown);

		if (!this.inComplete)return;
		TweenLite.to(this.triangle, .3, {scale: 0.9, ease: Circ.easeOut,force3D:false});
		TweenLite.to(this.circle, .4, {scale: 0.8, ease: Circ.easeOut,force3D:false});
	};

	animateIn(pDelay = 0) {

	}

	mouseDown( ) {
		TweenLite.to(this.triangle, .3, {scale: .9, ease: Circ.easeOut,force3D:false});
		TweenLite.to(this.circle, .4, {scale: .95, ease: Circ.easeOut,force3D:false});
		if (!window.isMobile){
			this.triangle.addEventListener("mouseup", this.mouseUp);
			this.triangle.removeEventListener("mousedown", this.mouseDown);
		}else
		{
			this.triangle.removeEventListener("touchstart", this.mouseDown);
			this.triangle.addEventListener("touchend", this.mouseUp);
		}


	}

	mouseUp( ) {
		this.hoverOut();
	}

	setupIn() {
		this.animateInComplete();
	}

	animateInComplete() {
		this.inComplete = true;
		if (this.isIn) this.hoverIn();

	}

	hoverOut() {
		this.isIn = false;
		this.triangle.removeEventListener("mouseup", this.mouseUp);
		this.triangle.removeEventListener("mouseleave", this.hoverOut);
		if (!window.isMobile) this.triangle.addEventListener("mouseenter", this.hoverIn);
		else this.triangle.addEventListener("touchstart", this.mouseDown);

		if (!this.inComplete)return;
		TweenLite.to(this.circle, .3, {scale: 0.7, ease: Circ.easeOut,force3D:false});
		TweenLite.to(this.triangle, .4, {scale: 0.7, ease: Circ.easeOut,force3D:false});

	}

}
