import toArray from '../lib/toArray';
import bindAll from '../lib/bindAll';
import inViewport from '../lib/inViewport';

import TweenMax from '../../vendor/gsap/src/uncompressed/TweenMax';

import {SectionAnim} from './sectionAnim';
import DrawSVG from '../lib/DrawSVGPlugin';


class Score {
    constructor(el) {
        this.$el = el;
        this.$text = this.$el.querySelector('span');
        this.$track = this.$el.querySelector('.about-st1');

        // Get score value
        this.score = ~~this.$el.getAttribute('data-num');

        // Default properties
        this.triggered = false;
    }

    set() {
        console.log('set');
        // Hide by default
        TweenMax.set(this.$text, {
            autoAlpha: 0
        });

        // Reset to 0%
        this.$text.textContent = "0%";
    }

    animateIn() {

        if (this.triggered) {
            return;
        }

        this.triggered = true;

        // Timeline + dummy for number
        let tl = new TimelineMax();
        let dummy = { nb: 0 };

        // Show text first
        tl.to(this.$text, .2, {
            autoAlpha: 1,
        }, 'draw-track');

        // Start animating text
        tl.to(dummy, 1, {
            nb: this.score,
            ease: SteppedEase.config(this.score),
            onUpdate: () => {
                this.$text.textContent = `${~~dummy.nb}%`;
            },
            onUpdateScope: this
        }, 'draw-track+=.25')

        // Animate circle
        tl.fromTo(this.$track, 1.4, {
            drawSVG: 0 + '%',
            ease: Circ.easeOut
        },
        {
            drawSVG: this.score + '%',
            onComplete: () => {
                // FIXME IE force linecanp
                this.$track.style.strokeLinecap = "round";
            },
            onCompleteScope: this,
            autoAlpha: 1
        }, 'draw-track+=.35');

    }

}

export class AboutSectionAnim extends SectionAnim {
    constructor(params) {
        super(params);

        bindAll(this, ['onScroll']);
    }

    getElements() {
        // Properties
        this.$scores = null;
        this._totalScores = 0;
        this.circles = [];

        // Assign scores
        this.$scores = toArray(document.querySelectorAll('.about__tracks__item__score'));
        this._totalScores = this.$scores.length;
        this.$scores.map( score => {
            this.circles.push( new Score(score) );
        });

        // Set defautl values and style
        this.circles.map( circle => {
            circle.set();
        })
    }

    // DOM Events - scroll
    onScroll(e) {
        for (let i = 0, l = this._totalScores; i < l; i++) {
            if (inViewport(this.$scores[i])) {
                if (!this.triggered) {
                    this.circles[i].animateIn();
                }
            }
        }
    }
}
