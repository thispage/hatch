import {SectionAnim} from './sectionAnim';
import {GMap} from '../gMap.js';

export class ContactSectionAnim extends SectionAnim {
	bindFunctions() {
		this.handleSiteResize = this.handleSiteResize.bind(this);
		this.resizeContent = this.resizeContent.bind(this);
	}

	getElements() {

		this.section = document.getElementById('section_contact');

		this.mapWrap = this.section.getElementsByClassName('contact__map-wrap')[0];

		if (typeof this.mapWrap !== 'undefined') {
			this.map = this.mapWrap.getElementsByClassName('__map')[0];
		}

		this.formWrap = this.section.getElementsByTagName("form")[0];
		this.lastP = this.formWrap.getElementsByTagName("p");
		this.lastP = this.lastP[this.lastP.length-1];

		this.updateWrapMapHeight();

		// this.button  =  document.createElement("button");
		// var span  =  document.createElement("span");


		// this.button.className = "section__smallbtn section__smallbtn--blue";
		// this.lastP.appendChild(this.button);
		// this.button.appendChild(span);
		// span.textContent = "SEND";
	}

	/**
	 * Triggers the section animations
	 */
	trigger(params) {
		if(!this.triggered) {
			this.triggered = true;
			if(window.gMapScript) this.initMap();
			this.timeline = params.timeline;
		}

		if (this.map) this.map.resize();
		this.mapIn = true;

		if (typeof this.mapWrap !== 'undefined' && typeof this.map !== 'undefined') {
			TweenLite.set(this.mapWrap,{height:this.wrapTargetHeight,ease:Circ.easeInOut});
			TweenLite.set(this.map,{height:this.wrapTargetHeight});
			TweenLite.set(this.mapWrap, {scale:1,force3D:true,ease:Sine.easeInOut, onComplete: this.resizeContent});
		}
	}

	/**
	 * Inits gmap
	 */
	initMap() {
		this.handleSiteResize();
		let mapContainer = document.getElementById('contactMap');

		if (mapContainer) {
			let coordinates = this.getCoordinatesData();

			this.map = this.map || new GMap({
				mapContainer,
				coordinates
			});
		}
	}

	/**
	 * Get Offices' Coordinates
	 */
	getCoordinatesData() {
		let offices = document.getElementsByClassName('contact__offices__address');
		let coordinates = [];

		for(let x = 0; x < offices.length; x++) {
			//let data = offices[x].dataset;

			if(offices[x].getAttribute('data-lat') && offices[x].getAttribute('data-lng')){
				coordinates.push( { lat: offices[x].getAttribute('data-lat')  * 1, lng: offices[x].getAttribute('data-lng') * 1 });
			}
		}

		return coordinates;
	}

	/**
	 * Resizes map wrap
	 */
	updateWrapMapHeight() {
		this.winWidth = window.innerWidth;
		if (this.winWidth<=1100) this.wrapTargetHeight = 300;
		else this.wrapTargetHeight = 500;

		if (typeof this.mapWrap !== 'undefined') {
			this.mapWrapHeight = this.mapWrap.offsetHeight;
		}
	}

	handleSiteResize() {
		if (this.map) this.map.resize();
		this.updateWrapMapHeight();

		if (typeof this.mapWrap !== 'undefined' && this.mapIn){
			TweenLite.set(this.mapWrap,{height:this.wrapTargetHeight});
			TweenLite.set(this.map,{height:this.wrapTargetHeight});
		}
	}

	resizeContent() {
		window.App.emitter.emit('blockResize');
	}
}
