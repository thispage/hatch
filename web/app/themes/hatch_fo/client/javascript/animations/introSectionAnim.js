import {SectionAnim} from './sectionAnim';
import {ArrowSpriteManager} from '../arrowSpriteManager';
import {VideoManager} from '../videoManager';
import {VideoButton} from './VideoButton';

export class IntroSectionAnim extends SectionAnim {
	constructor(params) {
		super(params);

		this.videoManager = new VideoManager({
			videoBtn: this.videoBtn
		});
		this.arrowSpriteManager = new ArrowSpriteManager({
			spriteContainer:this.spriteContainer,
			arrowNumber: 0
		});
	}

	getElements() {
		this.section = document.getElementById('section_intro');

		this.videoBtn = this.section.getElementsByClassName('intro__playBtn__btn')[0];
		this.videoBtnInteraction = new VideoButton(this.videoBtn);
		this.spriteContainer = document.querySelector('.section__arrow--intro').querySelector('.section__arrow__bg');
	}

	/**
	 * Triggers the section animations
	 */
	trigger(params) {
		if(!this.triggered) {
			this.videoManager.init();
			this.triggered = true;
		}
	}
}
