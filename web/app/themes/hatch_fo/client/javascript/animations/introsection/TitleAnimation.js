import SplitText from '../../lib/SplitText';
export class TitleAnimation {
	constructor(params) {
		this.target = params.pTargetHeader;
		this.title = this.target.getElementsByClassName("section__header__title")[0];
		this.subtitles = this.target.getElementsByClassName("section__header__subtitle");
		if (this.target.classList.contains('section__header--intro')) {
			this.subtitles = [];
		}
		this.pDelay = params.pDelay;
		this.binds();
		if (this.title !== undefined && this.subtitles !== undefined) this.prepareIn();
	}

;

	animateIn(pDelay = 0, pCallBack = null) {
		TweenLite.delayedCall(pDelay, () => {

			if (typeof this.timeline !== 'undefined') {
				this.timeline.resume();
			}
		});
		this.cbIn = pCallBack;
	}

	binds() {
		this.subtitleStart = this.subtitleStart.bind(this);
		this.animateInComplete = this.animateInComplete.bind(this);
	}

	animateInComplete() {
		if (this.cbIn)this.cbIn();
	}




	prepareIn() {
		let timelines = [];


		timelines.push(this.prepareLineAnimation(this.title, 1, 1, 0, null, null));

		if(this.subtitles.length > 0) {
			for(let i = 0; i < this.subtitles.length; i++) {
				this.subtitle = this.subtitles[i];

				if (this.subtitle) {
					var spans = this.subtitle.getElementsByTagName("span");
					if (spans.length == 0) {
						timelines.push(this.prepareLineAnimation(this.subtitle, 1.3, 1, timelines[0].totalDuration() * .3 + this.pDelay * i, this.subtitle) );
					}
					else {
						timelines.push(this.prepareLineAnimation(spans[0], 1.3, 1, timelines[0].totalDuration() * .3+ this.pDelay * i, this.subtitle));
					}
				}
			}
			this.timeline = new TimelineLite({tweens: timelines, paused: true, onComplete: this.animateInComplete});
		}
		else {
			this.timeline = new TimelineLite({tweens: timelines, paused: true, onComplete: this.animateInComplete});
		}




	}

	prepareLineAnimation(pTarget, pAccel = 1, pDelayAccel = 1, pDelay = 0, callbackEl) {
		pTarget.splitText = new SplitText(pTarget, {type: "chars"});
		let chars = pTarget.splitText.chars;
		let a = 0;
		let array = [];
		let len = chars.length;
		let cLine;
		let cDelay = 0;
		let delaySpace = pDelayAccel * .03;
		while (a < len, cLine = chars[a]) {
			if (cLine) {

				cDelay = a * delaySpace;
				cLine.style.opacity = 0;
				TweenLite.set(cLine, {scale: 1.2, transformOrigin: "50% 100%"});
				array.push(new TweenLite(cLine, .3, {ease: Sine.easeInOut, delay: cDelay, opacity: 1}));
				array.push(new TweenLite(cLine, .1, {ease: Circ.easeIn, delay: cDelay, scale: 1.4}));
				array.push(new TweenLite(cLine, .3, {ease: Circ.easeOut, delay: cDelay + .1, scale: 1}));
				a++
			}

		}

		let response = new TimelineLite({delay: pDelay, tweens: array, onStart: () => {
			if(callbackEl) {
				this.subtitleStart(callbackEl);
			}
		}});
		response.timeScale(pAccel);
		array.length = 0;
		return response;

	}

	subtitleStart(callbackEl) {
		callbackEl.classList.add("animate-border");
	}

;
}
