export class SimplePoint {
	constructor(pX,pY) {
		this.x = pX;
		this.y = pY;
	}

	get x (){
		return this.xVal;
	}

	get y (){
		return this.yVal;
	}


	set x (pX)	{
		this.xVal = pX;

	}

	set y (pY)	{
		this.yVal = pY;

	}

}