import {SvgCircle} from './SvgCircle';

export class LineAnimationsVertical {
	constructor(pTarget,pExtended,startCb) {
		this.binds();
		this.target = pTarget;
		this.path = this.target.querySelector(".center_line");
		this.circleStart = new SvgCircle(this.target.querySelector(".edge_circ_left"));
		this.circle1 = new SvgCircle(this.target.querySelector(".idea_circ_1"),this.target.querySelector(".light_bulb"),55,69,{r:255,g:255,b:255});
		this.circle2 = new SvgCircle(this.target.querySelector(".idea_circ_2"),this.target.querySelector(".falling_numbers"),54,64,{r:255,g:255,b:255});
		this.circle3 = new SvgCircle(this.target.querySelector(".idea_circ_3"),this.target.querySelector(".shopping_cart"),75,65,{r:255,g:255,b:255});
		this.circleEnd = new SvgCircle(this.target.querySelector(".edge_circ_right"));


		let targetY1=0;
		let targetY2=0;
		let targetY3 =0;
		let targetYEnd =0;

		if (pExtended)	{
			targetY1 = 138;
			targetY2 = 339;
			targetY3 = 539;
			targetYEnd = 677;
		} else {
			targetY1 = 128;
			targetY2 = 309;
			targetY3 = 489;
			targetYEnd = 600;
		}
		//--------------------

		let t1 = new TweenLite(this.circleStart,1.5,{delay:1,ease:Sine.easeOut,y:8});
		let t2 = new TweenLite(this.circleEnd,3,{delay:.1,ease:Power2.easeInOut,y:targetYEnd});

		let startTime1=1.5;
		let startTime2=2.4;
		let startTime3=3.6;
		//--------------------

		let slideEase = Quad.easeOut;
		let scaleEase = Quad.easeOut;
		let circleScaleSpeed = 2;
		let slideDuration=2;

		TweenLite.set(this.circleStart.target,{ drawSVG:"0%"});
		TweenLite.set(this.circleEnd.target,{ drawSVG:"0%"});




		let t3 = new TweenLite(this.circle1,slideDuration,{ease:slideEase,delay:startTime1,y:targetY1});
		let t4 = new TweenLite(this.circle1,circleScaleSpeed,{ease:scaleEase,delay:startTime1-.25,radius:78});
		let t5 = this.circle1.animateIn(startTime1+.7);


		let t6 = new TweenLite(this.circle2,slideDuration,{ease:slideEase,delay:startTime2,y:targetY2});
		let t7 = new TweenLite(this.circle2,circleScaleSpeed,{ease:scaleEase,delay:startTime2-.25,radius:78});
		let t8 = this.circle2.animateIn(startTime2+.1);

		let t9 = new TweenLite(this.circle3,slideDuration,{ease:slideEase,delay:startTime3,y:targetY3});
		let t10 = new TweenLite(this.circle3,circleScaleSpeed,{ease:scaleEase,delay:startTime3-.25,radius:78});
		let t11 = this.circle3.animateIn(startTime3+.1);

		let t12 = new TweenLite(this.circleEnd.target,1.5,{ease:Power3.easeInOut,drawSVG:"100%"});
		let t13 = new TweenLite(this.circleStart.target,1.5,{ ease:Power3.easeInOut,drawSVG:"100%"});


		this.inComplete = false;
		this.timeline  =new TimelineLite({tweens:[t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13],onStart:startCb,onUpdate:this.updatePath,paused:true,onComplete:this.onComplete});
		this.timeline.timeScale(2);
		this.updatePath();


	};

	onComplete() {
		this.inComplete = true;
		if (this.cbIn) this.cbIn();

	}

	extendedState() {
		if (!this.inComplete)return;

		TweenLite.to (this.circle1,.3,{ease:Sine.easeInOut,y:138});
		TweenLite.to (this.circle2,.5,{ease:Sine.easeInOut,y:339});
		TweenLite.to (this.circle3,.7,{ease:Sine.easeInOut,y:539});
		TweenLite.to (this.circleEnd,.7,{ease:Sine.easeInOut,y:677,onUpdate:this.updatePath});

	}

	regularState() {
		if (!this.inComplete)return;

		TweenLite.to (this.circle1,.3,{ease:Sine.easeInOut,y:128});
		TweenLite.to (this.circle2,.5,{ease:Sine.easeInOut,y:309});
		TweenLite.to (this.circle3,.7,{ease:Sine.easeInOut,y:489});
		TweenLite.to (this.circleEnd,.7,{ease:Sine.easeInOut,y:600,onUpdate:this.updatePath});

	}

	animateIn(pDelay =0,pCallBack = null) {
		this.target.querySelector('.integration__steps_icons').style.visibility = 'visible';
		TweenLite.delayedCall(pDelay, () =>{ this.timeline.resume()});
		this.cbIn = pCallBack;

	}

	binds() {
		this.updatePath = this.updatePath.bind(this);
		this.onComplete = this.onComplete.bind(this);
	};

	translateInBetween(pCircle) {
		let response = "";
		let sideY = pCircle.sideY;
		response += "L " +pCircle.x+ " " + sideY;
		response += " M " +pCircle.x+ " " + (sideY+pCircle.diameter);
		//
		return response;
	}
;


	translateStartPos(pCircle) {
		let response = "";
		response += "M " +pCircle.x+ " " + pCircle.sideYTop;
		return response;
	};

	translateEndPos(pCircle) {
		let response = "";
		response += "L " +pCircle.x+ " " + pCircle.sideY;
		return response;
	};


	updatePath() {
		var diffX= this.circleEnd.y - this.circleStart.y;
		let path = "";
		if (diffX<3)return;
		if (this.circleStart.y == this.circleEnd.y)return;
		path += this.translateStartPos(this.circleStart);
		if (this.circle1.radius >0)path += this.translateInBetween(this.circle1);
		if (this.circle2.radius >0)path += this.translateInBetween(this.circle2);
		if (this.circle3.radius >0)path += this.translateInBetween(this.circle3);
		path += " "+this.translateEndPos(this.circleEnd,true)+"z";
		this.path.setAttribute("d", path);
	}
;

}
