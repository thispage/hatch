export class Color {
	constructor(pR, pG, pB, pA) {
		this.updateColor = this.updateColor.bind (this);
		this.setColor(pR, pG, pB, pA);
	}
;

	translateRGB() {
		return "rgb(" + ~~(this.r) + "," + ~~(this.g) + "," + ~~(this.b) + ")";
	}

;

	translateRGBA() {
		return "rgba(" + ~~(this.r) + "," + ~~(this.g) + "," + ~~(this.b) + "," + (this.a) + ")";
	}

;

	updateColor() {
		var rgba = this.translateRGBA();
		this.target.style[this.style] = rgba;
	}

;

	setTarget(pTarget, pStyle) {
		this.style = (!pStyle) ? "background" : pStyle;
		this.target = pTarget;
		this.updateColor();
	}

;


	hex2rgb(hex) {
		if (hex[0] == "#") hex = hex.substr(1);
		if (hex.length == 3) {
			var temp = hex;
			hex = '';
			temp = /^([a-f0-9])([a-f0-9])([a-f0-9])$/i.exec(temp).slice(1);
			for (var i = 0; i < 3; i++) hex += temp[i] + temp[i];
		}
		var triplets = /^([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})$/i.exec(hex).slice(1);
		return {
			r: parseInt(triplets[0], 16),
			g: parseInt(triplets[1], 16),
			b: parseInt(triplets[2], 16),
			a: 1
		}
	}

;

	rgb2hex(pR, pG, pB) {
		return '#' + ('00000' + (pR << 16 | pG << 8 | pB[2]).toString(16)).slice(-6).toUpperCase();
	}

;

	rgb2hex2(pR, pG, pB) {
		return ( pR * 255 << 16 ) + (pG * 255 << 8 ) + pB * 255;
	}

;

	setColor(pR, pG, pB, pA) {
		pR = ~~(pR);
		pG = ~~(pG);
		pB = ~~(pB);
		pA = ~~(pA);

		pR = (!pR) ? 0 : pR;
		pG = (!pG) ? 0 : pG;
		pB = (!pB) ? 0 : pB;
		pA = (!pA) ? 0 : pA;

		this.r = pR;
		this.g = pG;
		this.b = pB;
		this.a = pA;

		if (this.target) this.updateColor();
	}

;
	animateToColor(pR, pG, pB, pA,pTime,pDelay, pEase, pTarget, pStyle) {
		pDelay=(!pDelay)?0:pDelay;
		if (pTarget) this.setTarget(pTarget, pStyle);
		pTime = (!pTime) ? .3 : pTime;
		pEase = (!pEase) ? Sine.easeInOut : pEase;
		return new TweenLite(this, pTime, {delay:pDelay,ease: pEase, r: pR, g: pG, b: pB, a:pA,onUpdate: this.updateColor});

	}

;


}
