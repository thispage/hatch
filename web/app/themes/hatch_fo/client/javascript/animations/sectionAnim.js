import {RoundButton} from './RoundButton';
export class SectionAnim {
	constructor(params) {
		this.section = params.section;
		this.sectionController = params.sectionController;
		this.suffix = params.suffix;
		this.bindFunctions();
		this.getElements();
		//this.setListeners();
		//this.setAnim();
		window.addEventListener("resize", this.handleSiteResize);
	}

	prepareHoverButtons(pTargetSel)	{
		var search = this.section.getElementsByClassName(pTargetSel);
		var lim = search.length;
		var cElem;
		if (!this.hoverButtons)this.hoverButtons = [];
		while ( --lim >=0,cElem = search[lim]) this.addHoverButton(cElem);
	}

	addHoverButton(pTargetSel){
		if (!this.findHoverButton(pTargetSel)){
			this.hoverButtons.push(new RoundButton(pTargetSel));
		}
	}

	findHoverButton(pTargetSel)	{
		
		var lim = this.hoverButtons.length;
		var cElem;
		while ( --lim >=0,cElem = this.hoverButtons[lim])if (pTargetSel === cElem.target) return cElem;
		return null;
	}

	selectHoverButton (pTargetSel)	{
		let response = this.findHoverButton(pTargetSel);
		if (response != null) response.select();
		return response;
	}

	deSelectHoverButton (pTargetSel)	{
		let response = this.findHoverButton(pTargetSel);
		if (response != null) response.select();
		return response;
	}


	bindFunctions() {
		
	};

	getElements() {
		
	};

	setListeners() {

	};

	handleSiteResize() {
		
	};

}