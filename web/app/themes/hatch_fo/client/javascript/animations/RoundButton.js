import Raphael from 'raphael';

export class RoundButton {
	constructor(pTarget) {
		//--
		this.update = this.update.bind(this);
		this.hoverIn = this.hoverIn.bind(this);
		this.hoverOut = this.hoverOut.bind(this);
		this.hoverOutComplete = this.hoverOutComplete.bind(this);
		//--
		this.target = pTarget;
		//--
		this.copy = this.target.getElementsByTagName("span")[0];
		
		// this.buttonW = this.getStyle(this.copy, "width");
		// this.buttonH = this.getStyle(this.copy, "height");
		// this.buttonW = ~~(this.buttonW.replace("px", ""));
		// this.buttonH = ~~(this.buttonH.replace("px", ""));
		this.buttonW = this.copy.offsetWidth;
		this.buttonH = this.copy.offsetHeight;

		this.raphael = Raphael(0, 0, this.buttonW, this.buttonH);
		this.svgCanvas = (this.raphael.canvas);
		this.target.appendChild((this.svgCanvas));
		this.svgCanvas.style.overflow = "hidden";
		this.svgCanvas.removeChild(this.svgCanvas.getElementsByTagName("desc")[0]);
		this.svgCanvas.removeChild(this.svgCanvas.getElementsByTagName("defs")[0]);
		this.svgCanvas.removeAttribute("preserveAspectRatio");
		this.targetRect = this.raphael.rect(0, 0, 1, 1, 3)[0];
		this.targetRect.setAttribute("fill", "none");
		this.targetRect.setAttribute("stroke", "none");
		//--
		this.prepareHover();
		this.resize();
		this.target.addEventListener("mouseenter", this.hoverIn);
		//--
		
	};

	hoverIn() {
		if (this.isSelected) return;
		this.target.removeEventListener("mouseenter", this.hoverIn);
		this.target.addEventListener("mouseleave", this.hoverOut);
		this.isIn = true;
		TweenLite.to(this.inTline, this.inTline.totalDuration() * .4, {
			overwrite: 1,
			ease: Linear.easeNone,
			progress: 1
		});

	};


	select() {
		this.isSelected=true;
		if (this.isSelected)TweenLite.to(this.inTline,.3,  {progress: 1});

	}

	deselect() {
		this.isSelected=false;
		TweenLite.to(this.inTline, .3, {ease: Linear.easeNone, progress: 0, onComplete: this.hoverOutComplete});
		this.target.addEventListener("mouseenter", this.hoverIn);


	}
	prepareHover() {
		this.centerX = true;
		this.centerY = false;
		this.width = this.buttonW;
		this.height = 0;
		this.roundness = 10;
		if (this.target.className.indexOf("blue") != -1)this.rgb = {r: 255, g: 129, b: 89};
		else this.rgb = {r: 77, g: 77, b: 77};


		let t1 = new TweenLite(this, .4, {ease: Circ.easeInOut, width: this.buttonW});
		let t2 = new TweenLite(this, .4, {ease: Circ.easeInOut, height: this.buttonH});
		let t3 = new TweenLite(this, .5, {ease: Quad.easeOut, y: 0});
		let t4 = new TweenLite(this, .6, {ease: Linear.easeNone, roundness: 3});
		this.inTline = new TimelineLite({tweens: [t1, t2, t3, t4],paused:true,onUpdate: this.update});
		this.inTline.timeScale(3);


	}

	hoverOut() {
		if (this.isSelected) return;
		this.isIn = false;
		this.target.removeEventListener("mouseleave", this.hoverOut);
		this.target.addEventListener("mouseenter", this.hoverIn);
		TweenLite.to(this.inTline, .3, {ease: Linear.easeNone, progress: 0, onComplete: this.hoverOutComplete});
	}

;


	hoverOutComplete() {
		this.inTline.stop();
	}

;

	resize() {
		this.x = this.buttonW * .5;
		this.y = -1 * (this.buttonH );
		
		// this.buttonW = this.getStyle(this.copy, "width");
		// this.buttonH = this.getStyle(this.copy, "height");
		// this.buttonW = ~~(this.buttonW.replace("px", ""))+1;
		// this.buttonH = ~~(this.buttonH.replace("px", ""));
		
		this.buttonW = this.copy.offsetWidth;
		this.buttonH = this.copy.offsetHeight;
		
		this.raphael.setViewBox(0, 0, this.buttonW, this.buttonH, false);
		this.svgCanvas.setAttribute("width",this.buttonW);
		this.svgCanvas.setAttribute("height",this.buttonH);
		if(this.inTline){
			this.inTline.kill();
			this.prepareHover();
			if(this.isSelected) {
				this.inTline.progress(1);
			}
		}

		this.update();


	}

;



	update() {
		if (this.centerX)this.x = (this.buttonW - this.width) * .5;
		if (this.centerY)this.y = (this.buttonH - this.height) * .5;
		this.targetRect.setAttribute("x", this.x);
		this.targetRect.setAttribute("y", this.y);
		this.targetRect.setAttribute("width", this.width);
		this.targetRect.setAttribute("height", this.height);
		this.targetRect.setAttribute("rx", this.roundness);
		this.targetRect.setAttribute("ry", this.roundness);
		this.targetRect.setAttribute("fill", "rgb(" + ~~(this.rgb.r) + "," + ~~(this.rgb.g) + "," + ~~(this.rgb.b) + ")");
	}

}
