export class OverlayManager {
	constructor(params) {
		this.overlayContainer = document.getElementById('overlay-container');
		this.overlay = params.overlay;
		
		if (typeof this.overlay === 'undefined') {
			return false;
		}
		
		this.bg = this.overlay.getElementsByClassName('overlay__background')[0];
		
		if (typeof this.bg === 'undefined') {
			return false;
		}
		
		this.polygonBg = this.overlay.getElementsByClassName('polygon-cache')[0];
		this.closeBtn = this.overlay.getElementsByClassName('overlay__close')[0];
		this.content = this.overlay.getElementsByClassName('overlay__content')[0];
		this.iframe = this.content.querySelector('iframe');

		this.clickEvent = window.isMobile ? 'touchend' : 'click';

		this.polyPoints = {
			x1: 0, 		y1: 0,
			x2: 0, 		y2: 0,
			x3: 0, 		y3: 0,
			x4: 0, 		y4: 0,
			angle: 45
		}

		this.bindFunctions();
		this.setEmitterEvents();
	}

	bindFunctions() {
		this.hide = this.hide.bind(this);
		this.setPolyPoints = this.setPolyPoints.bind(this);
		this.onResize = this.onResize.bind(this);
		this.scrollToOverlay = this.scrollToOverlay.bind(this);
		this.checkOutsideClick = this.checkOutsideClick.bind(this);
	}

	setEmitterEvents() {
		window.App.emitter.on('hideOverlay', this.hide);
		window.App.emitter.on('focusout', this.scrollToOverlay);
	}

	setListeners() {
		this.closeBtn.addEventListener(this.clickEvent, this.hide);
		this.overlayContainer.addEventListener(this.clickEvent, this.checkOutsideClick);

		window.addEventListener('resize', this.onResize);
	}

	removeListeners() {
		this.closeBtn.removeEventListener(this.clickEvent, this.hide);
		this.overlayContainer.removeEventListener(this.clickEvent, this.checkOutsideClick);
		window.removeEventListener('resize', this.onResize);
	}

	show(cb) {
		this.shown = true;
		this.overlayContainer.classList.add('visible');
		this.overlay.classList.remove('no-display');
		// Stop page scroll
		this.disableScroll();

		if(this.tl) {
			this.tl.kill();
		}

		this.tl = new TimelineLite({onComplete: () => {
			this.shown = true;
			// Add listeners
			this.setListeners();
			// Set video source
			this.setVideoSource();

		}});
		
		//this.tl.to(this.overlay, .3, {scaleX: 1, scaleY: 1});
		// Cache
		this.calculateCoords();
		// this.tl.set(this.overlayContainer, {y: window.pageYOffset, height: window.innerHeight});

		this.tl.fromTo(this.polygonBg, .5, {
			fill: "rgba(0,0,0,.8)"
		},{
			ease: Power2.easeOut,
			fill: "rgba(0,0,0,.6)",
		}, 'appear');

		this.tl.fromTo(this.polyPoints, .5, {
			x1: this.width, 	y1: this.height,
			x2: this.width, 	y2: this.height,
			x3: this.width, 	y3: this.height,
			x4: this.width, 	y4: this.height
		},{
			ease: Power2.easeOut,
			x1: this.width, 	y1: -this.y3,
			x2: this.width, 	y2: this.height,
			x3: this.width, 	y3: this.height,
			x4: -this.x2, 	y4: this.height,

			onUpdate: () => {
				this.setPolyPoints();
			}
		}, 'appear');

		if(typeof cb == 'function') {
			//this.tl.set(this.content, {autoAlpha: 1});
			TweenMax.delayedCall(0, cb);
		}
		else {
			this.tl.to(this.content, .3, {autoAlpha: 1});
		}
	}

	scrollToOverlay() {
		// TweenLite.set(this.overlayContainer, {y: window.pageYOffset, height: window.innerHeight});
	}

	calculateCoords() {
		this.width = window.innerWidth;
		this.height = window.innerHeight;
		this.scrollWidth = window.currentPage === 'index' ? 0 : 20;
		this.x2 = Math.tan(this.polyPoints.angle * Math.PI/180) * this.width + this.scrollWidth;
		this.y3 = this.height / Math.tan(this.polyPoints.angle * Math.PI/180);

		this.bg.setAttribute('viewBox', '0 0 ' + this.width + ' ' + this.height);
		this.bg.setAttribute('width', this.width);
		this.bg.setAttribute('height', this.height);
	}

	setPolyPoints() {
		this.polygonBg.setAttribute("points", this.polyPoints.x1 + "," + this.polyPoints.y1 + " " + this.polyPoints.x2 + "," + this.polyPoints.y2 + " " + this.polyPoints.x3 + "," + this.polyPoints.y3 + " " + this.polyPoints.x4 + "," + this.polyPoints.y4);
	}

	hide() {
		if(!this.shown) {
			return;
		}

		this.shown = false;
		let width = window.innerWidth;
		let height = window.innerHeight;
		this.bg.setAttribute('viewBox', '0 0 ' + width + ' ' + height);
		this.bg.setAttribute('width', width);
		this.bg.setAttribute('height', height);
		
		if(this.tl) {
			this.tl.kill();
		}

		this.tl = new TimelineLite({onComplete: () => {
			this.shown = false;
			this.overlayContainer.classList.remove('visible');
			this.overlay.classList.add('no-display');
			// Remove listeners
			this.removeListeners();
			// Put page scroll back in
			this.enableScroll();
			// Remove video
			this.removeVideo();

			window.App.emitter.emit('overlayClose');

		}});
		

		//this.tl.to(this.content, .3, {autoAlpha: 0});
		this.tl.to(this.polyPoints, .3, {
			ease: Power1.easeIn,
			x1: width, 		y1: height,
			x2: width, 		y2: height,
			x3: width, 	    y3: height,
			x4: width, 		y4: height,

			onUpdate: () => {
				this.setPolyPoints();
				
			}
		});
		//this.tl.to(this.overlay, .3, {scaleX: 0, scaleY: 0});

	}

	disableScroll() {
		window.App.emitter.emit('disableScroll');
	}

	enableScroll() {
		window.App.emitter.emit('enableScroll');
	}

	onResize() {
		TweenLite.set(this.overlayContainer, {height: window.innerHeight});
		this.calculateCoords();
		TweenLite.set(this.polyPoints, {
			x1: this.width, 	y1: -this.y3,
			x2: this.width, 	y2: this.height,
			x3: this.width, 	y3: this.height,
			x4: -this.x2, 		y4: this.height
		});
		this.setPolyPoints();
	}


	checkOutsideClick(e) {
		let nodeName = e.target.nodeName;
		if(e.target === this.overlayContainer || nodeName === 'polygon' || nodeName === 'svg') {
			e.preventDefault();
			this.hide();
		}
	}

	setVideoSource() {
		if(this.iframe) {
			let src = this.iframe.getAttribute('data-link');
			if(src) {
				this.iframe.setAttribute('src', src);
			}
		}
	}

	removeVideo() {
		if(this.iframe) {
			this.iframe.setAttribute('src', '');
		}
	}
}

