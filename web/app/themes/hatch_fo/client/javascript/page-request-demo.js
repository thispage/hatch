import {PageManager} from './pageManager';

export class RequestDemo extends PageManager {
	constructor() {
		super();
	}

	/**
	 * Initialize
	 */
	init() {
		super.init();
		
		var _this = this;
		
		_this.counter = document.getElementById('revenue_counter');
				
		window.addEventListener('resize', function() {
			_this.resizeCounter();
		});
		
		_this.resizeCounter();
		
		// Set counter
		var startAmount = this.counter.getAttribute('data-start_amount');
		var startAmountInt = parseInt(startAmount);
		
		var stepSpeedSetting = this.counter.getAttribute('data-step_speed');
		var amountPerStep = parseInt(this.counter.getAttribute('data-amount_per_step'));
		
		var numbers = startAmount.split('');
		var numberBlocks = this.counter.getElementsByClassName('number');
		var numberBlocksLength = numberBlocks.length;
		var numbersLength;
		
		var intervalTime = 1000; // 1 second
		var i;
		
		if (stepSpeedSetting == 'half a second') {
			intervalTime = intervalTime / 2;
		} else if (stepSpeedSetting == 'minute') {
			intervalTime = intervalTime * 60;
		}
		
		var stepInterval = setInterval(function() {
			// Calculate new amount
			startAmountInt += amountPerStep;
			
			// Split items into new array
			numbers = (startAmountInt+'').split('');
			numbersLength = numbers.length;
			
			// Add leading zeros
			if (numbersLength < numberBlocksLength) {
				for (i = 0; i < (numberBlocksLength - numbersLength); i++) {
					numbers.unshift(0);
				}
			}
			
			// Update counter
			for (i = 0; i < numberBlocksLength; i++) {
				numberBlocks[i].innerHTML = numbers[i];
			}
			
		}, intervalTime);
		
	}
	
	resizeCounter() {
		var counterWidth = 1122;
		var scalePercentage;
		
		if (window.innerWidth <= 1163) {
			
			scalePercentage = ((window.innerWidth - 42) / 2) / (counterWidth / 2);
			
			this.counter.style.transform = 'scale('+scalePercentage+')';
			
		} else {
			this.counter.style.transform = 'scale(1)';
			this.counter.style.width = counterWidth+'px';
		}
	}
}