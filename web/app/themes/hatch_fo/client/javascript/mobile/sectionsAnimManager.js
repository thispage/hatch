/**
 * Class Sections Anim Manager
 * Holds each section's animation
 */

import {SolutionsSectionAnim} from './animations/solutionsSectionAnim';

export class SectionsAnimManager {
	constructor(params) {
		this.sections = params.sections;
		this.animations = [];
		this.init();
	}

	init() {
		this.storeSectionAnimations();
	}

	/**
	 * Stores the section animations in this.sections array for further dynamic call
	 */
	storeSectionAnimations() {
		
		this.animations.push(new SolutionsSectionAnim({
			suffix: 'solutions',
			section: this.sections[2]
		}));
	}

	/**
	 * Triggers section animation
	 */
	triggerAnimation(sectionNumber) {
		// if(this.timeline) {
		// 	this.timeline.kill();
		// 	this.timeline.clear();
		// }

		// this.timeline = new TimelineLite();

		if(this.animations[sectionNumber]) {		
			this.animations[sectionNumber].trigger({
				//timeline: this.timeline
			})
		}

	}

	/**
	 * Triggers section titles animation
	 */
	triggetTitlesAnimation(sectionNumber, duration) {
		if(this.animations[sectionNumber] && typeof this.animations[sectionNumber].moveTitles == 'function') {		
			this.animations[sectionNumber].moveTitles({
				duration: duration
			})
		}
	}


}