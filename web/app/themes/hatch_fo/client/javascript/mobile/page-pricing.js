import {PageManager} from '../pageManager';
import {OverlayManager} from '../overlayManager';
import {FormManager} from '../formManager';


export class Pricing extends PageManager {
	constructor() {
		super();
	}

	/**
	 * Initialize
	 */
	init() {
		super.init();
		
		this.bindFunctions();
		this.getElements();
		this.setListeners();
		this.setEmitterEvents();

		this.formManager = new FormManager({
			action: this.formOverlay.querySelector('form').getAttribute('action'),
			submitBtn: document.getElementById('planSubmitBtn'),
			contactFormData: {
				id: this.formOverlay.querySelector('input[name="_wpcf7"]').getAttribute('value'),
				version: this.formOverlay.querySelector('input[name="_wpcf7_version"]').getAttribute('value'),
				lang: this.formOverlay.querySelector('input[name="_wpcf7_locale"]').getAttribute('value'),
				tag: this.formOverlay.querySelector('input[name="_wpcf7_unit_tag"]').getAttribute('value'),
				once: this.formOverlay.querySelector('input[name="_wpnonce"]').getAttribute('value')
			},
			inputs: {
				planName: document.getElementById('planName'),
				planContactName: document.getElementById('planContactName'),
				planContactEmail: document.getElementById('planContactEmail')
			}
		});


		
	}

	/**
	 * Bind functions
	 */
	bindFunctions() {
		this.onBtnClick = this.onBtnClick.bind(this);
		this.enableScroll = this.enableScroll.bind(this);
		this.disableScroll = this.disableScroll.bind(this);
		this.enableScroll = this.enableScroll.bind(this);
		this.onBodyTouchMove = this.onBodyTouchMove.bind(this);
		this.onBodyTouchStart = this.onBodyTouchStart.bind(this);
	}

	/**
	 * Get Elements
	 */
	getElements() {
		this.formOverlay = document.getElementsByClassName('overlay__getstarted')[0];
		this.formOverlayTriggers = document.getElementsByClassName('rpt_foot');
	}

	/**
	 * Set Listeneres
	 */
	setListeners() {
		for(let i = 0; i < this.formOverlayTriggers.length; i++) {
			this.formOverlayTriggers[i].addEventListener('touchend', this.onBtnClick);
		}

		document.body.addEventListener('touchmove', this.onBodyTouchMove);
		document.body.addEventListener('touchstart', this.onBodyTouchStart);
	}

	setEmitterEvents() {
		window.App.emitter.emit('initMenu');
		window.App.emitter.on('disableScroll', this.disableScroll);
		window.App.emitter.on('enableScroll', this.enableScroll);
	}

	/**
	 * Handle clicks on formOverlayTriggers
	 */
	onBtnClick(e) {
		if(this.dragging) {
			return;
		}
		e.preventDefault();
		this.setPlanName(e.target);
		this.showOverlay();
	}

	/**
	 * Find  and Set Plan Name in the input
	 */
	setPlanName(target) {
		let title = target.parentNode.getElementsByClassName('rpt_title')[0].textContent;
		let planInput = document.getElementById('planName');

		if(title) { planInput.value = title; }

		planInput.placeholder = "Plan";
	}

	/**
	 * Show Overlays
	 */
	showOverlay() {
		this.overlayManager = this.overlayManager || new OverlayManager({
			overlay: this.formOverlay
		});

		this.overlayManager.show();
	}

	onBodyTouchStart() {
		this.dragging = false;
	}

	onBodyTouchMove() {
		this.dragging = true;
	}

	/**
	 * Disable Scroll
	 */
	disableScroll() {
		var hasWheelEvent = 'onwheel' in document;
		var hasMouseWheelEvent = 'onmousewheel' in document;
		if(hasWheelEvent) {
			document.addEventListener('wheel', this.preventEventDefault);
		}
		if(hasMouseWheelEvent) {
			document.addEventListener('mousewheel', this.preventEventDefault);
		}
		document.addEventListener('touchmove', this.preventEventDefault);
	}

	/**
	 * Enable Scroll
	 */
	enableScroll() {
		var hasWheelEvent = 'onwheel' in document;
		var hasMouseWheelEvent = 'onmousewheel' in document;
		if(hasWheelEvent) {
			document.removeEventListener('wheel', this.preventEventDefault);
		}
		if(hasMouseWheelEvent) {
			document.removeEventListener('mousewheel', this.preventEventDefault);
		}
		document.removeEventListener('mousewheel', this.preventEventDefault, false);
		document.removeEventListener('touchmove', this.preventEventDefault, false);
	}

	/**
	 * Prevent Event's dafault
	 */
	preventEventDefault(e) {
		e.preventDefault();
	}
}

























