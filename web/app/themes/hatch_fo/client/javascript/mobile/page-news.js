import {PageManager} from '../pageManager';


export class News extends PageManager {
	constructor() {
		super();
	}

	/**
	 * Initialize
	 */
	init() {
		super.init();
		
		this.bindFunctions();
		this.getElements();
		this.setListeners();
		this.setEmitterEvents();


		
	}

	/**
	 * Bind functions
	 */
	bindFunctions() {
//		this.onBtnClick = this.onBtnClick.bind(this);
		this.disableScroll = this.disableScroll.bind(this);
		this.enableScroll = this.enableScroll.bind(this);
	}

	/**
	 * Get Elements
	 */
	getElements() {
//		this.textOverlayTriggers = document.getElementsByClassName('section__smallbtn');
	}

	/**
	 * Set Listeneres
	 */
	setListeners() {
//		for(let i = 0; i < this.textOverlayTriggers.length; i++) {
//			this.textOverlayTriggers[i].addEventListener('touchend', this.onBtnClick);
//		}
	}

	setEmitterEvents() {
		window.App.emitter.emit('initMenu');
		window.App.emitter.on('disableScroll', this.disableScroll);
		window.App.emitter.on('enableScroll', this.enableScroll);
	}

	/**
	 * Handle clicks on textOverlayTriggers
	 */
	onBtnClick(e) {
		//this.setOverlayContent(e.target.parentElement);
		//this.showOverlay();
//		this.toggleExpand(e)
	}

	toggleExpand(e) {
		let btn = e.target.nodeName === 'SPAN' ? e.target.parentElement : e.target;
		let card = btn.parentElement;
		if(card.classList.contains('active')) {
			btn.textContent = 'read more';
			card.classList.remove('active');
		}
		else {
			btn.textContent = 'hide';
			card.classList.add('active');	
		}
	}

	/**
	 * Disable Scroll
	 */
	disableScroll() {
		var hasWheelEvent = 'onwheel' in document;
		var hasMouseWheelEvent = 'onmousewheel' in document;
		if(hasWheelEvent) {
			document.addEventListener('wheel', this.preventEventDefault);
		}
		if(hasMouseWheelEvent) {
			document.addEventListener('mousewheel', this.preventEventDefault);
		}
		document.addEventListener('touchmove', this.preventEventDefault);
	}

	/**
	 * Enable Scroll
	 */
	enableScroll() {
		var hasWheelEvent = 'onwheel' in document;
		var hasMouseWheelEvent = 'onmousewheel' in document;
		if(hasWheelEvent) {
			document.removeEventListener('wheel', this.preventEventDefault);
		}
		if(hasMouseWheelEvent) {
			document.removeEventListener('mousewheel', this.preventEventDefault);
		}
		document.removeEventListener('mousewheel', this.preventEventDefault, false);
		document.removeEventListener('touchmove', this.preventEventDefault, false);
	}

	/**
	 * Prevent Event's dafault
	 */
	preventEventDefault(e) {
		e.preventDefault();
	}
}