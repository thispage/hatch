import {SectionsAnimManager} from './sectionsAnimManager';


/**
 * class Section Manager
 * Responsible for handling the different site's sections and entry point for intro / scroller
 */
export class SectionsManager {
	constructor(params) {
		this.pagewrap = params.pagewrap;
		this.intro = params.sections[0];
		this.sectionElts = params.sections;
		this.sectionNames = [
			'logo',
			'intro',
			'solutions',
			'customers',
			'about',
			'contact'
		];
		
		this.bindFunctions();

		this.setEmitterEvents();

		this.sectionsAnimManager = new SectionsAnimManager({
			sections: this.sectionElts
		});
	}

	/**
	 * Binds proper this to class methods
	 * 
	 */
	bindFunctions() {
		this.onResize = this.onResize.bind(this);
		this.onTick = this.onTick.bind(this);
		this.disableResize = this.disableResize.bind(this);
		this.enableResize = this.enableResize.bind(this);
	}

	/**
	 * Sets listeners
	 * 
	 */
	setListeners() {
		window.addEventListener('resize', this.onResize);
		TweenMax.ticker.addEventListener('tick', this.onTick);
	}

	setEmitterEvents() {
		
		window.App.emitter.on('disableResize', this.disableResize);
		window.App.emitter.on('enableResize', this.enableResize);
	}

	disableResize() {
		this.resizeEnabled = false;
	}

	enableResize() {
		this.resizeEnabled = true;
	}

	/**
	 * Catches window resize events
	 * 
	 */
	onResize() {	
		if(this.resizeEnabled) {
			this.resizing = true;
		}
	}

	/**
	 * Catches browser's RAF / TweenMax Tick
	 * 
	 */
	onTick() {
		if(this.resizing) {
			this.resizing = false;
			this.getSectionsCoord();
		}
	}

	/**
	 * Class initialization
	 */
	init() {
		this.setListeners();
		// Checks the slug if we want to init the site at any location
		this.checkSlug();
		//this.SectionsAnimManager
	}

	/**
	 * Binds proper this to class methods
	 * 
	 */
	checkSlug() {
		let hash = window.location.hash.substring(1);
		this.initSection = this.getSectionFromHash(hash)
	}

	getSectionFromHash(hash) {
		for(let i = 0; i < this.sectionNames.length; i++) {
			if(this.sectionNames[i] === hash) {
				return i;
			}
		}
		// Default
		return 1;
	}


}