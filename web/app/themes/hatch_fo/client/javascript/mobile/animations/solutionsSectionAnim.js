import {SectionAnim} from '../../animations/sectionAnim';

export class SolutionsSectionAnim extends SectionAnim {
	constructor(params) {
		super(params);
		this.blockHidden = true;
		this.sliders = [];
		this.bindFunctions();
		this.init();
	}

	bindFunctions() {
	}

	
	getElements() {
		this.contents = this.section.getElementsByClassName('section__content'); 
		this.cards = this.section.getElementsByClassName('solutions__content__card');
		this.blocks = this.section.getElementsByClassName('solutions__content__block');
		this.subTitles = this.section.getElementsByClassName('section__header__subtitle');
		console.log(this.contents);
	}

	createBlocks() {
		
	}

	init() {
		
	}

	/**
	 * Animate titles
	 */
	moveTitles(params) {}

	/**
	 * Triggers the section animations
	 */
	trigger(params) {}

	/**
	 * Sets timeline init values
	 */
	prepareElements() {}

}