import {SectionAnim} from './sectionAnim';
import DrawSVG from '../lib/DrawSVGPlugin';

export class AboutSectionAnim extends SectionAnim {
	constructor(params) {           
		super(params);
		console.log("here");
		

	}

	getElements() {
		this.tracks = document.querySelectorAll('.section--about .about__tracks__item__track .about-st1');
		this.scores = document.getElementsByClassName('about__tracks__item__score');
	
		this.trackProgress = [
			{value: 0, index: 0},
			{value: 0, index: 1},
			{value: 0, index: 2}
		];
	}

	/**
	 * Triggers the section animations
	 */
	trigger() {
		
		this.getTrackValues();
		this.animateIn();
	}

	getTrackValues() {
		this.trackValues = [];
		
		for(let i = 0; i < this.scores.length; i++) {
			this.trackValues.push(this.scores[i].getAttribute('data-num'));
		}
	}

	animateIn() {
		this.triggered = true;
				
		for(let i = 0; i < this.trackValues.length; i++) {
			TweenMax.set(this.tracks[i], {
				drawSVG: this.trackValues[i] + '%',
				opacity: 1
			}, 'draw-track+=' + i / 3);
		}
	}
}