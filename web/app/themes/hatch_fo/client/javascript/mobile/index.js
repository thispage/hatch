
import {PageManager} from '../pageManager';
import toArray from '../lib/toArray';
import bindAll from '../lib/bindAll';


export class Index extends PageManager {
	constructor() {
		super();
		this.bindFunction();

	}

	init() {
		super.init();
		this.getElements();
		this.setListeners();
		console.log(this.$demoRequest);
	}

	getElements() {
		this.$card = document.querySelector('.solutions__content__card')
		this.$cardButton = toArray(document.querySelectorAll('.solutions__content__card .section__smallbtn'));
		this.$cardButtonLinks = toArray(document.querySelectorAll('.solutions__content__card .section__smallbtn span'));
		this.$cardDescription = toArray(document.querySelectorAll('.solutions__content__card__content'));
		this.$demoRequest = document.getElementById('demo__request');
		this.$demoCloseBtn = document.getElementById('request-demo-close');

	}
	setListeners() {
		this.$cardButton.map((el) => {
            el.addEventListener("click", this.openCard);
        });
        this.$demoCloseBtn && this.$demoCloseBtn.addEventListener('click', this.closeDemo);
	}
	bindFunction() {
		bindAll(this, ["openCard", 'closeDemo']);
	}

	openCard(ev) {
		event.preventDefault();

		let $current = ev.currentTarget;
		let $number =  ev.currentTarget.getAttribute('data-id');
		let $currentLink = $current.querySelector('span');
		let $description  = document.querySelector(`[data-desc= "${$number}"]`);

		if ($current.classList.contains('open')) {
			$current.classList.remove('open');
			$currentLink.innerHTML = 'read more';
			$description.classList.remove('open');
		}
		else {
			for (var i = this.$cardButton.length - 1; i >= 0; i--) {
				this.$cardButton[i].classList.remove('open')
				this.$cardButtonLinks[i].innerHTML = 'read more';
				this.$cardDescription[i].classList.remove('open')
			}

			$current.classList.add('open');

			$currentLink.innerHTML = 'read less';
			$description.classList.add('open');
		}


	}
	closeDemo() {
		this.$demoRequest.classList.remove('open');
	}
}
