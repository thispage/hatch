import {PageManager} from './pageManager';
import {IntroSectionAnim} from './animations/introSectionAnim';

export class Index extends PageManager {
	constructor() {
		super();
	}

	init() {
		this.demoSetUp = this.demoSetUp.bind(this);
		super.init();

		this.getElement();
		this.demoSetUp();

		// Homepage animations
		var introAnim = new IntroSectionAnim({});
		introAnim.trigger();
		this.setRequestListener();
	}

	getElement(){
		this.$demo = document.querySelector('#demo__request')
		this.$demoCloseBtn = document.getElementById('request-demo-close');
	}

	demoSetUp() {
		let demoShown = sessionStorage.getItem('demo-request');
		if (!demoShown) {
			setTimeout( ()=> {
				this.$demo.classList.add('open')
			}, 30000);
		}
	}

	setRequestListener() {
		this.$demoCloseBtn.addEventListener('click', () => {
			this.$demo.classList.remove('open');
			sessionStorage.setItem('demo-request', 'shown');
		});
	}
}
