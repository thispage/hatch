/* lib/inViewport.js */

/**
 * inViewport
 *
 * @author karl
 * @param element
 * @returns {Boolean} if element is in viewport or not
 */
export default function inViewport(element) {
    let rect = element.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
}
