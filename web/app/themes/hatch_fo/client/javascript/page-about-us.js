import {PageManager} from './pageManager';
import {AboutSectionAnim} from './animations/aboutSectionAnim';
import inViewport from "./lib/inViewport";

export class AboutUs extends PageManager {
	constructor() {
		super();
        this.columns = document.querySelector('.section__content.about__description').querySelectorAll('p');
	}

	/**
	 * Initialize
	 */
	init() {
		super.init();
		if (!window.isMobile && !window.isTablet) this.checkScroll();
		// var aboutAnim = new AboutSectionAnim({});

        // Register onScroll
        // window.addEventListener('scroll', aboutAnim.onScroll);

        // Trigger default onScroll
        // aboutAnim.onScroll();
	}

	checkScroll() {
		if (inViewport(this.columns[0])) this.animateColumns();
		else {
			window.addEventListener('scroll', inView);
		}
		const self = this;
		function inView() {
			if (inViewport(self.columns[0])) {
				window.removeEventListener('scroll', inView)
				self.animateColumns();
			}
		}
	}

    animateColumns() {
		TweenMax.fromTo(this.columns[0], 0.5, {
			autoAlpha: 0,
			x: -150
		}, {
			autoAlpha: 1,
			x: 0,
		});
		TweenMax.fromTo(this.columns[1], 0.5, {
			autoAlpha: 0,
			x: 150
		}, {
			autoAlpha: 1,
			x: 0,
		});
    }
}
