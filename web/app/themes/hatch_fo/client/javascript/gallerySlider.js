import {GalleryOverlayManager} from './galleryOverlayManager';

export class GallerySlider {

	constructor(params) {
		this.gallery = params.gallery;
		this.currentSlide = params.currentSlide;
		this.slidingInProgress = false;
		this.images = [];
		this.polyPoints = {
			x1: 0, 		y1: 0,
			x2: 0, 		y2: 0,
			x3: 0, 		y3: 0,
			x4: 0, 		y4: 0,
			angle: 30
		}
		this.touchEvent = window.isMobile ? 'touchend' : 'click';
		this.bindFunctions();
		this.init();
	}

	bindFunctions() {
		this.onSliderOver = this.onSliderOver.bind(this);
		this.onSliderOut = this.onSliderOut.bind(this);
		this.onSliderClick = this.onSliderClick.bind(this);
		this.onNavClick = this.onNavClick.bind(this);
		this.onResize = this.onResize.bind(this);
	}

	/**
	 * Initialize
	 * 
	 */
	init() {
		this.getElements();
		this.setNavigation();
		this.setListeners();
		this.setImages();
		this.initSliderOverlay();
		if(window.isTablet) {
			TweenLite.set(this.sliderCoverSVGCache, {fill: "rgba(0,0,0,0.2)"})
			this.showCover()
		}
	}

	/**
	 * Inits slider overlay
	 */
	initSliderOverlay() {
		this.overlay = new GalleryOverlayManager({
			overlay: this.sliderOverlay,
			images: this.images
		});
	}

	/**
	 * Initialize
	 * 
	 */
	getElements() {
		this.navigation = this.gallery.getElementsByClassName('solutions__content__gallery__navi')[0];
		this.navigationItems = this.navigation.getElementsByTagName('li');
		this.slidesContainer = this.gallery.getElementsByClassName('solutions__content__gallery__img')[0];
		this.slides = this.slidesContainer.getElementsByTagName('li');
		this.sliderCover = this.gallery.getElementsByClassName('solutions__content__gallery__cover')[0];
		this.sliderSvg = this.sliderCover.querySelector('svg');
		this.sliderCoverSVGCache = this.sliderCover.getElementsByClassName('polygon-cache')[0];
		this.sliderCoverEye = this.sliderCover.querySelectorAll('svg')[1];
		this.sliderOverlay = document.getElementsByClassName('overlay__gallery')[0];
	}

	/**
	 * Add data to the navigation
	 * 
	 */
	setNavigation() {
		for(let x = 0; x < this.navigationItems.length; x++){
			this.navigationItems[x].setAttribute('data-slide', x);
		}

		this.navigationItems[this.currentSlide].classList.add("active");
	}

	/**
	 * Listen to Clicks on the navigation
	 * 
	 */
	setNavListener() {
		this.navigation.addEventListener(this.touchEvent, this.onNavClick);
	}

	removeNavListener() {
		this.navigation.removeEventListener(this.touchEvent,  this.onNavClick);
	}

	setSliderListeners() {
		this.sliderCover.addEventListener(this.touchEvent, this.onSliderClick);
	}

	removeSliderListeners() {
		this.sliderCover.removeEventListener(this.touchEvent, this.onSliderClick);
	}

	setHoverListeners() {
		if(!window.isMobile) {
			this.gallery.addEventListener('mouseover', this.onSliderOver);
			this.gallery.addEventListener('mouseleave', this.onSliderOut);
		}
	}

	removeHoverListeners() {
		if(!window.isMobile) {
			this.gallery.removeEventListener('mouseover', this.onSliderOver);
			this.gallery.removeEventListener('mouseleave', this.onSliderOut);
		}
	}

	setListeners() {
		this.setNavListener();
		this.setHoverListeners();
		this.setSliderListeners();
		//window.App.emitter.on('blockResize', this.onResize);
	}

	removeListeners() {
		this.removeNavListener();
		this.removeHoverListeners();
		this.removeSliderListeners();
		//window.App.emitter.off('blockResize', this.onResize);
	}

	/**
	 * Removes listeners and overlay properties when other slide is shown
	 */
	kill() {
		this.removeListeners();
		this.overlay.remove();
		this.overlay = null;
	}


	/**
	 * Handle navigation clicks
	 * 
	 */
	handleNavClick(target){
		if(this.slidingInProgress) return;

		this.goToslide(target.getAttribute('data-slide') * 1);

		for(let x = 0; x < this.navigationItems.length; x++){
			this.navigationItems[x].classList.remove("active");
		}

		target.classList.add("active");
	}


	/**
	 * Go to a chosen slide
	 * 
	 */
	goToslide(targetSlide){
		if(this.currentSlide === targetSlide) return;

		let slideDifference = this.currentSlide - targetSlide;

		if(slideDifference < 0 && targetSlide < this.slides.length){
			// Remove slider listeners
			this.removeNavListener();
			this.removeSliderListeners();
			this.slideNext(targetSlide);
		} else if(targetSlide >= 0) {
			// Remove slider listeners
			this.removeNavListener();
			this.removeSliderListeners();
			this.slidePrev(targetSlide);
		}
	}

	/**
	 * Go to next Slide x times
	 * 
	 */
	slideNext(slide){
		let oldSlide = this.currentSlide;
		let newSlide = slide;
		this.slidingInProgress = true;


		TweenMax.set(this.slides[newSlide], {x:'100%', zIndex:20});
		TweenMax.to(this.slides[oldSlide], .5, {x:'-20%'});
		TweenMax.to(this.slides[newSlide], .5, {ease: Power2.easeOut, x:"0%", onComplete: () => {
			this.slidingInProgress = false;
            TweenMax.set(this.slides[oldSlide], {zIndex: 0 });
            TweenMax.set(this.slides[newSlide], {zIndex: 10 });
            // Put listeners back in
            this.setNavListener();
            this.setSliderListeners();
			this.currentSlide = newSlide;


        }});
	}

	/**
	 * Go to previous Slide x times
	 * 
	 */
	slidePrev(slide){
		let oldSlide = this.currentSlide;
		let newSlide = slide;
		this.slidingInProgress = true;

		TweenMax.set(this.slides[newSlide], {x:'-100%', zIndex:20});
		TweenMax.to(this.slides[oldSlide], .5, {x:'20%'});
		TweenMax.to(this.slides[newSlide], .5, {ease: Power2.easeOut, x:'0%', onComplete: () => {
			this.slidingInProgress = false;
	        TweenMax.set(this.slides[oldSlide], {zIndex: 0 });
	        TweenMax.set(this.slides[newSlide], {zIndex: 10 });
			// Put listeners back in
			this.setNavListener();
            this.setSliderListeners();
			this.currentSlide = newSlide;
	    }});
	}


	/**
	 *	Set Images for slides
	 */
	setImages() {
		for(let x = 0; x < this.slides.length; x++) {
			this.loadImages(this.slides[x], this.slides[x].getAttribute('data-bgimg'));
		}
	}
	
	/**
	 *	Load Images
	 * 
	 */
	loadImages(slide, src) {
		this.images.push(src);
		this.imgSrc = src;
		let Img = new Image();
		Img.onload = () => {
			this.onImgLoaded(slide, src);
		}
		Img.src = this.imgSrc;
	}

	/**
	 * On image load
	 * 
	 */
	onImgLoaded(slide, src) {
		//  TODO: Img.src is empty
		slide.style.backgroundImage = 'url(' + src + ')';
	}

	//// Slide interactions ///////////////////
	onSliderOver(e) {
		this.enterDirection = e.movementX > 0 ? 1 : 0;
		// 
		if(!this.coverShown) {
			this.showCover();
			this.coverShown = true;
		}
	}

	onSliderOut(e) {
		// 
		this.leaveDirection = e.movementX > 0 ? 1 : 0;
		if(this.coverShown) {
			this.hideCover();
			this.coverShown = false;
		}	
	}

	onSliderClick(e) {
		this.overlay.show(this.currentSlide);
	}

	onNavClick(e) {
		if(e.target && e.target.nodeName == "LI"){
			this.handleNavClick(e.target);
		}
	}

	showCover() {
		this.showing = true;
		this.sliderCover.classList.remove('no-display');
		this.leftEnter();	
		
		// Enter from left to right
		// if(this.enterDirection > 0) {
		// 	
		// 	// if(this.hiding && this.leaveDirection > 0) {
		// 	// 	this.rightEnter();
		// 	// }
		// 	// else if(this.hiding && this.leaveDirection === 0) {
		// 	// 	this.leftEnter();
		// 	// }
		// 	// else {
		// 	// 	this.leftEnter();	
		// 	// }
		// 	this.leftEnter();	
		// }
		// else {
		// 	this.rightEnter();
		// }
		
	}

	hideCover() {
		this.hiding = true;
		this.leftLeave();
	}

	/**
	 * Left enter
	 */
	leftEnter() {
		// Cache
		let x2 = Math.tan(this.polyPoints.angle * Math.PI/180) * 520 + 520;
		let y3 = 500 / Math.tan(this.polyPoints.angle * Math.PI/180) + 500;
		
		TweenMax.to(this.polyPoints, .4, {
			x1: 0, 		y1: 0,
			x2: x2, 	y2: 0,
			x3: 0, 	    y3: y3,
			x4: 0, 		y4: 0,

			onUpdate: () => {
				this.setPolyPoints();
				
			}
		});

		// Eye
		TweenMax.to(this.sliderCoverEye, .4, 
			{ x: "0%", y: "0%", autoAlpha: 1, onComplete: () => { this.showing = false; }}
		);
	}

	/**
	 * Right enter
	 */
	//  rightEnter() {
	//  	let x2 = -1 * (Math.tan(this.polyPoints.angle * Math.PI/180) * 520 + 520);
	// 	let y3 = -1 * (520 / Math.tan(this.polyPoints.angle * Math.PI/180) + 520);
		
	// 	TweenMax.fromTo(this.polyPoints, .4, {
	// 		x1: 520, 	y1: 500,
	// 		x2: 520,	y2: 500,
	// 		x3: 520, 	y3: 500,
	// 		x4: 520, 	y4: 500
	// 	},
	// 	{
	// 		x1: 520, 		y1: 500,
	// 		x2: x2, 		y2: 500,
	// 		x3: 520, 	    y3: y3,
	// 		x4: 520, 		y4: 500,

	// 		onUpdate: () => {
	// 			this.setPolyPoints();
	// 		}
	// 	});
	// }



	leftLeave() {
		TweenMax.to(this.polyPoints, .4, {
			x1: 0, 		y1: 0,
			x2: 0, 		y2: 0,
			x3: 0, 		y3: 0,
			x4: 0, 		y4: 0,

			onUpdate: () => {
				this.setPolyPoints();
			},

			onComplete: () => {
				this.sliderCover.classList.add('no-display');
			}
		});

		// Eye
		TweenMax.to(this.sliderCoverEye, .4, 
			{ x: "-10%", y: "-10%", autoAlpha: 0, onComplete: () => { this.hiding = false; }}
		);
	}
	
	setPolyPoints() {
		this.sliderCoverSVGCache.setAttribute("points", this.polyPoints.x1 + "," + this.polyPoints.y1 + " " + this.polyPoints.x2 + "," + this.polyPoints.y2 + " " + this.polyPoints.x3 + "," + this.polyPoints.y3 + " " + this.polyPoints.x4 + "," + this.polyPoints.y4);
	}

	calculateCoords() {
		let width = this.gallery.offsetWidth;
		let height = this.gallery.offsetHeight;
		this.sliderSvg.setAttribute('viewBox', '0 0 ' + width + ' ' + height);
		this.sliderSvg.setAttribute('width', width);
		this.sliderSvg.setAttribute('height', height);
	}



	onResize() {
		this.calculateCoords();
	}




	
}