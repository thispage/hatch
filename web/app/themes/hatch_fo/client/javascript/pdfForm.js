import {PageManager} from './pageManager';
import {OverlayManager} from './overlayManager';

export default class PdfForm {
    constructor() {
        this.init();
    }

    /**
     * Initialize
     */
    init() {
        this.bindFunctions();
        this.setEmitterListeners();
        this.getElements();
        this.setListeners();

        this.overlayManager = new OverlayManager({
            overlay: this.pdfOverlay
        })
    }

    getElements() {
        this.section = document.querySelector('.integration__cards');
        this.dlButtons = this.section.getElementsByClassName('section__smallbtn');
        this.pdfOverlay = document.getElementsByClassName('overlay__download')[0];
        // this.pdfOverlayInput = document.getElementById('form-pdfCaseName');
        this.pdfOverlayInput = document.getElementById('pdfCaseName');
        this.formAction = this.pdfOverlay.querySelector('form').getAttribute('action');
        this.formSubmit = document.getElementById('form-pdfSubmitBtn'); // Changed from pdfSubmitBtn

        this.formId = this.pdfOverlay.querySelector('input[name="_wpcf7"]').getAttribute('value');
        this.formVersion = this.pdfOverlay.querySelector('input[name="_wpcf7_version"]').getAttribute('value');
        this.formLang = this.pdfOverlay.querySelector('input[name="_wpcf7_locale"]').getAttribute('value');
        this.formTag = this.pdfOverlay.querySelector('input[name="_wpcf7_unit_tag"]').getAttribute('value');
        // this.formNonce = this.pdfOverlay.querySelector('input[name="_wpnonce"]').getAttribute('value');


        this.titleInput = this.pdfOverlayInput;
        this.nameInput = document.querySelector('#form-pdfContactName');
        this.mailInput = document.querySelector('#form-pdfContactEmail');

        this.realForm = document.getElementById('wpcf7-f212-o2');
    }


    bindFunctions() {
        this.onBtnClick = this.onBtnClick.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onOverlayClose = this.onOverlayClose.bind(this);
        this.onreadyStateChange = this.onreadyStateChange.bind(this);
    }

    setEmitterListeners() {
        window.App.emitter.on('overlayClose', this.onOverlayClose);
    }

    setListeners() {
        for(let i = 0; i < this.dlButtons.length; i++) {
            this.dlButtons[i].addEventListener('click', this.onBtnClick);
        }
        // this.formSubmit.addEventListener('click', this.onFormSubmit);
        this.realForm.addEventListener('wpcf7submit', e => e.detail.status === 'mail_sent' && window.App.emitter.emit('hideOverlay'));
    }

    onBtnClick(e) {
        // Populates overlay input
        let title = e.currentTarget.getAttribute('data-title');
        if(title) {
            this.pdfOverlayInput.setAttribute('value', title);
        }
        // Show overlay
        this.overlayManager.show();
        //this.pdfOverlay.classList.remove('no-display');
        //this.overlayShown = true;
    }

    onFormSubmit(e) {
        e.preventDefault();
        this.checkInputs();
        //this.overlayManager.hide();
    }

    checkInputs() {
        this.titleVal = this.titleInput.value;
        this.nameVal = this.nameInput.value;
        this.mailVal = this.mailInput.value;

        if (this.nameVal === '') {
            this.nameInput.classList.add('error');
            this.nameValid = false;
        }
        else {
            this.nameInput.classList.remove('error');
            this.nameValid = true;
        }

        if (this.mailVal === '') {
            this.mailInput.classList.add('error');
            this.mailValid = false;
        }
        else {
            this.mailInput.classList.remove('error');
            this.mailValid = true;
        }

        if (this.titleVal === '') {
            this.titleValid = false;
        }
        else {
            this.titleValid = true;
        }

        if (this.nameValid && this.mailValid && this.titleValid) {
            this.sendForm();
        }
    }

    sendForm() {
        if (window.XMLHttpRequest) { // Mozilla, Safari, ...
            this.httpRequest = new XMLHttpRequest();
        } else if (window.ActiveXObject) { // IE
            try {
                this.httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e) {
                try {
                  this.httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {}
            }
        }

        this.httpRequest.onreadystatechange = this.onreadyStateChange;
        this.httpRequest.open('POST', this.formAction);
        this.httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        this.httpRequest.send('_wpcf7=' + encodeURIComponent(this.formId)
            + '&_wpcf7_version='+ encodeURIComponent(this.formVersion)
            + '&_wpcf7_locale='+ encodeURIComponent(this.formLang)
            + '&_wpnonce='+ encodeURIComponent(this.formNonce)
            + '&_wpcf7_unit_tag='+ encodeURIComponent(this.formTag)
            + '&pdfCaseName='+ encodeURIComponent(this.titleVal)
            + '&pdfContactName='+ encodeURIComponent(this.nameVal)
            + '&pdfContactEmail='+ encodeURIComponent(this.mailVal)
            + '&_wpcf7_is_ajax_call='+ encodeURIComponent(1));
    }

    clearInputs() {
        // this.nameInput.classList.remove('error');
        // this.mailInput.classList.remove('error');
        // this.nameInput.value = '';
        // this.mailInput.value = '';
        // this.formSubmit.value = "SEND";
    }

    showOkMessage() {
        this.formSubmit.value = "Thank you !";
        TweenMax.delayedCall(.5, () => {
            window.App.emitter.emit('hideOverlay');
        })
    }

    onreadyStateChange(e) {
        try {
            if (this.httpRequest.readyState === 4) {
                if (this.httpRequest.status === 200) {
                    this.showOkMessage();
                } else {

                }
            }
        }
        catch(e) {

        }
    }

    onOverlayClose() {
        this.clearInputs();
    }
}
