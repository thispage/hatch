import ScrollToPlugin from '../vendor/gsap/src/uncompressed/plugins/ScrollToPlugin';

import {PageManager} from './pageManager';

export class News extends PageManager {
	constructor() {
		super();
	}

	/**
	 * Initialize
	 */
	init() {
		super.init();
		this.bindFunctions();
		this.getElements();
		this.setListeners();
	}

	bindFunctions() {
		this.loadMoreNews = this.loadMoreNews.bind(this);
	}

	/**
	 * Get Elements
	 */
	getElements() {
		this.wrapper = document.getElementsByClassName('page-wrapper')[0];

		this.loadMoreNewsBtn = document.getElementById('load-more-news');

		if (this.loadMoreNewsBtn) {
			this.visibleNewsHolder = document.getElementsByClassName('visible-news-rows')[0];
			this.hiddenNewsHolder = document.getElementsByClassName('hidden-news-rows')[0];
		}
	}

	/**
	 * Set Listeneres
	 */
	setListeners() {
		if (this.loadMoreNewsBtn) {
			this.loadMoreNewsBtn.addEventListener('click', this.loadMoreNews);
		}
	}

	/**
	 * Load more news
	 */
	loadMoreNews(ev) {
		var cards = document.querySelectorAll('.hidden-news-rows .news__content__card');


		for (var i = 0; i < cards.length; i++) {
			this.visibleNewsHolder.appendChild(cards[i]);
			if (i == 2) {
				break;
			}
		}

		let $scrollEl = this.loadMoreNewsBtn;

		if (this.hiddenNewsHolder.childElementCount === 0) {
			this.loadMoreNewsBtn.style.display = 'none';
			$scrollEl = this.visibleNewsHolder;
		}

		this.scrollToNew($scrollEl);
	}


	scrollToNew(el) {
		TweenMax.to(window, 0.7, {
				scrollTo: {
					y: el.offsetTop - (window.innerHeight - el.offsetHeight) + 20,
					autoKill : true
				},
				ease:Power3.easeInOut
			}
		);
	}
}
