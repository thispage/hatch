import {PageManager} from './pageManager';
import {ContactSectionAnim} from './animations/contactSectionAnim';

export class Contact extends PageManager {
	constructor() {
		super();
	}

	/**
	 * Initialize
	 */
	init() {
		super.init();
		
		// Contact animations			
		var contactAnim = new ContactSectionAnim({});
		contactAnim.trigger({});
	}
}