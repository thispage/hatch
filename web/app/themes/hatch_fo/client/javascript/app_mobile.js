import ee from 'event-emitter';
import ClassListPolyfill from 'classlist-polyfill';

import {Index} from './mobile/index';
import {Pricing} from './mobile/page-pricing';
import {News} from './page-news';
import {Disclaimer} from './page-disclaimer';
import {Privacy} from './page-privacy';
import {Single} from './page-single';
import {CookiePolicy} from './cookiePolicy';


import {ArrowBtnsManager} from './arrowBtnsManger';

export class App {
	constructor() {
		window.appStates = {};
	}

	init() {

		this.initEmitter();
		this.setArrowButtons();
		this.initPage();
        // this.cookiePolicy = new CookiePolicy().init();
	}

	initEmitter() {
		this.emitter = ee({});
	}

	initPage() {
		switch(window.currentPage) {
			case 'index':
				this.page = new Index();
				break;
			case 'page-packages':
				this.page = new Pricing();
				break;
			case 'page-news':
				this.page = new News();
				break;
			case 'page-blog':
				this.page = new News();
				break;
			case 'page-disclaimer':
				this.page = new Disclaimer();
				break;
			case 'page-privacy':
				this.page = new Privacy();
				break;
			case 'single-post':
				this.page = new Single();
				break;
			default:
				this.page = new Index();
				break;
		}
		this.page.init();
	}
	setArrowButtons() {
		// Arrow buttons
		this.arrowBtns = document.getElementsByClassName('section__arrow');

		this.arrowBtnsManger = new ArrowBtnsManager({
			arrows: this.arrowBtns
		});
	}
}

window.onload = () => {
	window.App = new App();
	window.App.init();
};


window.initMap = () => {
	window.gMapScript = true;
};
