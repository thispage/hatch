import TweenMax from '../vendor/gsap/src/uncompressed/TweenMax';
import {MenuManager} from './menuManager';

export class PageManager {
	constructor() {
		this.touchEvent = window.isMobile ? 'touchend' : 'click';
		this.MenuManager = new MenuManager({
			menu: document.getElementById('navigation')
		})
	}

	init() {
		this.MenuManager.init();
	}
}