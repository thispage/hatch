/* app/views/view */
import TweenMax from '../vendor/gsap/src/uncompressed/TweenMax';

import bindAll from './lib/bindAll';
import toArray from './lib/toArray';


export class Video {

    constructor(options) {
        this.init();
    }

    bindFunctions() {
        bindAll(this, ['onYouTubeIframeAPIReady', 'initialize', 'onPlayerReady'])
    }

    cacheElements() {
      var player;
      this.$videoIdHolder = window.document.querySelector(".videoId");
    }

    eventListeners() {}


    init() {

        this.cacheElements();
        this.eventListeners();
        this.bindFunctions();
        if (this.$videoIdHolder.innerHTML !== '') this.onYouTubeIframeAPIReady();
    }


    onYouTubeIframeAPIReady() {
        this.player;
        var videoId = this.$videoIdHolder.innerHTML
        this.player = new YT.Player('myVideo', {
            width: 600,
            height: 400,
            videoId: videoId,

            playerVars: {
                color: 'white',
                playlist: videoId,
                loop: '1'
            },
            events: {
                // onReady: this.initialize,
                'onReady': this.onPlayerReady,
            }
        });
    }

    initialize(){

        // Update the controls on load
        updateTimerDisplay();
        updateProgressBar();

        // Clear any old interval.
        clearInterval(time_update_interval);

        // Start interval to update elapsed time display and
        // the elapsed part of the progress bar every second.
        time_update_interval = setInterval(function () {
            updateTimerDisplay();
            updateProgressBar();
        }, 1000)

    }


    onPlayerReady(event) {
        event.target.mute();
        event.target.playVideo();

        let $video = document.querySelector('.video-foreground');
        $video.classList.add('ready');

        let $videoBackground = document.querySelector('.video-background');
        $videoBackground.style.opacity = 1;
    }
}
