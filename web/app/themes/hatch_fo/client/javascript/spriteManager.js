export class SpriteManager {
	constructor(params) {
		this.animating = true;
		this.spriteInfoObj = {
			frameDuration: 0,
			frameCount: 0,
			coords: []
		};

		this.spriteContainer = params.spriteContainer;
	}

	init() {

	}

	animateSprite(cb) {
		this.animating = true;

		let x = 0;
		let y = 0;
		let counter = 0;


		this.tween = TweenMax.to(this.spriteInfoObj, this.spriteInfoObj.frameDuration * 2, {useFrames: true, ease: Linear.easeNone, frameCount: this.spriteInfoObj.frameDuration,
			onUpdate: () => {
				counter = Math.floor(this.spriteInfoObj.frameCount);
				if(this.spriteInfoObj.coords[counter]) {
					x = - this.spriteInfoObj.coords[counter][0];
					y = - this.spriteInfoObj.coords[counter][1];

					TweenMax.set(this.spriteContainer, {backgroundPosition: x + 'px ' + y + 'px'});
				}
				else {
					x = - this.spriteInfoObj.coords[this.spriteInfoObj.coords.length][0];
					y = - this.spriteInfoObj.coords[this.spriteInfoObj.coords.length][1];
					TweenMax.set(this.spriteContainer, {backgroundPosition: x + 'px ' + y + 'px'});
				}
			},
			onComplete: ()=> {
				this.animating = false;
				// Hide the logo when scrolling to the first section
				if(typeof cb == 'function') {
					cb();
				}
			}
		});
	}

	pause() {
		if(this.tween) {
			this.tween.pause();
		}
	}

	resume() {
		if(this.tween) {
			this.tween.play();
		}
	}
}
