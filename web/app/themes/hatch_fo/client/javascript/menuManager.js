export class MenuManager {
	constructor(params) {
		
		this.menu = params.menu;
		this.menuButton = this.menu.getElementsByClassName('menu-btn')[0];
		this.menuBar = this.menu.getElementsByClassName('navigation__bar')[0];
		this.navItems = this.menu.getElementsByClassName('navigation__menu__item__link--internal');
		this.logo = this.menu.getElementsByClassName('navigation__menu__item--logo')[0];
		this.leftNavItems = this.menu.querySelectorAll('.navigation__menu--left li');
		this.leftNavItemsNoLogo = Array.prototype.slice.call(this.leftNavItems);
		this.leftNavItemsNoLogo.shift();
		this.rightNavItems = this.menu.querySelectorAll('.navigation__menu--right a');
		this.requestDemoBtn = this.menu.querySelectorAll('.navigation__menu__item__link--requestDemoBtn') || {};
		this.loginBtn = this.menu.querySelectorAll('.navigation__menu__item__link--loginBtn') || {};
		
		this.sections = [
			'loader',
			'intro',
			'solutions',
			'customers',
			'about',
			'contact'
		];
		this.bindFunctions();
		
	}

	setEmitterEvents() {
		window.App.emitter.on('initMenu', this.onMenuInteractionStart);
		window.App.emitter.on('contrastCheck', this.onContrastCheck);
		window.App.emitter.on('resetMenu', this.onResetMenu);
	}

	bindFunctions(){
		this.onNavItemClick = this.onNavItemClick.bind(this);
		this.onNavItemTouch = this.onNavItemTouch.bind(this);
		this.onNavItemTouchStart = this.onNavItemTouchStart.bind(this);
		this.onBodyTouch = this.onBodyTouch.bind(this);
		this.onMenuInteractionStart = this.onMenuInteractionStart.bind(this);
		this.onContrastCheck = this.onContrastCheck.bind(this);

		this.onMouseLeave = this.onMouseLeave.bind(this);

		this.onResetMenu = this.onResetMenu.bind(this);
		this.onLoginClick = this.onLoginClick.bind(this);
		
		this.onMenuBtnClick = this.onMenuBtnClick.bind(this);
		this.onLoginMouseOut = this.onLoginMouseOut.bind(this);
		this.onLoginMouseIn = this.onLoginMouseIn.bind(this);
		
		this.onRequestDemoBtnMouseClick = this.onRequestDemoBtnMouseClick.bind(this);
		this.onRequestDemoBtnMouseOut = this.onRequestDemoBtnMouseOut.bind(this);
		this.onRequestDemoBtnMouseIn = this.onRequestDemoBtnMouseIn.bind(this);
	}

	/**
	 * Class init
	 */
	init() {
		this.setEmitterEvents();
		
		//if(window.isMobile && !window.isTablet) {
			this.setListeners();
		//}
		
		this.showMenu();
	}

	/**
	 * Initialises the menu interactions
	 */
	onMenuInteractionStart() {
		this.setListeners();
	}

	/**
	 * Sets the menu contrast mode on white sections
	 */
	onContrastCheck(sectionNumber) {
		if(sectionNumber === 3) {
			this.menu.classList.add('contrast');
			TweenMax.to(this.rightNavItems, .5, {color: "#4B4B4B", borderColor: "#4B4B4B", onComplete: () => {
				TweenMax.set(this.rightNavItems, {clearProps: "all"});
			}});
			this.contrastOn = true;
		}
		else {
			if(this.contrastOn) {
				TweenMax.to(this.rightNavItems, .5, {color: "#FFFFFF", borderColor: "#FFFFFF", onComplete: () => {
					this.menu.classList.remove('contrast'); 
					TweenMax.set(this.rightNavItems, {clearProps: "all"});
				}});
				this.contrastOn = false;
			}
		}
	}

	setMenuBtnListener() {
		
		// if(window.isMobile) {
		// 	this.menuButton.addEventListener('touchend', this.onMenuBtnClick);
		// }
		// else {
			this.menuButton.addEventListener('click', this.onMenuBtnClick); 
		// }
	}

	removeMenuBtnListener() {
		if(window.isMobile) {
			this.menuButton.removeEventListener('touchend', this.onMenuBtnClick);
		}
		else {
			this.menuButton.removeEventListener('click', this.onMenuBtnClick);  
		}
	}

	setListeners() {

		let i = 0;
		for(i; i < this.navItems.length; i++) {
			// if(window.isMobile && !window.isTablet) {
			// 	this.navItems[i].addEventListener('touchstart', this.onNavItemTouchStart);
			// 	this.navItems[i].addEventListener('touchend', this.onNavItemTouch);
			// 	document.body.addEventListener('touchend', this.onBodyTouch)
			// }
			// else {
				this.navItems[i].addEventListener('click', this.onNavItemClick);
			// }
		}
		
		this.setMenuBtnListener();
	}

	removeListeners() {
		let i = 0;
		for(i; i < this.navItems.length; i++) {
			if(window.isMobile && !window.isTablet) {
				this.navItems[i].addEventListener('touchstart', this.onNavItemTouchStart);
				this.navItems[i].removeEventListener('touchend', this.onNavItemTouch);
				document.body.removeEventListener('touchend', this.onBodyTouch)
			}
			else {
				this.navItems[i].removeEventListener('click', this.onNavItemClick);
			}
		}

		if(!window.isMobile) {      
			this.menu.removeEventListener('mouseenter', this.onMouseEnter);
			this.menu.removeEventListener('mouseleave', this.onMouseLeave);
//			this.loginBtn.removeEventListener('click', this.onLoginClick);
			this.requestDemoBtn.removeEventListener('click', this.onRequestDemoBtnMouseClick);
		}

		this.removeMenuBtnListener();
	}

	onNavItemClick(e) {
		let sectionNumber = e.target.getAttribute('data-section');  
		if(sectionNumber === 'news' || sectionNumber === 'packages') { // LOL
			return;
		}

		if(this.menu.classList.contains('open')) {
			this.menu.classList.remove('open');
			this.enableScroll();
		}
		else if(window.innerWidth < 1024){
			this.menu.classList.add('open');    
			this.disableScroll();
		}
		// Notify scroller that we wanna scroll
		window.App.emitter.emit('scrollto', sectionNumber);
	}

	onNavItemTouchStart(e) {
		
		if(this.currentItemSelected) {
			this.currentItemSelected.classList.remove('selected');  
		}

		this.currentItemSelected = e.target;
		this.currentItemSelected.classList.add('selected');

	}

	onNavItemTouch(e) {
		let sectionNumber = e.target.getAttribute('data-section');  
		if(sectionNumber === 'news' || sectionNumber === 'packages') { // LOL
			return;
		}
		this.onMenuBtnClick(e);
	}

	onBodyTouch(e) {

		if(e.target.id === 'navigation' && this.menu.classList.contains('open')) {
			this.onMenuBtnClick(e);
		}

	}

	onMenuBtnClick(e) {
		if(this.scrolling) {
			return;
		}
		let href = e.target.getAttribute('href');

		if(href) {
			if(href === '/') {
				window.location = href; 
			}
			else {
				window.location.hash = href;
			}
		}
		if(this.menu.classList.contains('open')) {
			this.menu.classList.remove('open');
			this.enableScroll();
		}
		else {
			this.menu.classList.add('open');    
			this.disableScroll();
		}
	}

	onLoginClick(e) {
		// ga('send', {
		// 	hitType: 'event',
		// 	eventCategory: 'Clickout',
		// 	eventAction: 'login',
		// 	eventLabel: 'User clicked login'
		// });
	}
	
	onRequestDemoBtnMouseClick(e) {
		// ga('send', {
		// 	hitType: 'event',
		// 	eventCategory: 'Clickout',
		// 	eventAction: 'login',
		// 	eventLabel: 'User clicked login'
		// });
	}

	onMouseEnter() {
		if(window.innerWidth > 1024) {
			this.showMenu();
		}
	}

	onMouseLeave() {
		if(window.innerWidth > 1024) {
//			this.hideMenu();
		}
	}

	onLoginMouseIn() {
		TweenMax.to(this.loginBtn, .1, {borderColor: '#6edcd4', backgroundColor: '#6edcd4', color: '#FFFFFF', overwrite: 1});
	}
	
	onRequestDemoBtnMouseIn() {
		TweenMax.to(this.requestDemoBtn, .1, {borderColor: '#6edcd4', backgroundColor: '#6edcd4', color: '#FFFFFF', overwrite: 1});
	}

	onLoginMouseOut() {
		TweenMax.to(this.loginBtn, .1, {borderColor: '#4b4b4b', backgroundColor: 'none', color: '#4b4b4b', overwrite: 0});
	}
	
	onRequestDemoBtnMouseOut() {
		TweenMax.to(this.requestDemoBtn, .1, {borderColor: '#4b4b4b', backgroundColor: 'none', color: '#4b4b4b', overwrite: 0});
	}

	showMenu() {
		// if(this.menuShown) {
		// 	return;
		// }

		this.menuShown = true;
		this.logo.classList.add('active');

		if(this.menuTl) {
			this.menuTl.kill();
			this.menuTl.clear();
		}

		this.menuTl = new TimelineLite({onComplete: () => {
			this.menu.classList.add('active');
			TweenLite.set([this.leftNavItemsNoLogo, this.navItems, this.loginBtn], {clearProps: 'all'});
		}});
		
		this.menuTl.timeScale(.9);
		//this.menuTl.timeScale(.3);

		this.menuTl.to(this.menuBar, .2, {y: "0%", ease: Power2.easeOut}, 'menu');
		this.menuTl.to(this.leftNavItemsNoLogo, .2, {y: 0, ease: Power2.easeOut}, 'menu');
		// //this.menuTl.to(this.leftNavItems, .05, {autoAlpha: 1}, 'menu');
		this.menuTl.to(this.rightNavItems, .1, {color: '#4b4b4b', ease: Power2.easeOut}, 'menu');
		this.menuTl.to(this.loginBtn, .1, {borderColor: "#4B4B4B", ease: Power2.easeOut}, 'menu');
	}

	hideMenu() {
		
		this.logo.classList.remove('active');

		if(this.menuTl) {
			this.menuTl.kill();
			this.menuTl.clear();
		}

		this.menuTl = new TimelineLite({onComplete: () => {
			this.menuShown = false;
			this.menu.classList.remove('active');
			TweenLite.set([this.leftNavItemsNoLogo, this.navItems, this.loginBtn], {clearProps: 'all'});
		}});
		this.menuTl.timeScale(.9);
		
		this.menuTl.to(this.menuBar, .1, {y:"-100%", ease: Power2.easeOut}, 'menu');
		this.menuTl.to(this.leftNavItemsNoLogo, .2, {y: -130, ease: Power2.easeOut}, 'menu');
		// //this.menuTl.to(this.leftNavItems, .05, {autoAlpha: 0}, 'menu');

		if(!this.contrastOn) {
			this.menuTl.to(this.rightNavItems, .1, {color: '#FFFFFF', ease: Power2.easeOut}, 'menu');
			this.menuTl.to(this.loginBtn, .1, {borderColor: "#FFFFFF", ease: Power2.easeOut}, 'menu');
			
		}
	}

	onHideMenu() {
		this.scrolling = true;
//		this.hideMenu();
	}

	onResetMenu() {
		this.scrolling = false;
		this.setListeners();
	}

	/**
	 * Disable Scroll
	 */
	disableScroll() {
		document.addEventListener('mousewheel', this.preventEventDefault);
		document.addEventListener('touchmove', this.preventEventDefault);
	}

	/**
	 * Enable Scroll
	 */
	enableScroll() {
		document.removeEventListener('mousewheel', this.preventEventDefault, false);
		document.removeEventListener('touchmove', this.preventEventDefault);
	}

	/**
	 * Prevent Event's dafault
	 */
	preventEventDefault(e) {
		e.preventDefault();
	}
} 