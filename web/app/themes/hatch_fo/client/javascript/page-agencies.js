import {PageManager} from './pageManager';
import {Slider} from './slider';
import {Video} from './video';
import PdfForm from './pdfForm';

export class Agencies extends PageManager {
	constructor() {
		super();
	}

	/**
	 * Initialize
	 */
	init() {
		super.init();
		this.pdfForm = new PdfForm();
	}
}