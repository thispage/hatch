import ee from 'event-emitter';
import ClassListPolyfill from 'classlist-polyfill';

import bindAll from "./lib/bindAll";
import toArray from "./lib/toArray";
import inViewport from "./lib/inViewport";

import {Index} from './index';
import {AboutUs} from './page-about-us';
import {Pricing} from './page-pricing';
import {News} from './page-news';
import {Single} from './page-single';
import {Partners} from './page-partners';
import {Faq} from './page-faq';
import {RequestDemo} from './page-request-demo';
import {Contact} from './page-contact';
import {Disclaimer} from './page-disclaimer';
import {Privacy} from './page-privacy';
import {Brands} from './page-brands';
import {Retailers} from './page-retailers';
import {Agencies} from './page-agencies';
import {Slider} from './slider';
import {Video} from './video';
import {CookiePolicy} from './cookiePolicy';

import {ArrowBtnsManager} from './arrowBtnsManger';
import {ArrowSpriteManager} from './arrowSpriteManager';
import {LogoSpriteManager} from './logoSpriteManager';

import {TitleAnimation} from './animations/introsection/TitleAnimation';
import {LineAnimations} from './animations/introsection/LineAnimations';
import {LineAnimationsVertical} from './animations/introsection/LineAnimationsVertical';

export class App {
    constructor() {
        window.appStates = {};

        // Default properties
        this.$sections = null;
        this._totalSections = 0;
    }

    init() {
        this.bindFunctions();
        this.initEmitter();
        this.setArrowButtons();

        if (!window.isMobile && !window.isTablet && window.currentPage === 'index' && !localStorage.getItem('iPld')) {
            this.setLoadingStyle();
            this.setLoader();
        } else if (!window.isMobile && !window.isTablet) {
            // this.cookiePolicy = new CookiePolicy().init();
            this.getSections();
            this.setPageNormal();
        } else {
            // this.cookiePolicy = new CookiePolicy().init();
            this.setPageNormal();
        }
        this.initPage();
    }

    bindFunctions() {
        bindAll(this, ['getSections', 'finishLoader', 'onScroll'])
    }


    initEmitter() {
        this.emitter = ee({});
    }

    initPage() {
        switch(window.currentPage) {
            case 'index':
                this.page = new Index();
                this.setLineAnimations();
                break;
            case 'page-about-us':
                this.video = new Video();
                this.page = new AboutUs();
                break;
            case 'page-where-to-buy-solutions':
                this.page = new Pricing();
                break;
            case 'page-news':
                this.page = new News();
                break;
            case 'page-blog':
                this.page = new News();
                break;
            case 'single-post':
                this.page = new Single();
                break;
            case 'page-partners':
                this.page = new Partners();
                break;
            case 'page-contact-us':
                this.page = new Contact();
                break;
            case 'page-request-demo':
                this.page = new RequestDemo();
                break;
            case 'page-disclaimer':
                this.page = new Disclaimer();
                break;
            case 'page-privacy':
                this.page = new Privacy();
                break;
            case 'page-faqs':
                this.page = new Faq();
                break;
            case 'page-brands':
                this.page = new Brands();
                this.video = new Video();
                this.setVertLineAnimations();
                break;
            case 'page-retailers':
                this.page = new Retailers();
                this.video = new Video();
                break;
            case 'page-agencies':
                this.page = new Agencies();
                this.slider = new Slider({
                    el: window.document.querySelector('.agencies-slideshow-mask'),
                    dots: true
                });
                this.video = new Video();


                break;
            default:
                return false;
                break;
        }

        this.page.init();
    }

    getSections() {
        this.titleAnimations = [];
        this.$sections = toArray(document.querySelectorAll('.section__header'));
        this._totalSections = this.$sections.length;

        this.$sections = this.$sections.filter((section) => {
            return !section.classList.contains('no-animate');
        });

        for (let i = 0; i < this.$sections.length; i++) {
            this.titleAnimations.push(new TitleAnimation({ pDelay: 0, pTargetHeader: this.$sections[i] }));
            if (inViewport(this.$sections[i])) {
                TweenMax.to(this.$sections[i], 0.5, {
                    autoAlpha: 1
                });
                this.titleAnimations[i].timeline.play();
            }
        }

        // Register scroll event
        window.addEventListener('scroll', this.onScroll);
    }
    // FOR INDEX PAGE
    setLineAnimations() {
        const lineAnimGraphic = document.querySelector('.section__content__animation');
        if (lineAnimGraphic) {
            this.introLineAnim = new LineAnimations(lineAnimGraphic, this.animStepsText);
            if (inViewport(lineAnimGraphic)) this.introLineAnim.animateIn();
            else {
                window.addEventListener('scroll', inView);
                const self = this;
                function inView() {
                    if (inViewport(lineAnimGraphic)) {
                        self.introLineAnim.animateIn();
                        window.removeEventListener('scroll', inView);
                    }
                }
            }
        }
    }
    //
    animStepsText() {
        const findText = document.querySelector('.intro__steps__text--find');
        const selectText = document.querySelector('.intro__steps__text--select');
        const completeText = document.querySelector('.intro__steps__text--complete');
        TweenMax.set([findText, selectText, completeText], {clearProps:'all'});
        if (findText && selectText && completeText) {
            const textsTl = new TimelineLite()
            textsTl.staggerFromTo([findText, completeText, selectText], 0.5, {
                y: -30,
                autoAlpha: 0
            }, {
                y: 0,
                autoAlpha: 1,
                ease: Back.easeOut
            }, 0.2, '+=1.2');
        }
    }

    //FOR BRANDS PAGE
    setVertLineAnimations() {
        const vertLineAnimGraphic = document.querySelector('.section__content.integration__steps');
        if (vertLineAnimGraphic) {
            this.vertIntroLineAnim = new LineAnimationsVertical(vertLineAnimGraphic, false, this.animVertStepsText);
            if (inViewport(vertLineAnimGraphic)) {
                this.vertIntroLineAnim.animateIn();
            } else {
                window.addEventListener('scroll', inView);
                const self = this;
                function inView() {
                    if (inViewport(vertLineAnimGraphic)) {
                        self.vertIntroLineAnim.animateIn();
                        window.removeEventListener('scroll', inView);
                    }
                }
            }
        }
    }
    //
    animVertStepsText() {
        const start = document.querySelector('.integration__steps__item--start');
        const bullets = toArray(document.querySelectorAll('.integration__steps__item--bullet'));
        const texts = toArray(document.querySelectorAll('.integration__steps__item--txt'));
        const end = document.querySelector('.integration__steps__item--end');
        const bltsTxtTl = new TimelineLite();
        bltsTxtTl.to(start, 0.5, {
            autoAlpha: 1
        });
        bltsTxtTl.staggerFromTo(bullets, 0.5, {
            y: -30,
            autoAlpha: 0
        }, {
            y: 0,
            autoAlpha: 1
        }, 0.2, '+=0.7');
        bltsTxtTl.staggerFromTo(texts, 0.5, {
            y: -30,
            autoAlpha: 0
        }, {
            y: 0,
            autoAlpha: 1
        }, 0.2, '-=.7');
        bltsTxtTl.to(end, 0.5, {
            autoAlpha: 1
        });

    }

    setLoadingStyle() {
        const $nav = document.querySelector('nav');
        const navHeight = $nav.clientHeight;
        TweenMax.set($nav, {
            y: `-=${navHeight}`
        });
        TweenMax.set(document.querySelector('.loader__logo'), {
            autoAlpha: 1
        });
        document.querySelector('body').style.overflow = 'hidden';
    }

    setLoader() {
        TweenMax.to(document.querySelector('.section--loader'), .5, {
            autoAlpha: 1
        });
        this.logoSpriteManager = new LogoSpriteManager({spriteContainer: document.querySelectorAll('.loader__logo')});
        this.logoSpriteManager.animateSprite(this.finishLoader);
    }

    finishLoader() {
        localStorage.setItem('iPld', true);
        document.querySelector('body').style.overflow = 'scroll';
        const $nav = document.querySelector('nav');
        const navHeight = $nav.clientHeight;
        TweenMax.to($nav, .5, {
            y: `+=${navHeight}`,
            autoAlpha: 1
        });
        TweenMax.to(document.querySelector('.section--loader'), 3, {
                autoAlpha: 0,
                ease:Power3.easeOut,
                onStart: () => {
                    this.getSections();
                    // this.cookiePolicy = new CookiePolicy().init();
                }
            }
        );
    }

    setArrowButtons() {
        // Arrow buttons
        this.arrowBtns = document.getElementsByClassName('section__arrow');

        this.arrowBtnsManger = new ArrowBtnsManager({
            arrows: this.arrowBtns
        });

        this.setArrowSprites();
    }

    setPageNormal() {
        TweenMax.to(document.querySelector('nav'), 1, {
            autoAlpha: 1
        });
        if (document.querySelector('.section--loader')) {
            TweenMax.to(document.querySelector('.section--loader'), 1, {
                autoAlpha: 0,
                onComplete: () => {
                    document.querySelector('.section--loader').style.display = 'none';
                }
            })
        }
    }

    setArrowSprites() {
        this.arrowSprites = [];
        for (let i = 0; i < this.arrowBtns.length; i++) {
            const spriteContainer = this.arrowBtns[i].querySelector('.section__arrow__bg');
            this.arrowSprites.push(new ArrowSpriteManager({
                spriteContainer: spriteContainer,
                arrowNumber: i
            }));
        }
    }


    // DOM Events
    // Scroll
    onScroll(e) {
        for (let x = 0, l = this._totalSections; x < l; x++) {
            if (inViewport(this.$sections[x])) {
                TweenMax.to(this.$sections[x], 0.5, {
                    autoAlpha: 1
                });
                this.titleAnimations[x].timeline.play();
            }
        }
    }
}

window.onload = () => {
    window.App = new App();
    window.App.init();
};

window.initMap = () => {
    window.gMapScript = true;
};
