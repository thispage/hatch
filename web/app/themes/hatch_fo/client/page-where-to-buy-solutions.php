<section class="section pricing">
	<!-- <div class="pricing__section__pattern--top pricing__section__pattern pricing__section__pattern"></div> -->
	<div class="section__pattern section__pattern--4">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>

<?php

$fields = array(
	'get-in-touch' => array(
		'navigation' => '',
		'title' => '',
		'subtitle' => '',
		'shortcode' => '',
		'our_offices_subtitle' => '',
		'offices' => array()
	)
);

$c_fields = array(
	'id' => (int) 216,
	'title' => '',
	'sub_title' => '',
	'plans' => array(),
	'disclaimer' => '',
	'shortcode' => ''
);

$c_fields['title'] = get_the_title();

define(CFIELD_ID, $c_fields['id']);

$args = array(
	'include' => $c_fields['id'],
	'post_type' => 'page',
	'post_status' => 'publish'
);

$posts_array = get_pages($args);

if (count($posts_array) === 1) {
	$post = $posts_array[0];

	$custom_fields = get_fields($post->ID);

//	exit(print_r($custom_fields));

	$c_fields['title'] 	= $custom_fields['title'];
	$c_fields['sub_title'] 	= $custom_fields['sub_title'];
	$c_fields['disclaimer']	= $custom_fields['disclaimer'];
	$c_fields['shortcode']	= $custom_fields['shortcode'];
}

echo '<div class="section__holder">
		<header class="section__header section__header--pricing">';
		echo '<h1 class="section__header__title">'.$c_fields['title'].'</h1>';

		echo '<h2 class="section__header__subtitle shown"><span>'.$c_fields['sub_title'].'</span></h2>';
echo '</header>';

echo '<div class="pricing__content">';
echo do_shortcode('[rpt name="pricing-list"]');
echo '</div>';

echo '
	<div class="disclaimer">
		<p>'.$c_fields['disclaimer'].'</p>
	</div>';
echo '</div>' // end 'section__holder'

?>

</section>
