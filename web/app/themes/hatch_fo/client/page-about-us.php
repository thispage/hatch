<?php

// Default field values
$fields = array(
	'about-us' => array(
		'navigation' => '',
		'title' => '',
		'first_description_column' => '',
		'second_description_column' => '',
		'track_records_sub_title' => '',
		'track_records' => array(),
		'our_team_sub_title' => '',
		'team_members' => array()
	),
);

// About Us (133)
$args = array(
	'include' => 133,
	'post_type' => 'page',
	'post_status' => 'publish'
);
$posts_array = get_pages($args);

if (count($posts_array) === 1) {
	$post = $posts_array[0];
	$custom_fields = get_fields($post->ID);

	$fields['about-us']['title']		= $post->post_title;
	$fields['about-us']['subtitle'] 	= $custom_fields['subtitle'];
	$fields['about-us']['navigation'] 	= $custom_fields['navigation'];
	$fields['about-us']['first_description_column'] 	= $custom_fields['first_description_column'];
	$fields['about-us']['second_description_column'] 	= $custom_fields['second_description_column'];
	$fields['about-us']['track_records'] = $custom_fields['track_records'];
	$fields['about-us']['track_records_sub_title'] = $custom_fields['track_records_sub_title'];

	$fields['about-us']['our_team_sub_title'] 	= $custom_fields['our_team_sub_title'];
	$fields['about-us']['team_members'] 		= $custom_fields['team_members'];
}

?>
<section class="full-page-header">
	<div class="full-page-header-content">
		<h1><?php echo $custom_fields['header_title']; ?></h1>
		<h3><span class="full-page-header-date"><?php echo $custom_fields['header_subtitle']; ?></span></h3>
	</div>

    <div class="full-page-header-bg video-background"
        <?php
            if (isset($custom_fields['header_background_image']) && $custom_fields['header_background_image'] != '') {
                echo 'style="background-image: url('.$custom_fields['header_background_image'].')"';
            }
        ?>
    >
    <?php
	if(!IS_TABLET) {
	    echo '<div class="video-foreground " id="myVideo">
	        <iframe id="myVideo" width="100%" height="100%"
	        src="https://www.youtube.com/embed/YaAwSaMYDyU?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=0&amp;volume=0"
	        frameborder="0"  allowfullscreen">
	        </iframe>
	    </div>';
	}
	?>

	</div>
</section>

<section class="section section--about about" id="section_about">

	<div class="section__pattern section__pattern--4">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>

	<div class="section__holder">
		<header class="section__header section__header--about">
			<h1 class="section__header__title"><?=strtoupper($fields['about-us']['title'])?></h1>
		</header>

		<div class="section__content about__description">
			<p>
				<?=$fields['about-us']['first_description_column']?>
			</p>
			<p>
				<?=$fields['about-us']['second_description_column']?>
			</p>
		</div>

		<div class="section__content about__tracks">
            <div class="section__header">
                <h1 class="section__header__title"></h1>
                <h2 class="section__header__subtitle shown"><span>Track Records</span></h2>
            </div>
			<?php
		if (isset($fields['about-us']['track_records']) && !empty($fields['about-us']['track_records']) && count($fields['about-us']['track_records']) > 0) {
			foreach ($fields['about-us']['track_records'] as $index => $record) {

				echo '<div class="about__tracks__item">
						<div class="about__tracks__item__score" data-num="'.intval($record['number']).'">

							<svg class="about__tracks__item__track" version="1.1"
	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="162px" height="162px"
	 viewBox="0 0 162 162" style="enable-background:new 0 0 162 162;" xml:space="preserve">
								<path class="about-st0" d="M152,81c0,39.2-31.8,71-71,71s-71-31.8-71-71s31.8-71,71-71c19,0,36.2,7.4,48.9,19.6"/>
								<path class="about-st1" d="M152,81c0,39.2-31.8,71-71,71s-71-31.8-71-71s31.8-71,71-71c19,0,36.2,7.4,48.9,19.6" stroke-width="17px"/>
							</svg>
							<span>'.intval($record['number']).'%</span>
						</div>
						<p class="about__tracks__item__txt"><span>'.$record['description'].'</span></p>
					</div>';
			}
		}
		?>
		</div>

		<div class="section__content about__team">
			<div class="section__header">
                <h1 class="section__header__title"></h1>
				<h2 class="section__header__subtitle shown"><span><?=$fields['about-us']['our_team_sub_title']?></span></h2>
			</div>
			<?php
		if (isset($fields['about-us']['team_members']) && !empty($fields['about-us']['team_members']) && count($fields['about-us']['team_members']) > 0) {
			foreach ($fields['about-us']['team_members'] as $index => $member) {

				echo '<div class="about__team__member">
					<div class="about__team__member__photo">
						<img src="'.$member['photo'].'" alt="'.$member['name'].'">
						<div class="about__team__member__photo__links">';

							if (isset($member['linkedin']) && $member['linkedin'] != '') {
								echo '<a href="'.$member['linkedin'].'" class="about__team__member__photo__links__linkedin"></a>';
							}

							if (isset($member['skype']) && $member['skype'] != '') {
								echo '<a href="'.$member['skype'].'" class="about__team__member__photo__links__skype"></a>';
							}

						echo '</div>
					</div>
					<div class="about__team__member__txt">
						<h3 class="about__team__member__txt__name">'.$member['name'].'</h3>
						<p class="about__team__member__txt__function">'.$member['job_title'].'</p>
					</div>
				</div>';

			}
		}
		?>

		</div>
	</div>

</section>
<div class="videoId"><?php echo $custom_fields['video_background']; ?></div>
