<?php

// Default field values
$fields = array(
	'about-us' => array(
		'navigation' => '',
		'title' => '',
		'first_description_column' => '',
		'second_description_column' => '',
		'track_records_sub_title' => '',
		'track_records' => array(),
		'our_team_sub_title' => '',
		'team_members' => array()
	),
);

// About Us (133)
$args = array(
	'include' => 133,
	'post_type' => 'page',
	'post_status' => 'publish'
);
$posts_array = get_pages($args); 

if (count($posts_array) === 1) {
	$post = $posts_array[0];
	$custom_fields = get_fields($post->ID);
	
	$fields['about-us']['title']		= $post->post_title;
	$fields['about-us']['subtitle'] 	= $custom_fields['subtitle'];
	$fields['about-us']['navigation'] 	= $custom_fields['navigation'];
	$fields['about-us']['first_description_column'] 	= $custom_fields['first_description_column'];
	$fields['about-us']['second_description_column'] 	= $custom_fields['second_description_column'];
	$fields['about-us']['track_records'] = $custom_fields['track_records'];
	$fields['about-us']['track_records_sub_title'] = $custom_fields['track_records_sub_title'];
	
	$fields['about-us']['our_team_sub_title'] 	= $custom_fields['our_team_sub_title'];
	$fields['about-us']['team_members'] 		= $custom_fields['team_members'];
}

?>

<section class="section section--about about" id="section_about">
	
	<div class="section__pattern section__pattern--4 top_pattern	">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>

	<div class="section__holder">
		<header class="section__header section__header--about">
			<h1 class="section__header__title"><?=strtoupper($fields['about-us']['title'])?></h1>
		</header>

		<div class="section__content about__description">
			<p>
				<?=$fields['about-us']['first_description_column']?>
			</p>
			<p>
				<?=$fields['about-us']['second_description_column']?>
			</p>
		</div>
	</div>
	
	<!-- <div class="about__section__pattern section__pattern about__section__pattern--bottom"></div> -->
</section>
