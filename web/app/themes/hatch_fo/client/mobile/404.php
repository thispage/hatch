<?php get_template_part('templates/page', 'header'); ?>

<div class="overlay__error">
	<span class="overlay__error__content">
		<img src="<?php echo $dist_path . 'assets/images/ico_error.svg' ?>" width="150" height="126" alt="">
		<span class="overlay__error__content__title">“OOPS SOMETHING WHENT WRONG...”</span>
		<span class="overlay__error__content__msg">This page can not be found!</span>
		<a class="overlay__error__confirm" href="/">GO BACK HOME</a>
		<!-- <button class="overlay__error__confirm">GO BACK HOME</button> -->
	</span>
</div>
