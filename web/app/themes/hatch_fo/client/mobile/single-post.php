<?php

$current_post_id = (int) get_the_ID();

$c_fields = array(
	'id' => $current_post_id,
	'title' => '',
	'sub_title' => '',
	'posts' => array()
);

$c_fields['title'] = get_the_title();
$custom_fields = get_fields((int) get_the_ID());

?>

<section class="news-page-header">
	<div class="news-page-header-content">
		<h1><?php the_title(); ?></h1>
		<span class="date"><?php echo the_field('date'); ?></span>
	</div>

	<div class="news-page-header-bg"
		<?php
		 	if (isset($custom_fields['header_image']) && $custom_fields['header_image'] != '') {
				echo 'style="background-image: url('.$custom_fields['header_image'].')"';
			}
		 ?>
	></div>
</section>

<section class="section news news-article">
	<div class="news__section__pattern news__section__pattern--bottom news-page-top-pattern section__pattern top_pattern green news_one"></div>

	<div class="section__holder">
		<h1><?php echo the_field('short_title'); ?></h1>
		<h2 class="section__header__subtitle shown animate-border"><span><?php echo the_field('short_description'); ?></span></h2>

		<div class="section__content news__content news__content--single">
			<?php the_content(); ?>
		</div>

        <?php
		function getCatList($category)
		{
			return $category->cat_ID;
		}

		$currentPostCats = array_map("getCatList", get_the_category());

		$news_posts = get_posts(array("category" => join(",", $currentPostCats), "exclude" => $current_post_id));

		if ($custom_fields['show_related_posts'] && !empty($news_posts)) {
		?>
		<div class="section-news-read-more">
			<h2 class="section__header__subtitle shown animate-border"><span>Read more</span></h2>
			<?php
			// Previews
			echo '<div class="news-rows read-more-news-rows">';

			$shown = 0;

			foreach($news_posts as $index => $post) {
				?> <div class="news__content__card news__content__card" data-bg="<?php echo the_field('thumbnail'); ?>">
					<div class="news__content__image" style="background-image: url(<?php echo the_field('thumbnail'); ?>)"></div>

					<h3 class="news__content__title"><?php the_title(); ?></h3>
					<div class="news__content__date"><?php the_time('jS F Y'); ?></div>
					<a href="<?php echo get_permalink(); ?>" class="section__smallbtn"><span>read more</span></a>
				</div>
				<?php
				if ($index == 5) {
					break;
				}
			}

            echo '</div>';
			?>
		</div>
		<?php
		}
		?>
	</div>

</section>
