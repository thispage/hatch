<section class="section privacy">

<?php

$fields = array(
	'get-in-touch' => array(
		'navigation' => '',
		'title' => '',
		'subtitle' => '',
		'shortcode' => '',
		'our_offices_subtitle' => '',
		'offices' => array()
	)
);

$c_fields = array(
	'id' => (int) get_the_ID(),
	'title' => '',
	'sub_title' => '',
	'introduction' => array(
		'title' => 'Introduction',
		'content' => ''
	),
	'privacy' => array(
		'title' => 'Privacy',
		'content' => ''
	),
	'collected_information' => array(
		'title' => 'What information does Hatch collect?',
		'content' => ''
	),
	'what_are_cookies' => array(
		'title' => 'What are cookies, and how does Hatch use them?',
		'content' => ''
	),
	'information_logged' => array(
		'title' => 'What information does Hatch log?',
		'content' => ''
	),
	'information_retention_policy' => array(
		'title' => 'What is Hatch\'s information retention policy?',
		'content' => ''
	),
	'information_share' => array(
		'title' => 'With whom does Hatch share information?',
		'content' => ''
	),

	'security' => array(
		'title' => 'Security',
		'content' => ''
	),
	'third_party_sites' => array(
		'title' => 'Links to Third Party Web Sites and Services',
		'content' => ''
	),
	'user_choices' => array(
		'title' => 'What Choices Do You Have?',
		'content' => ''
	),
	'conditions' => array(
		'title' => 'Conditions of Use, Notices, and Revisions',
		'content' => ''
	)
);

$c_fields['title'] = get_the_title();

$args = array(
	'include' => $c_fields['id'],
	'post_type' => 'page',
	'post_status' => 'publish'
);

$posts_array = get_pages($args); 

if (count($posts_array) === 1) {
	$post = $posts_array[0];
	
	$custom_fields = get_fields($post->ID);
	
	$c_fields['sub_title'] 									= $custom_fields['sub_title'];
	$c_fields['introduction']['content']					= $custom_fields['introduction'];
	$c_fields['privacy']['content']							= $custom_fields['privacy'];
	$c_fields['collected_information']['content']			= $custom_fields['collected_information'];
	$c_fields['what_are_cookies']['content']				= $custom_fields['what_are_cookies'];
	$c_fields['information_logged']['content']				= $custom_fields['information_logged'];
	$c_fields['information_retention_policy']['content']	= $custom_fields['information_retention_policy'];
	$c_fields['information_share']['content']				= $custom_fields['information_share'];
	$c_fields['security']['content']						= $custom_fields['security'];
	$c_fields['third_party_sites']['content']				= $custom_fields['third_party_sites'];
	$c_fields['user_choices']['content']					= $custom_fields['user_choices'];
	$c_fields['conditions']['content']						= $custom_fields['conditions'];
}

$content = array_slice($c_fields, 3, count($c_fields));

echo '<div class="section__holder">
		<header class="section__header section__header--privacy">';

	echo '<div class="section__content privacy__content">';
	echo '<h1 class="section__header__title">'.$c_fields['title'].'</h1>';
	forEach($content as $field) {
		echo '<h2 class="section__header__subtitle shown"><span>'.$field['title'].'</span></h2>';
		echo '<p>'.$field['content'].'</p>';
	}
	echo '</div>';
	echo '</div>
	<div class="section__pattern section__pattern--6 privacy__section__pattern--bottom">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>

</section>';
?>