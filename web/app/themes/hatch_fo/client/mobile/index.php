<?php
	get_template_part('templates/page', 'header');

	// Default field values
	$fields = array(
		'home' => array(
			'navigation' => '',
			'title' => '',
			'subtitle' => '',
			'description' => '',
			'vimeo_link' => '',
			'shortcode' => '',
			'solution_title' => '',
			'solution_subtitle' => '',
			'solution_description' => '',
			'solution_highlights' => array(),
			'solution_markets_description' => '',
			'solution_markets_blocks' => array(),
		),
	);


	// Our Where To Buy Solution
	$args = array(
		'include' => 59,
		'post_type' => 'page',
		'post_status' => 'publish'
	);

	$posts_array = get_pages($args);

	if (count($posts_array) === 1) {
		$post = $posts_array[0];

		$custom_fields = get_fields($post->ID);

		$fields['home']['navigation'] = $custom_fields['navigation'];
		$fields['home']['title'] = $custom_fields['title'];
		$fields['home']['subtitle'] = $custom_fields['subtitle'];
		$fields['home']['description'] = $custom_fields['description'];
		$fields['home']['vimeo_link'] = $custom_fields['vimeo_link'];
		$fields['home']['shortcode'] = $custom_fields['shortcode'];
		$fields['home']['request_demo_active'] = $custom_fields['request_demo_active'];

		$fields['home']['solution_title'] 		= $custom_fields['solution_title'];
		$fields['home']['solution_subtitle'] 	= $custom_fields['solution_subtitle'];
		$fields['home']['solution_description'] = $custom_fields['solution_description'];
		$fields['home']['solution_highlights'] 	= $custom_fields['solution_highlights'];
		$fields['home']['solution_markets_description'] 	= $custom_fields['solution_markets_description'];
		$fields['home']['solution_markets_blocks'] 	= $custom_fields['solution_markets_blocks'];

	}
?>

	<div class="section__pattern section__pattern--1 top_pattern">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>

	<section class="section section--intro intro" id="section_intro">


		<div class="intro__bg section__bg">

			<?php
			if ($fields['home']['request_demo_active']) { ?>
			<div id="demo__request" class="open">
				<div id="request-demo-close">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
						<polygon class="st0" points="24,2.9 21.1,0 12,9.1 2.9,0 0,2.9 9.1,12 0,21.1 2.9,24 12,14.9 21.1,24 24,21.1 14.9,12 "/>
					</svg>
				</div>

				<?=do_shortcode($fields['home']['shortcode'])?>
			</div>

			<?php } ?>

		</div>

		<div class="section__holder">

			<header class="section__header section__header--intro">
				<h1 class="section__header__title section__header__title"><?=$fields['home']['title']?></h1>
				<h2 class="section__header__subtitle"><?=$fields['home']['subtitle']?></h2>
				<!-- <p class="section__header__subtitle"><?=$fields['home']['description']?></p> -->
			</header>

			<div class="section__content intro__playBtn">
				<a class="intro__playBtn__link" href="https://player.vimeo.com/video/217142682" target="_blank">
					<button class="intro__playBtn__btn"></button>
					<p class="intro__playBtn__text"><strong>Play the video</strong></p>
				</a>
			</div>
		</div>
	</section>



	<div class="section__separators">
		<div class="section__arrow section__arrow--intro" data-next="2">
			<div class="section__arrow__bg white"></div>
			<span></span>
		</div>

		<div class="section__pattern section__pattern--2 home__section__pattern--bottom">
			<div class="section__pattern__part section__pattern__part--top"></div>
			<div class="section__pattern__part section__pattern__part--bottom"></div>
		</div>
	</div>

	<div class="section section--our-solution our-solution">
		<div class="section__holder">
			<header class="section__header section__header--our-solution">

				<h1 class="section__header__title"><?=$fields['home']['solution_title']?></h1>

				<h2 class="section__header__subtitle shown animate-border">
					<span>
						<?=$fields['home']['solution_subtitle']?>
					</span>
				</h2>

				<p><?=$fields['home']['solution_description']?></p>
			</header>

			<div class="section__content our_solution__steps" style="visibility: inherit; opacity: 1;">
				<svg height="160" width="752" xml:space="preserve" style="enable-background:new 0 0 752 160;" viewBox="0 0 752 160" y="0px" x="0px" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" class="section__content__animation" version="1.1">
					<circle stroke-width="4" stroke="white" fill="none" r="6" cy="80" cx="10" class="edge_circ_left" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
					<circle stroke-width="4" stroke="white" fill="none" r="6" cy="80" cx="742" class="edge_circ_right" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
					<path stroke-width="2" stroke="white" fill="red" d="M 18 80L 78.5 80 M 230.5 80L 298.5 80 M 450.5 80L 518.5 80 M 670.5 80 L 734 80z" class="center_line"/>
					<circle stroke-width="3" stroke="white" fill="none" r="76" cy="80" cx="156" class="idea_circ_1" />
					<circle stroke-width="3" stroke="white" fill="none" r="76" cy="80" cx="376" class="idea_circ_2" />
					<circle stroke-width="3" stroke="white" fill="none" r="76" cy="80" cx="596" class="idea_circ_3" />
					<g class="svgicon light_bulb" transform="matrix(1,0,0,1,128.5,45.5)">
						<path d="M54.8,27.7c0-15-12.2-27.2-27.2-27.2c-1,0-1.8,0.8-1.8,1.8s0.8,1.8,1.8,1.8c13,0,23.6,10.6,23.6,23.6
							c0,7.2-3.3,14.1-9,18.5h-1.6V22.8c0-0.7-0.4-1.3-1-1.6c-0.6-0.3-1.4-0.2-1.9,0.2l-10,7.8l-9.9-7.8c-0.5-0.4-1.3-0.5-1.9-0.2
							c-0.6,0.3-1,0.9-1,1.6v17.1c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V26.4l8.2,6.4c0.6,0.5,1.6,0.5,2.2,0l8.2-6.4v19.7h-24
							c-5.6-4.5-9-11.3-9-18.5c0-4.5,1.3-8.9,3.7-12.7c2.4-3.7,5.7-6.6,9.7-8.5C18.4,6,18.7,4.9,18.3,4c-0.4-0.9-1.5-1.3-2.4-0.8
							C11.4,5.4,7.5,8.8,4.8,13C2,17.4,0.5,22.5,0.5,27.7c0,8.1,3.6,15.8,9.8,20.9v9v9.6c0,1,0.8,1.8,1.8,1.8h31.3c1,0,1.8-0.8,1.8-1.8
							v-9.6v-9.2C51.3,43.3,54.8,35.7,54.8,27.7z M41.6,65.4H13.9v-6h16.9c1,0,1.8-0.8,1.8-1.8s-0.8-1.8-1.8-1.8H13.9v-6h27.7v7.8V65.4
							L41.6,65.4z" class="fill" fill="black" style="fill: rgb(255, 255, 255);"/>
						<path d="M54.8,27.7c0-15-12.2-27.2-27.2-27.2c-1,0-1.8,0.8-1.8,1.8s0.8,1.8,1.8,1.8c13,0,23.6,10.6,23.6,23.6
							c0,7.2-3.3,14.1-9,18.5h-1.6V22.8c0-0.7-0.4-1.3-1-1.6c-0.6-0.3-1.4-0.2-1.9,0.2l-10,7.8l-9.9-7.8c-0.5-0.4-1.3-0.5-1.9-0.2
							c-0.6,0.3-1,0.9-1,1.6v17.1c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V26.4l8.2,6.4c0.6,0.5,1.6,0.5,2.2,0l8.2-6.4v19.7h-24
							c-5.6-4.5-9-11.3-9-18.5c0-4.5,1.3-8.9,3.7-12.7c2.4-3.7,5.7-6.6,9.7-8.5C18.4,6,18.7,4.9,18.3,4c-0.4-0.9-1.5-1.3-2.4-0.8
							C11.4,5.4,7.5,8.8,4.8,13C2,17.4,0.5,22.5,0.5,27.7c0,8.1,3.6,15.8,9.8,20.9v9v9.6c0,1,0.8,1.8,1.8,1.8h31.3c1,0,1.8-0.8,1.8-1.8
							v-9.6v-9.2C51.3,43.3,54.8,35.7,54.8,27.7z M41.6,65.4H13.9v-6h16.9c1,0,1.8-0.8,1.8-1.8s-0.8-1.8-1.8-1.8H13.9v-6h27.7v7.8V65.4
							L41.6,65.4z" class="border" stroke-width="2" fill="none" stroke="white" style="stroke: rgba(255, 255, 255, 0); stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
					</g>
					<g class="svgicon falling_numbers" transform="matrix(1,0,0,1,349,48)">
						<g stroke="none" fill="white" class="fill" style="fill: rgb(255, 255, 255);">
							<path d="M5.6,40.1c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V25.9c0-0.7-0.4-1.4-1.1-1.7c-0.7-0.3-1.5-0.1-2,0.4l-4.6,4.6
								c-0.7,0.7-0.7,1.9,0,2.6s1.9,0.7,2.6,0l1.5-1.5V40.1z"/>
							<path d="M16.3,8.7l1.5-1.5V17c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V2.8c0-0.7-0.4-1.4-1.1-1.7c-0.7-0.3-1.5-0.1-2,0.4
								l-4.6,4.6C13,6.8,13,8,13.7,8.7S15.6,9.4,16.3,8.7z"/>
							<path d="M36.9,24.2c-0.7-0.3-1.5-0.1-2,0.4l-4.6,4.6c-0.7,0.7-0.7,1.9,0,2.6s1.9,0.7,2.6,0l1.5-1.5v9.8
								c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V25.9C38.1,25.2,37.6,24.5,36.9,24.2z"/>
							<path d="M29.1,46.5c-0.7-0.3-1.5-0.1-2,0.4l-4.6,4.6c-0.7,0.7-0.7,1.9,0,2.6s1.9,0.7,2.6,0l1.5-1.5V58
								c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8v-9.7C30.2,47.5,29.7,46.8,29.1,46.5z"/>
							<path d="M26,35.6v-5.3c0-3.4-2.8-6.2-6.2-6.2h-0.1c-3.4,0-6.2,2.8-6.2,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1
								C23.2,41.9,26,39.1,26,35.6z M22.4,35.6c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6v-5.3c0-1.4,1.2-2.6,2.6-2.6h0.1
								c1.4,0,2.6,1.2,2.6,2.6V35.6z"/>
							<path d="M48.8,24.1L48.8,24.1c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1c3.4,0,6.2-2.8,6.2-6.2v-5.3
								C55,26.9,52.2,24.1,48.8,24.1z M51.4,35.6c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6v-5.3c0-1.4,1.2-2.6,2.6-2.6h0.1
								c1.4,0,2.6,1.2,2.6,2.6V35.6z"/>
							<path d="M32,1L32,1c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2H32c1,0,1.8-0.8,1.8-1.8S33,15.1,32,15.1h-0.1
								c-1.4,0-2.6-1.2-2.6-2.6V7.2c0-1.4,1.2-2.6,2.6-2.6H32c1.4,0,2.6,1.2,2.6,2.6c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8
								C38.2,3.8,35.4,1,32,1z"/>
							<path d="M48.8,1L48.8,1c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1c3.4,0,6.2-2.8,6.2-6.2V7.2
								C55,3.8,52.2,1,48.8,1z M51.4,12.6c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6V7.3c0-1.4,1.2-2.6,2.6-2.6h0.1
								c1.4,0,2.6,1.2,2.6,2.6V12.6z"/>
							<path d="M11.3,46.4L11.3,46.4c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1c3.4,0,6.2-2.8,6.2-6.2v-5.3
								C17.5,49.2,14.7,46.4,11.3,46.4z M13.9,57.9c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6v-5.3c0-1.4,1.2-2.6,2.6-2.6h0.1
								c1.4,0,2.6,1.2,2.6,2.6V57.9z"/>
						</g>
						<g fill="none" stroke-width="2" class="border" style="stroke: rgba(255, 255, 255, 0);">
							<path d="M5.6,40.1c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V25.9c0-0.7-0.4-1.4-1.1-1.7c-0.7-0.3-1.5-0.1-2,0.4l-4.6,4.6
								c-0.7,0.7-0.7,1.9,0,2.6s1.9,0.7,2.6,0l1.5-1.5V40.1z" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
							<path d="M16.3,8.7l1.5-1.5V17c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V2.8c0-0.7-0.4-1.4-1.1-1.7c-0.7-0.3-1.5-0.1-2,0.4
								l-4.6,4.6C13,6.8,13,8,13.7,8.7S15.6,9.4,16.3,8.7z" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
							<path d="M36.9,24.2c-0.7-0.3-1.5-0.1-2,0.4l-4.6,4.6c-0.7,0.7-0.7,1.9,0,2.6s1.9,0.7,2.6,0l1.5-1.5v9.8
								c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8V25.9C38.1,25.2,37.6,24.5,36.9,24.2z" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
							<path d="M29.1,46.5c-0.7-0.3-1.5-0.1-2,0.4l-4.6,4.6c-0.7,0.7-0.7,1.9,0,2.6s1.9,0.7,2.6,0l1.5-1.5V58
								c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8v-9.7C30.2,47.5,29.7,46.8,29.1,46.5z" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
							<path d="M26,35.6v-5.3c0-3.4-2.8-6.2-6.2-6.2h-0.1c-3.4,0-6.2,2.8-6.2,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1
								C23.2,41.9,26,39.1,26,35.6z M22.4,35.6c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6v-5.3c0-1.4,1.2-2.6,2.6-2.6h0.1
								c1.4,0,2.6,1.2,2.6,2.6V35.6z" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
							<path d="M48.8,24.1L48.8,24.1c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1c3.4,0,6.2-2.8,6.2-6.2v-5.3
								C55,26.9,52.2,24.1,48.8,24.1z M51.4,35.6c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6v-5.3c0-1.4,1.2-2.6,2.6-2.6h0.1
								c1.4,0,2.6,1.2,2.6,2.6V35.6z" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
							<path d="M32,1L32,1c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2H32c1,0,1.8-0.8,1.8-1.8S33,15.1,32,15.1h-0.1
								c-1.4,0-2.6-1.2-2.6-2.6V7.2c0-1.4,1.2-2.6,2.6-2.6H32c1.4,0,2.6,1.2,2.6,2.6c0,1,0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8
								C38.2,3.8,35.4,1,32,1z" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
							<path d="M48.8,1L48.8,1c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1c3.4,0,6.2-2.8,6.2-6.2V7.2
								C55,3.8,52.2,1,48.8,1z M51.4,12.6c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6V7.3c0-1.4,1.2-2.6,2.6-2.6h0.1
								c1.4,0,2.6,1.2,2.6,2.6V12.6z" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
							<path d="M11.3,46.4L11.3,46.4c-3.5,0-6.3,2.8-6.3,6.2v5.3c0,3.4,2.8,6.2,6.2,6.2h0.1c3.4,0,6.2-2.8,6.2-6.2v-5.3
								C17.5,49.2,14.7,46.4,11.3,46.4z M13.9,57.9c0,1.4-1.2,2.6-2.6,2.6h-0.1c-1.4,0-2.6-1.2-2.6-2.6v-5.3c0-1.4,1.2-2.6,2.6-2.6h0.1
								c1.4,0,2.6,1.2,2.6,2.6V57.9z" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
						</g>
					</g>
					<g class="svgicon shopping_cart" transform="matrix(1,0,0,1,558.5,47.5)">
						<g stroke="none" fill="white" class="fill" style="fill: rgb(255, 255, 255);">
							<path d="M70.7,21.3c-0.3-0.5-0.9-0.8-1.5-0.8h-8c-1,0-1.8,0.8-1.8,1.8c0,1,0.8,1.8,1.8,1.8h5.2L56.1,47.3H26.5
								L14,21.5c-0.3-0.6-0.9-1-1.6-1H2.8c-1,0-1.8,0.8-1.8,1.8c0,1,0.8,1.8,1.8,1.8h8.4l12.5,25.8c0.3,0.6,0.9,1,1.6,1h32
								c0.7,0,1.4-0.4,1.6-1.1L70.8,23C71.1,22.5,71,21.8,70.7,21.3z"/>
							<path d="M27.9,53.3c-3.4,0-6.2,2.8-6.2,6.2s2.8,6.2,6.2,6.2s6.2-2.8,6.2-6.2C34.1,56.1,31.4,53.3,27.9,53.3z
								M27.9,62.2c-1.4,0-2.6-1.2-2.6-2.6c0-1.4,1.2-2.6,2.6-2.6s2.6,1.2,2.6,2.6C30.5,61,29.4,62.2,27.9,62.2z"/>
							<path d="M54.1,53.3c-3.4,0-6.2,2.8-6.2,6.2s2.8,6.2,6.2,6.2c3.4,0,6.2-2.8,6.2-6.2C60.4,56.1,57.6,53.3,54.1,53.3z
								M54.1,62.2c-1.4,0-2.6-1.2-2.6-2.6c0-1.4,1.2-2.6,2.6-2.6c1.4,0,2.6,1.2,2.6,2.6C56.8,61,55.6,62.2,54.1,62.2z"/>
							<path d="M37.2,38.2c10.3,0,18.6-8.3,18.6-18.6S47.5,1,37.2,1S18.6,9.3,18.6,19.6S27,38.2,37.2,38.2z M37.2,4.6
								c8.3,0,15,6.7,15,15s-6.7,15-15,15s-15-6.7-15-15S28.9,4.6,37.2,4.6z"/>
							<path d="M29.5,21.2c0,0.9,0.7,1.5,1.5,1.6c0.9,3.6,3.4,5.5,7.5,5.5c2.6,0,4.9-1,6.2-2.7c0.2-0.3,0.3-0.7,0.3-1
								c0-1-0.8-1.8-1.8-1.8c-0.5,0-0.9,0.2-1.4,0.7c-1,1-1.8,1.3-3.3,1.3c-1.8,0-2.8-0.6-3.3-1.9h2c0.9,0,1.6-0.7,1.6-1.6
								c0-0.7-0.4-1.3-1-1.5c0.6-0.2,1-0.8,1-1.5c0-0.9-0.7-1.6-1.6-1.6h-2c0.6-1.5,1.6-2.1,3.4-2.1c1.4,0,2.2,0.4,3.1,1.4
								c0.4,0.4,0.9,0.7,1.4,0.7c1,0,1.8-0.8,1.8-1.8c0-0.4-0.1-0.7-0.4-1.1c-0.7-1-2.4-2.8-6-2.8c-4.1,0-6.7,2-7.5,5.7
								c-0.8,0.1-1.5,0.7-1.5,1.6c0,0.7,0.4,1.3,1,1.5C29.9,19.9,29.5,20.5,29.5,21.2z"/>
						</g>
						<g fill="none" stroke-width="2" class="border" style="stroke: rgba(255, 255, 255, 0);">
							<path d="M70.7,21.3c-0.3-0.5-0.9-0.8-1.5-0.8h-8c-1,0-1.8,0.8-1.8,1.8c0,1,0.8,1.8,1.8,1.8h5.2L56.1,47.3H26.5
								L14,21.5c-0.3-0.6-0.9-1-1.6-1H2.8c-1,0-1.8,0.8-1.8,1.8c0,1,0.8,1.8,1.8,1.8h8.4l12.5,25.8c0.3,0.6,0.9,1,1.6,1h32
								c0.7,0,1.4-0.4,1.6-1.1L70.8,23C71.1,22.5,71,21.8,70.7,21.3z" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
							<path d="M27.9,53.3c-3.4,0-6.2,2.8-6.2,6.2s2.8,6.2,6.2,6.2s6.2-2.8,6.2-6.2C34.1,56.1,31.4,53.3,27.9,53.3z
								M27.9,62.2c-1.4,0-2.6-1.2-2.6-2.6c0-1.4,1.2-2.6,2.6-2.6s2.6,1.2,2.6,2.6C30.5,61,29.4,62.2,27.9,62.2z" s="" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
							<path d="M54.1,53.3c-3.4,0-6.2,2.8-6.2,6.2s2.8,6.2,6.2,6.2c3.4,0,6.2-2.8,6.2-6.2C60.4,56.1,57.6,53.3,54.1,53.3z
								M54.1,62.2c-1.4,0-2.6-1.2-2.6-2.6c0-1.4,1.2-2.6,2.6-2.6c1.4,0,2.6,1.2,2.6,2.6C56.8,61,55.6,62.2,54.1,62.2z" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
							<path d="M37.2,38.2c10.3,0,18.6-8.3,18.6-18.6S47.5,1,37.2,1S18.6,9.3,18.6,19.6S27,38.2,37.2,38.2z M37.2,4.6
								c8.3,0,15,6.7,15,15s-6.7,15-15,15s-15-6.7-15-15S28.9,4.6,37.2,4.6z" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
							<path d="M29.5,21.2c0,0.9,0.7,1.5,1.5,1.6c0.9,3.6,3.4,5.5,7.5,5.5c2.6,0,4.9-1,6.2-2.7c0.2-0.3,0.3-0.7,0.3-1
								c0-1-0.8-1.8-1.8-1.8c-0.5,0-0.9,0.2-1.4,0.7c-1,1-1.8,1.3-3.3,1.3c-1.8,0-2.8-0.6-3.3-1.9h2c0.9,0,1.6-0.7,1.6-1.6
								c0-0.7-0.4-1.3-1-1.5c0.6-0.2,1-0.8,1-1.5c0-0.9-0.7-1.6-1.6-1.6h-2c0.6-1.5,1.6-2.1,3.4-2.1c1.4,0,2.2,0.4,3.1,1.4
								c0.4,0.4,0.9,0.7,1.4,0.7c1,0,1.8-0.8,1.8-1.8c0-0.4-0.1-0.7-0.4-1.1c-0.7-1-2.4-2.8-6-2.8c-4.1,0-6.7,2-7.5,5.7
								c-0.8,0.1-1.5,0.7-1.5,1.6c0,0.7,0.4,1.3,1,1.5C29.9,19.9,29.5,20.5,29.5,21.2z" style="stroke-dashoffset: 0.00001; stroke-dasharray: none;"/>
						</g>
					</g>
				</svg>
				<div class="caption section__content__caption">
					<div class="intro__steps__text intro__steps__text--find" style="visibility: inherit; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
						<p>Select product</p>
					</div>
					<div class="intro__steps__text intro__steps__text--select" style="visibility: inherit; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
						<p>Select retailer</p>
					</div>
					<div class="intro__steps__text intro__steps__text--complete" style="visibility: inherit; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
						<p>Complete purchase<i></i></p>
					</div>
				</div>

			</div>

			<!-- Highlights - blocks -->

			<?php

			if (isset($fields['home']['solution_highlights']) && count($fields['home']['solution_highlights']) > 0) {
				echo '<div class="our_solution__hightlight_blocks">';

				foreach($fields['home']['solution_highlights'] as $index => $block) {
					echo '<div class="our_solution__hightlight_block">
						<div class="our_solution__hightlight_block__title">
							'.$block['title'].'
						</div>
						<div class="our_solution__hightlight_block__desc">
							'.$block['description'].'
						</div>
					</div>';
				}

				echo '</div>';
			}

			?>

			<!-- Markets - description -->

			<h2><?=$fields['home']['solution_markets_description']?></h2>

			<!-- Markets - blocks -->

			<?php

			if (isset($fields['home']['solution_markets_blocks']) && count($fields['home']['solution_markets_blocks']) > 0) {
				echo '<div class="solutions__content__cards">';

				foreach($fields['home']['solution_markets_blocks'] as $index => $block) {
					echo '<div class="solutions__content__card'.($index == 0 ? ' solutions__content__card--left' : '').''.(($index + 1) == count($fields['home']['solution_markets_blocks']) ? ' solutions__content__card--left' : '').'" style="visibility: inherit; opacity: 1;">
						<div class="solutions__content__card__top" style="background-image: url('.$block['image'].');">
						</div>
						<div class="solutions__content__card__bottom">
							<h3 class="solutions__content__title">'.$block['title'].'</h3>
							<p class="solutions__content__text">'.$block['description'].'</p>
						</div>
						<a class="section__smallbtn btn-left selected" href="'.$block['button_link'].'" data-id="'.$index.'"><span>'.$block['button_text'].'</span><svg height="40" version="1.1" width="140" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: absolute; left: 0px; top: 0px;" viewBox="0 0 140 40" preserveAspectRatio="xMinYMin"><rect x="0" y="0" width="140" height="40" rx="3" ry="3" fill="rgb(77,77,77)" stroke="none" style="" stroke-width="1"/></svg>
						</a>
					</div>

					<div class="solutions__content__card__content" data-desc="'.$index.'">
						<p>
							'.$block['mobile_read_more'].'
						</p>
					</div>'

					 ;
				}

				echo '</div>';
							}

			?>

		</div>

		<div class="pricing__section__pattern pricing__section__pattern--bottom"></div>

	</div>
