<section class="section news">
<div class="news__section__pattern--top news__section__pattern news__section__pattern section__pattern top_pattern"></div>

<?php


$fields = array(
	'get-in-touch' => array(
		'navigation' => '',
		'title' => '',
		'subtitle' => '',
		'shortcode' => '',
		'our_offices_subtitle' => '',
		'offices' => array()
	)
);

$c_fields = array(
	'id' => (int) get_the_ID(),
	'title' => '',
	'sub_title' => '',
	'posts' => array()
);

$c_fields['title'] = get_the_title();

$args = array(
	'include' => $c_fields['id'],
	'post_type' => 'page',
	'post_status' => 'publish'
);

$posts_array = get_pages($args);

if (count($posts_array) === 1) {
	$post = $posts_array[0];

	$current_url = get_permalink( $post->ID );

	$custom_fields = get_fields($post->ID);

	$c_fields['sub_title'] 	= $custom_fields['sub_title'];
	$c_fields['disclaimer']	= $custom_fields['disclaimer'];
	$c_fields['left_column']	= $custom_fields['left_column'];
	$c_fields['right_column']	= $custom_fields['right_column'];
}


$preview_length = 290;

echo '<div class="section__holder">
		<header class="section__header section__header--news">
			<h1 class="section__header__title">'.$c_fields['title'].'</h1>

			<h2 class="section__header__subtitle shown animate-border"><span>'.$c_fields['sub_title'].'</span></h2>

			<div class="news-intro-cols">
				<div class="news-intro-col">
					<p>
						'.$c_fields['left_column'].'
					</p>
				</div>
				<div class="news-intro-col">
					<p>
						'.$c_fields['right_column'].'
					</p>
				</div>
			</div>
		</header>
		<div class="section__content news__content">';

	$news_posts = get_posts();

    function checkShowPost($post)
    {
        return get_post_custom($post->ID)['show_on_blog_page'][0];
    }

    $news_posts = array_filter($news_posts, "checkShowPost");

	// Previews
	echo '<div class="news-rows visible-news-rows">';

	foreach($news_posts as $index => $post) {

		?> <div class="news__content__card news__content__card" data-bg="<?php echo the_field('thumbnail'); ?>">
			<div class="news__content__image" style="background-image: url(<?php echo the_field('thumbnail'); ?>)"></div>

			<h3 class="news__content__title"><?php the_title(); ?></h3>
			<div class="news__content__date"><?php the_time('jS F Y'); ?></div>
			<a href="<?php echo get_permalink(); ?>" class="section__smallbtn"><span>read more</span></a>
		</div>
		<?php
		if ($index == 5) {
			break;
		}
	}

	echo '</div>';

	if (count($news_posts) > 6) {
		echo '<button type="button" class="load-more-btn" id="load-more-news">load more</button>';

		echo '<div class="news-rows hidden-news-rows">';

		foreach($news_posts as $index => $post) {
			if ($index <= 5) {
				continue;
			}

			?> <div class="news__content__card news__content__card" data-bg="<?php echo the_field('thumbnail'); ?>">
				<div class="news__content__image"></div>

				<h3 class="news__content__title"><?php the_title(); ?></h3>
				<span class="news__content__date"><?php the_time('jS F Y'); ?></span>
				<a href="<?php echo get_permalink(); ?>" class="section__smallbtn"><span>read more</span></a>
			</div>
			<?php
		}

		echo '</div>';
	}

	?>

		</div>
	</div>
</section>
