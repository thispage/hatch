<?php

// Default field values
$fields = array(
	'demo' => array(
		'navigation' => '',
		'title' => '',
		'description' => '',
		'shortcode' => '',
		'counter_title' => '',
		'counter_start_amount' => '',
		'counter_start_date' => '',
		'counter_step_speed' => '',
		'counter_amount_per_step' => '',
	),
);

// FAQs (133)
$args = array(
	'include' => 495,
	'post_type' => 'page',
	'post_status' => 'publish'
);
$posts_array = get_pages($args); 

if (count($posts_array) === 1) {
	$post = $posts_array[0];
	$custom_fields = get_fields($post->ID);
	
//	echo '<pre>';
//	exit(print_r($custom_fields));
	
	$fields['demo']['title']		= $custom_fields['title'];
	$fields['demo']['description']	= $custom_fields['description'];
	$fields['demo']['shortcode']	= $custom_fields['shortcode'];
	$fields['demo']['counter_title']	= $custom_fields['counter_title'];
	$fields['demo']['counter_start_amount']	= $custom_fields['counter_start_amount'];
	$fields['demo']['counter_start_date']	= $custom_fields['counter_start_date'];
	$fields['demo']['counter_step_speed']	= $custom_fields['counter_step_speed'];
	$fields['demo']['counter_amount_per_step']	= $custom_fields['counter_amount_per_step'];
}

?>

<section class="section section--request-demo request-demo-section" id="request-demo-section">
	
	<div class="section__pattern section__pattern--5 top_pattern green">
		<div class="section__pattern__part section__pattern__part--top"></div>
		<div class="section__pattern__part section__pattern__part--bottom"></div>
	</div>

	<div class="section__holder">
		<header class="section__header">
			<h1 class="section__header__title"><?php echo $fields['demo']['title']; ?></h1>
		</header>
		
		<p>
			<?php echo $fields['demo']['description']; ?>
		</p>
		
		<?php echo do_shortcode($fields['demo']['shortcode']); ?>
		
		
		<h2><?php echo $fields['demo']['counter_title']; ?></h2>
		
		<?php
			// Revenue counter
			$numbers = array(
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0
			);
			
			$start_num = str_replace(array(',', '-', ' ', '.'), '', $fields['demo']['counter_start_amount']);
			
			$amount_per_step = $fields['demo']['counter_amount_per_step'];

			$step_speed = $fields['demo']['counter_step_speed'];
		
			$start_date = $fields['demo']['counter_start_date'];
			$current_date = date('d-m-Y H:i:s');
		
			if ($start_date != $current_date) {

				// Add amount based on date, step_speed and amount_per_step
				$start_seconds = strtotime($start_date);
				$current_seconds = strtotime($current_date);
			
				$diff_seconds = $current_seconds - $start_seconds;
				
				if ($step_speed == 'minute') {
					$diff_seconds / 60;
				}
				
				$exta_amount = $amount_per_step * $diff_seconds;
				$start_num += $exta_amount;
			}
		
			$start_amount = array_reverse(str_split($start_num));
		
			foreach($start_amount as $index => $amount) {
				$numbers[count($numbers) - ($index + 1)] = $amount;
			}
			
		?>
		
		<div class="revenue_counter" id="revenue_counter" 
			data-current_date="<?php echo $current_date; ?>" 
			data-start_amount="<?php echo $start_num; ?>" 
			data-step_speed="<?php echo $step_speed; ?>" 
			data-amount_per_step="<?php echo $amount_per_step; ?>" 
		>
			
			<span class="number"><?php echo $numbers[0]; ?></span>
			<span class="number"><?php echo $numbers[1]; ?></span>
			<span class="number"><?php echo $numbers[2]; ?></span>
			
			<span class="comma">,</span>
			
			<span class="number"><?php echo $numbers[3]; ?></span>
			<span class="number"><?php echo $numbers[4]; ?></span>
			<span class="number"><?php echo $numbers[5]; ?></span>
			
			<span class="comma">,</span>
			
			<span class="number"><?php echo $numbers[6]; ?></span>
			<span class="number"><?php echo $numbers[7]; ?></span>
			<span class="number"><?php echo $numbers[8]; ?></span>
			
		</div>
		
	</div>
</section>