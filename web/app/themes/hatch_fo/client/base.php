<?php

use Roots\Sage\Config;
use Roots\Sage\Custom_Wrapper;

$dist_path = "/wp-content/themes/hatch_fo/dist/"; //get_template_directory_uri() . DIST_DIR;
$filename = Custom_Wrapper\get_page_name();
define("FILENAME", $filename);

if (
	(FILENAME == 'page-where-to-buy-solutions' ||
	FILENAME == 'page-partners' ||
	FILENAME == 'page-brands' ||
	FILENAME == 'page-retailers' ||
	FILENAME == 'page-agencies' ||
	FILENAME == 'page-faqs')
	&& IS_MOBILE == '1'
) {
	wp_redirect('/', 307);
}

?>
<!doctype html>
<!--[if IE]>
  <html xmlns="http://www.w3.org/1999/xhtml" class="no-js ie <?php echo (IS_MOBILE && !IS_TABLET) ? 'mobile' : 'no-mobile' ?> <?php echo (IS_TABLET) ? 'tablet' : 'no-tablet' ?>" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !IE]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" class="no-js <?php echo (IS_MOBILE && !IS_TABLET) ? 'mobile' : 'no-mobile' ?> <?php echo (IS_TABLET) ? 'tablet' : 'no-tablet' ?>" <?php language_attributes(); ?>>
<!--<![endif]-->
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class($filename); ?>>
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->

	<?php
	  $dev_envs = ['hatch.dev'];

	  if(!in_array($_SERVER['HTTP_HOST'], $dev_envs )) { ?>
		<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-K8RTSV"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-K8RTSV');</script>
		<!-- End Google Tag Manager -->
	<?php } ?>

		<section class="page-wrapper">
			<div id="scrollwrap">
			<?php
				do_action('get_header');
				get_template_part('templates/header');
			?>
			<?php include Custom_Wrapper\template_path(); ?>

			<?php
				do_action('get_footer');
				get_template_part('templates/footer');
				wp_footer();
			?>
			</div>

		<?php
			do_action('get_navigation');
			get_template_part('templates/navigation');
		?>

		<?php
			do_action('get_overlay');
			get_template_part('templates/overlay');
		?>
		</section>

		<script>
			window.currentPage = '<?php echo $filename ?>';
			window.isMobile = '<?php echo IS_MOBILE ?>';
			window.isTablet = '<?php echo IS_TABLET ?>';
		</script>

		<script src="https://www.youtube.com/iframe_api"></script>

		<?php
			// die($dist_path);
		?>

		<script src="<?php echo (IS_MOBILE && !IS_TABLET) ? get_template_directory_uri() . '/dist/scripts/app_mobile.js' : get_template_directory_uri() . '/dist/scripts/app.js' ?>"></script>

		<?php
		if (FILENAME == 'page-contact-us') {
		?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZkTXiTBkOF9zkxUrwVLlH05ZSk_RNDmo&callback=initMap" async defer></script>
		<?php } ?>
        <div class="cookie-policy">
			<p class="cookie-policy__text">
				Hatch is committed to protecting and respecting your privacy. Our website uses cookies to provide you with a good experience when you browse our website and also allows us to improve our site. By continuing to browse the site, you are agreeing to our use of cookies.
			</p>
			<div class="cookie-policy__button">
				OK
			</div>
		</div>
	</body>
</html>
