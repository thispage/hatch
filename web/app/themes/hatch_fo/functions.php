<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/utils.php',                 // Utility functions
  'lib/mobiledetect.php',          // Moible detect
  'lib/init.php',                  // Initial theme setup and constants
  //'lib/wrapper.php',               // Theme wrapper class
  'lib/customwrapper.php',         // Custom wrapper class
  'lib/conditional-tag-check.php', // ConditionalTagCheck class
  'lib/config.php',                // Configuration
  'lib/assets.php',                // Scripts and stylesheets
  'lib/titles.php',                // Page titles
  'lib/extras.php',                // Custom functions
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    var_dump($file);
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

add_filter('wpcf7_ajax_loader', 'my_wpcf7_ajax_loader');
function my_wpcf7_ajax_loader () {
	return  '/wp-content/themes/hatch_fo/dist/assets/images/ajax-loader.gif?from-functions';
}

remove_filter( 'wpcf7_validate_text', 'wpcf7_text_validation_filter', 10);
remove_filter( 'wpcf7_validate_text*', 'wpcf7_text_validation_filter', 10);
add_filter( 'wpcf7_validate_text', 'custom_text_validation_filter', 11, 2 );
add_filter( 'wpcf7_validate_text*', 'custom_text_validation_filter', 11, 2 );

function custom_text_validation_filter( $result, $tag ) {
    $tag = new WPCF7_Shortcode( $tag );

    $name = $tag->name;

    $value = isset( $_POST[$name] )
    ? trim( wp_unslash( strtr( (string) $_POST[$name], "\n", " " ) ) )
    : '';

    if ( 'customContactName' == $tag->name ) {
        if ( $tag->is_required() && '' == $value ) {
          $result->invalidate( $tag, 'Please provide a name' );

        }
    // } else if ( 'customComments' == $tag->name ) {
    //     if ( $tag->is_required() && '' == $value ) {
    //       $result->invalidate( $tag, 'Please provide a message' );

    //     }
    } else {
      if ( $tag->is_required() && '' == $value ) {
        $result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
      }
    }

    return $result;
}

remove_action ('wp_head', 'rsd_link');