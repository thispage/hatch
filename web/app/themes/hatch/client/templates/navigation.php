<?php


////// INDEX PAGE GETS A PROPER MENU

if(FILENAME === 'index') {
	// Default nav itemst copy
	$nav = array(
		'our_where_to_buy_solutions' => 'Our WTB solution',
		'solutions' => 'Solutions',
		'simple_integration' => 'Simple integration',
		'about_us' => 'About us',
		'news' => 'News',
		'get_in_touch' => 'Get in touch'
	);
	// Get WordPress content for the navigation items

	// Our Where To Buy Solution
	$args = array(
		'include' => 59,
		'post_type' => 'page',
		'post_status' => 'publish'
	); 
	$posts_array = get_pages($args); 

	if (count($posts_array) === 1) {
		$post = $posts_array[0];
		$custom_fields = get_fields($post->ID);
		$nav['our_where_to_buy_solutions'] = $custom_fields['navigation'];
	}

	// Solutions
	$args = array(
		'include' => 67,
		'post_type' => 'page',
		'post_status' => 'publish'
	); 
	$posts_array = get_pages($args); 

	if (count($posts_array) === 1) {
		$post = $posts_array[0];
		$custom_fields = get_fields($post->ID);
		$nav['solutions'] = $custom_fields['navigation'];
	}

	// Simple Integrations
	$args = array(
		'include' => 107,
		'post_type' => 'page',
		'post_status' => 'publish'
	);
	$posts_array = get_pages($args); 

	if (count($posts_array) === 1) {
		$post = $posts_array[0];
		$custom_fields = get_fields($post->ID);
		$nav['simple_integration'] 	= $custom_fields['navigation'];
	}

	// About us
	$args = array(
		'include' => 133,
		'post_type' => 'page',
		'post_status' => 'publish'
	);
	$posts_array = get_pages($args); 

	if (count($posts_array) === 1) {
		$post = $posts_array[0];
		$custom_fields 		= get_fields($post->ID);
		$nav['about_us'] 	= $custom_fields['navigation'];
	}

	// Get In Touch
	$args = array(
		'include' => 192,
		'post_type' => 'page',
		'post_status' => 'publish'
	);
	$posts_array = get_pages($args); 

	if (count($posts_array) === 1) {
		$post = $posts_array[0];
		$custom_fields = get_fields($post->ID);
		$nav['get_in_touch'] = $custom_fields['navigation'];
	}

	
	echo '<nav id="navigation" class="navigation">
		<div class="navigation__bar"></div>
		<ul class="navigation__menu navigation__menu--left">
			<li class="navigation__menu__item navigation__menu__item--logo"></li>
			<li class="navigation__menu__item"><a class="navigation__menu__item__link navigation__menu__item__link--internal" data-section="1" href="#intro">'.$nav["our_where_to_buy_solutions"].'</a></li>
			<li class="navigation__menu__item"><a class="navigation__menu__item__link navigation__menu__item__link--internal" data-section="2" href="#solutions">'.$nav["solutions"].'</a></li>
			<li class="navigation__menu__item navigation__menu__item--integration"><a class="navigation__menu__item__link navigation__menu__item__link--internal" data-section="3" href="#integration">'.$nav["simple_integration"].'</a></li>
			<li class="navigation__menu__item"><a class="navigation__menu__item__link navigation__menu__item__link--internal"  data-section="4" href="#about">'.$nav["about_us"].'</a></li>
			<li class="navigation__menu__item"><a class="navigation__menu__item__link navigation__menu__item__link--internal"  data-section="news" href="/news" target="_blank">'.$nav["news"].'</a></li>
		</ul>

		<ul  class="navigation__menu navigation__menu--right">
			<li class="navigation__menu__item"><a class="navigation__menu__item__link navigation__menu__item__link--internal"  data-section="5" href="#contact">'.$nav["get_in_touch"].'</a></li>';
			if(!IS_MOBILE) {
			 echo '<li class="navigation__menu__item"><a class="navigation__menu__item__link navigation__menu__item__link--loginBtn">Login</a></li>'; 
			}
		echo '</ul>';
		echo '<div class="menu-btn">';
			echo '<span></span><span></span><span></span><span></span>';
		echo '</div>';
	echo '</nav>';
}

else {
	// Default nav itemst copy
	$nav = array(
		'home' => 'Home',
		'get_in_touch' => 'Get in touch'
	);	
	
	// Get In Touch
	$args = array(
		'include' => 192,
		'post_type' => 'page',
		'post_status' => 'publish'
	);
	$posts_array = get_pages($args); 

	if (count($posts_array) === 1) {
		$post = $posts_array[0];
		$custom_fields = get_fields($post->ID);
		$nav['get_in_touch'] = $custom_fields['navigation'];
	}

	echo $nav["get_in_touch"];
	echo '<nav id="navigation" class="navigation">
		<div class="navigation__bar"></div>
		<ul class="navigation__menu navigation__menu--left">
			<li class="navigation__menu__item navigation__menu__item--logo"></li>
			<li class="navigation__menu__item"><a class="navigation__menu__item__link navigation__menu__item__link--internal" data-section="1" href="/">'.$nav["home"].'</a></li>
		</ul>

		<ul  class="navigation__menu navigation__menu--right">
			<li class="navigation__menu__item"><a class="navigation__menu__item__link navigation__menu__item__link--internal"  data-section="5" href="#contact">'.$nav["get_in_touch"].'</a></li>
			<li class="navigation__menu__item"><a class="navigation__menu__item__link navigation__menu__item__link--loginBtn">Login</a></li>
		</ul>
		<div class="menu-btn">
			<span></span><span></span><span></span><span></span>
		</div>
	</nav>';
}

?>

