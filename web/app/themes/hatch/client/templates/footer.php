<?php /*<footer class="content-info" role="contentinfo">
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
</footer>
*/ ?>


	<footer class="section--footer footer" id="section_footer">
		<div class="footer__logo"></div>
		<div class="footer__text">
			<span>© 2015 HATCH B.V.</span>
			<span><a href="/packages">Packages</a></span>
			<span><a href="http://gethatch.workable.com">Careers</a></span>
			<span><a href="/terms">Terms of Use</a></span>
			<span><a href="/privacy">Privacy policy</a></span>
		</div>
		<div class="footer__social">
			<div class="footer__social__icon footer__social__icon--twitter"><a href="" target="_blank"></a></div>
			<div class="footer__social__icon footer__social__icon--linkedin"><a href="" target="_blank"></a></div>
		</div>
	</footer>
</section>

<div class="section__separator"></div>
